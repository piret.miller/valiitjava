package it.vali;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;


public class Main {

    static String pin;

    public static void main(String[] args) {
        // Hoiame pin koodi failis pin.txt
        // ja kontojääki balance.txt

        int balance = loadBalance();
        pin = loadPin();

        if(!validatePin()) {
            System.out.println("Kaart konfiskeeritud");
            return;
        }

        System.out.println("Vali tehing:");
        System.out.println("a) Sularaha sissemaks");
        System.out.println("b) Sularaha väljamaks");
        System.out.println("c) Kontojääk");
        System.out.println("d) Konto väljavõte");
        System.out.println("e) Muuda pin");

        Scanner scanner = new Scanner(System.in);

        String answer = scanner.nextLine();
        switch (answer) {
            case "a":
                System.out.println("Sisesta summa");
                int amount =  Integer.parseInt(scanner.nextLine());
                balance += amount;
                saveBalance(balance);
                saveTransaction(amount, "Deposit");
                break;
            case "b":
                System.out.println("Vali summa:");
                System.out.println("a) 5");
                System.out.println("b) 10");
                System.out.println("c) 20");
                System.out.println("d) 100");
                System.out.println("e) Muu summa");
                answer = scanner.nextLine();
                switch (answer) {
                    case "a":
                        amount = 5;
                        if(balance < amount) {
                            System.out.println("Pole piisavalt vahendeid");
                        }
                        else {
                            balance -= amount;
                            saveBalance(balance);
                            saveTransaction(amount, "Withdraw");
                        }

                        break;
                }
                break;
            case "d":
                printTransactions();
                break;
            default:
                break;

        }
    }
    static String currentDateTimeToString() {
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss EEEEE");

        Date date = new Date();
        return dateFormat.format(date);
    }

    static void savePin(String pin) {

        // Toimub pin kirjutamine pin.txt faili
        try {
            FileWriter fileWriter = new FileWriter("pin.txt");
            fileWriter.write(pin + System.lineSeparator());
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("Pin koodi salvestamine ebaõnnestus");
        }


    }
    static String loadPin() {
        // Toimub pin välja lugemine pin.txt failist
        // toimub Shadowing
        //Shadowing refers to the practice in Java programming
        // of using two variables with the same name within scopes that overlap.
        // When you do that, the variable with the higher-level scope is hidden
        // because the variable with lower-level scope overrides it.
        // The higher-level variable is then “shadowed.”
        String pin = null;
        try {
            FileReader fileReader = new FileReader("pin.txt");
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            pin = bufferedReader.readLine();
            bufferedReader.close();
            fileReader.close();
        } catch (IOException e) {
            System.out.println("Pin koodi laadimine ebaõnnestus");
        }
        return pin;
    }

    static void saveBalance(int balance) {
        // Toimub balance kirjutamine balance.txt faili
        try {
            FileWriter fileWriter = new FileWriter("balance.txt");
            fileWriter.write(balance + System.lineSeparator());
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("Kontojäägi salvestamine ebaõnnestus");
        }

    }
    static int loadBalance() {
        // Toimub balance välja lugemine balance.txt failist
        int balance = 0;
        try {
            FileReader fileReader = new FileReader("balance.txt");
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            balance = Integer.parseInt(bufferedReader.readLine());
            bufferedReader.close();
            fileReader.close();
        } catch (IOException e) {
            System.out.println("Kontojäägi laadimine ebaõnnestus");
        }
        return balance;
    }

    static boolean validatePin() {
        Scanner scanner = new Scanner(System.in);

        for (int i = 0; i < 3; i++) {
            System.out.println("Palun sisesta PIN kood");

            String enteredPin = scanner.nextLine();

            if(enteredPin.equals(pin)) {
                System.out.println("Tore! Õige pin kood");
                return true;
            }
        }
        return false;
    }
    static void saveTransaction(int amount, String transaction) {

        try {
            FileWriter fileWriter = new FileWriter("transactions.txt", true);

            if(transaction.equals("Deposit")) {

                fileWriter.append(String.format("%s Raha sissemaks +%d%n", currentDateTimeToString(), amount));
            }
            else {
                fileWriter.append(String.format("%s Raha väljamakse -%d%n", currentDateTimeToString(), amount));
            }

            fileWriter.close();
        } catch (IOException e) {
            System.out.println("Tehingu salvestamine ebaõnnestus");
        }
    }

    static void printTransactions() {
        try {
            System.out.println("Konto väljavõte:");
            FileReader fileReader = new FileReader("transactions.txt");

            BufferedReader bufferedReader = new BufferedReader(fileReader);

            // readLine loeb iga kord välja kutsudes järgmise rea
            String line = bufferedReader.readLine();

            // Kui readLine avastab, et järgmist rida tegelikult ei ole,
            // siis tahastab see meetod null
            while (line != null) {
                System.out.println(line);
                line = bufferedReader.readLine();
            }

            bufferedReader.close();
            fileReader.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}

