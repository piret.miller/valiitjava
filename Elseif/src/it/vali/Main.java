package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        System.out.println("Sisesta arv");
        Scanner scanner = new Scanner(System.in);

        int number = Integer.parseInt(scanner.nextLine());

        if (number > 3) {
            System.out.println("Number on suurem kui 3");
        }
        else if (number < 3 && number != 0) {
                System.out.println("Number on väiksem kui 3");
        }
        else if (number == 0) {
            System.out.println("Number on 0");
        }
        else { // else käib ALATI vaid oma if-ga koos! Kõik, mis on eespool, teda ei tohi huvitada.
                System.out.println("Numbrid on võrdsed");
        }
    }
}

