package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        boolean isMinor = false;

        // boolean isGrownUp = false;

        System.out.println("Kui vana sa oled?");
        Scanner scanner = new Scanner(System.in);

        int age = Integer.parseInt(scanner.nextLine());

        if (age < 18) {
            isMinor = true;
        }
        if (isMinor) { // isMinor on sama , mis isMinor == true
            System.out.println("Oled alaealine");
        } else {
            System.out.println("Oled täisealine");
        }

        int number = age;
        boolean numberIsGreaterThan3 = number > 3;
        System.out.println(numberIsGreaterThan3); // Prindib välja "true" või "false"

        if (number > 2 && number < 6 || number > 10 || number < 20 || number == 100) {

        }
        // ...ja allpool sama asi:

        boolean a = number > 2;
        boolean b = number < 6;
        boolean c = number > 10;
        boolean d = number < 20;
        boolean e = number == 100;
        boolean f = a && b;

        if (f || c || d || e) {

        }

        if (true || false || (true && false)) ;
        {

        }

        if (true) { // Saan ka nii panna, et ongi alati true

        }

        // Küsi kasutajalt, kas ta on söönud hommikusööki.

        boolean hasEaten = false;

        System.out.println("Kas sa oled söönud hommikusööki? Kirjuta jah või ei");
        String userAnswer = scanner.nextLine();

        if (userAnswer.equals("jah")) { // Stringide võrdlemiseks on Javas kasutusel .equals. Otse stringe märgi == abil võrrelda ei saa! (mujal võib saada)
            hasEaten = true;
        }
        // Küsi, kas ta on söönud lõunat

        System.out.println("Kas sa oled söönud lõunasööki? Kirjuta jah või ei");
        userAnswer = scanner.nextLine();

        if (userAnswer.equals("jah")) {
            hasEaten = true;
        }

        // Küsi, kas ta on söönud õhtusööki
        System.out.println("Kas sa oled söönud õhtusööki? Kirjuta jah või ei");
        userAnswer = scanner.nextLine();

        if (userAnswer.equals("jah")) {
            hasEaten = true;
        }

        // Prindi ekraanile, kas kasutaja on täna söönud või mitte.

        if (hasEaten) { // Kui see muutus ülevalpool kas või ühe korra trueks, siis on siin vastus true
            System.out.println("Kasutaja on täna söönud");
        } else {
            System.out.println("Kasutaja ei ole täna söönud");
        }
    }
}
