package it.vali;

import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        // Do While tsükkel on nagu while tsükkel, kuid kontroll tehakse pärast esimest kordust.
        // St ALATI tehakse enne üks kord tegevus ära ja alles siis kontrollitakse.

//        System.out.println("Kas tahad mängu jätkata? Jah/ei");
//        // Jätkame seni, kuni kasutaja kirjutab "ei"
//
//        String noContinue = "ei";
//        String doContinue = "jah";
//
//        Scanner scanner = new Scanner(System.in);
//        String answer = scanner.nextLine();
//
//        while (!(answer.equals(noContinue))) { // Aga "ei" ja "jah" saab ka ilma eelpool deklareerimata siia while loopi sisse tuua! answer.equals("ei")
//            System.out.println("Kas tahad mängu jätkata? Jah/ei");
//            answer = scanner.nextLine();
//        }

	    // Aga kas kuidagi lihtsamalt ei saa? Saab!
        // Do while tsükkel on nagu while tsükkel, ainult et
        // kontroll tehakse pärast esimest kordust.
        // Üks kordus tehakse ALATI do while puhul. Alles edasi hakatakse kontollima tingimust ja kas tuleb edasi kordus või ei tule.
        // While puhul saab kohe alguses määrata, kas tahad üldse korrata

        Scanner scanner = new Scanner(System.in); // Paneme scanneri kõige üles, mitte do järele ega tsükli sisse
        String answer; // Iga muutuja, mille me deklareerime,
        // kehtib vaid oma looksulgude sees!
        // Selle vastu aga aitab see, kui muutuja väärtustada juba main meetodi looksulgude sees, st kuskil üleval pool.
        // Siis piisab, kui tsükli sees kirjutada välja vaid muutuja nimi, mitte enam tüüp
        Random random = new Random();



// P.S.- Do saab teha ka nii, et teed tsükli aktiivseks ja siis "Code" --> "Surround with" ja sealt valida "Do/While"
        do {
            int number = random.nextInt(5) +1;

            System.out.println("Mõtlesin välja ühe numbri. Arva see number ära!");
            int enteredNumber = Integer.parseInt(scanner.nextLine());

            while (enteredNumber != number) {
                System.out.println("Vale number, arva uuesti");
                enteredNumber = Integer.parseInt(scanner.nextLine()); // seda osa on vaja seetõttu, et peame andma While loopile uue tingimuse, mida võrrelda! Tööpõhimõte sarnaneb counteriga i++
            }
            System.out.println("Õige number, sinu võit!");

            System.out.println("Kas tahad mängu jätkata? Jah/ei");
            answer = scanner.nextLine();
        }
        while (!answer.toLowerCase().equals("ei")); // aga saab ka nii:
        // while (scanner.nextLine(),equals ("ei")).
        // Siis saab üks rida üleval pool tsükli sees oleva answer = scanner.nextLine(); ära kustutada
    }
}