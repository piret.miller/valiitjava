package it.vali;

public class Main {

    public static void main(String[] args) {
	// AbstractClass on hübriid liidesest ja klassist.
        // Selles klassis saab defineerida nii meetodite struktuure (nagu liideses),
        // aga saab ka defineerida meetodi koos sissuga.
        // Abstractsest classist ei saa otse objekti luua, saab ainult pärineda

        ApartmentKitchen kitchen = new ApartmentKitchen();
        kitchen.setHeight(100);
        kitchen.setWidth(200);
        kitchen.setLength(400);
        kitchen.becomeDirty();
    }
}
