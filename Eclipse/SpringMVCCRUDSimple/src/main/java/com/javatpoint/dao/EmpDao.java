package com.javatpoint.dao;  
import java.sql.ResultSet;  
import java.sql.SQLException;  
import java.util.List;  
import org.springframework.jdbc.core.BeanPropertyRowMapper;  
import org.springframework.jdbc.core.JdbcTemplate;  
import org.springframework.jdbc.core.RowMapper;  
import com.javatpoint.beans.Emp;  
  
public class EmpDao {  
JdbcTemplate template;  
  
	public void setTemplate(JdbcTemplate template) {  
	    this.template = template;  
	}  
	public int save(Emp p){  
	    String sql= String.format(
	    	"INSERT INTO Emp99"
	    		+ "(name,salary,designation, age) "
	    	+ "VALUES"
	    		+ "('%s', %f, '%s', %d)", p.getName(), p.getSalary(), p.getDesignation(), p.getAge());  
	    return template.update(sql);  
	}  
	public int update(Emp p){  
	    String sql=String.format(
	    	"UPDATE Emp99"
	    	+ "SET"
	    		+ "name = '%s',"
	    		+ "salary = %f,"
	    		+ "designation = '%s',"
	    		+ "age = %d"
	    	+ "WHERE id = %d", p.getName(), p.getSalary(), p.getDesignation(), p.getAge(), p.getId());  
	    return template.update(sql);  
	}  
	public int delete(int id){  
	    String sql="delete from Emp99 where id="+id+"";  
	    return template.update(sql);  
	}  
	public Emp getEmpById(int id){  
	    String sql="select * from Emp99 where id=?";  
	    return template.queryForObject(sql, new Object[]{id},new BeanPropertyRowMapper<Emp>(Emp.class));  
	}  
	public List<Emp> getEmployees(){  
	    return template.query("select * from Emp99",new RowMapper<Emp>(){  
	        public Emp mapRow(ResultSet rs, int row) throws SQLException {  
	            Emp e=new Emp();  
	            e.setId(rs.getInt(1));  
	            e.setName(rs.getString(2));  
	            e.setSalary(rs.getFloat(3));  
	            e.setDesignation(rs.getString(4));  
	            e.setAge(rs.getInt(5));
	            return e;  
	        }  
	    });  
	}  
}  