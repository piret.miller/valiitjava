package it.vali;

public class Main {

    public static void main(String[] args) {
        // Method overriding ehk meetodi ülekirjutamine
        // tähendab seda, et kuskil klassis, millest antud klass pärineb, oleva meetodi sisu
        // kirjutatakse pärinevas klassis üle.
        // Päritava klassi meetodi sisu kirjutatakse pärinevas klassis üle

        Dog buldog = new Dog();
        buldog.eat();
        buldog.printInfo();
        buldog.getAge();
        buldog.getName();

        System.out.println();

        Cat siam = new Cat();
        siam.eat();
        siam.setOwnerName("Palle");
        siam.printInfo(); // Kassi puhul on printInfo üle kirjutataud

        System.out.println(buldog.getAge());
        System.out.println(siam.getAge());

        // Kirjuta koera getAge üle nii, et kui koeral vanus on 0, siis näitaks 1
        // Kui koera kaal on 0, tagasta 1

        System.out.println(buldog.getWeight());

        // Metsloomadel printInfo võiks kirjutada Nimi: metsloomal pole nime

        WildAnimal wildAnimal = new WildAnimal();
        wildAnimal.printInfo();
    }
}
