package it.vali;

public class Pig extends FarmAnimal {
    private boolean hasTwistedTail;

    public boolean isHasTwistedTail() {
        return hasTwistedTail;
    }

    public void setHasTwistedTail(boolean eatsMeat) {
        this.hasTwistedTail = hasTwistedTail;
    }

    public void bathInMud() {
        System.out.println("Mulle meeldib mudas püherdada");
    }


}
