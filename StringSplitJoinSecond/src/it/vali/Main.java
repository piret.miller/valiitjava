package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        String sentence = "Väljas on ilus ilm, vihma ei saja ja päike paistab";

        // Split tükeldab Stringi ette antud sümbolite kohalt ja tekitab sõnade massiivi
        // | tähendab regulaaravaldises või

        String[] words = sentence.split(" ja | |, "); // jagab lause tühikute abil sõnadeks.
        // Stringist tehti stringi massiiv.
        // Saab ette anda, mitu korda tükeldatakse. Nt 3 tükeldab kuni kolm esimest tühikut

        for (int i = 0; i < words.length; i++) {
            System.out.println(words[i]);
        }
        System.out.println();

        String newSentence = String.join(", ", words);
        // 1. parameeter on see, kus ühendame ehk mida tahan, mida vahele pandaks (antud juhul koma).
        // 2. parameeter on see, mida ühendame ehk massiiv nimega words
        System.out.println(newSentence);

        newSentence = String.join(" ", words);
        System.out.println(newSentence);

        newSentence = String.join(" ja ", words);
        System.out.println(newSentence);

        // 3 8 9 6 5 8 0 9

        newSentence = String.join("\t", words); // Paneb ühe tab'i vahele
        // \n võib alati kasutada, %n võib kasutada vaid printf-ga.
        // Escape Symbols Java--> leiab veelgi selliseid sümboleid
        System.out.println(newSentence);

        // Escaping
        System.out.println("Juku ütles: \"Mulle meeldib suvi.\""); // Lisab jutumärgid

        System.out.println("Juku ütles: \n\"Mulle meeldib suvi.\""); // Lisab jutumärgid ja viib lause järgmisele reale

        System.out.println("Juku\b\b\007 \bütles:\\n \"Mulle meeldib suvi.\""); // \b kustutab kõik vaskaule jääva ära, ühe koha võrra.
        // 007 pidavat tegema piiksu

        System.out.println("C:\\Users\\opilane\\Documents"); // Selleks, et saada trükkida kaldkriipse, peab need topelt kirjutama

        // Küsi kasutajalt 5 numbrit nii, et ta paneb need numbrid kirja ühele reale,
        // eraldades tühikuga. Seejärel liida need kõik numbrid kokku ja prindi summa

        System.out.println("Sisesta arvud, mida tahad omavahel liita, eraldades tühikuga");
        Scanner scanner = new Scanner(System.in);
        String numbersText = scanner.nextLine();
        String[] numbers = numbersText.split(" "); // Siin võtame tühikud ära ja
        // "numbers"is on juba puhtad numbrid

        int sum = 0;

        for (int i = 0; i < numbers.length; i++) {
//            sum = sum + Integer.parseInt(numbers[i]);
           sum += Integer.parseInt(numbers[i]); // Kuna "numbers" on praegu veel STRINGI massiiv,
            // siis tuleb see "numbers" teisendada, kuid AINULT "numbers"
        }
        String joinedNumbers = String.join(", ", numbers);
        System.out.printf("Arvude %s summa on %d%n", joinedNumbers, sum);
    }
}