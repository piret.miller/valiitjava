package it.vali;

public class Main {

    static int a = 3; // Saab nt eristada ka nii, et märgid _a

    public static void main(String[] args) {
        int b = 7;

        System.out.println(b + a);

        a = 0;
        System.out.println(b + a);
        System.out.println(increaseByA(10));

        // Shadowing ehk siis sees pool defineeritud muutuja
        // varjutab väljaspool olevat muutujat
        int a = 6;
        a = 1;

        // If ja else statememndiga EI SAA ÕNNEKS A-D MUUTA!

//        if (b > 0) {
//            int a = -7;
//        }

        // Nii saan klassi a väärtust muuta:
        Main.a = 8;

        // Meetodis defineeritud a väärtuse muutmine
        a = 5;
        System.out.println(b + a);

        System.out.println(increaseByA(10));
        System.out.println();
        System.out.println(b + Main.a);
        System.out.println(b + a);

    }
    static int increaseByA(int b) {
        return b += a;
    }
}
