package it.vali;

public class Main {

    public static void main(String[] args) {
	// Järjend, massiiv, nimekiri
        // Luuakse täisarvude massiiv, millesse mahub 5 elementi
        // Loomise hetkel määratud elementide arvu hiljem muuta ei saa.
        int[] numbers = new int[5];

        // Massiivi indeksid algavad 0-st, mitte 1-st (igal massiivi elemendil on oma indeks)
        // Viimane indeks on alati 1 võrra väiksem kui massiivi pikkus

        numbers [0] = 2;
        numbers [1] = 7;
        numbers [2] = -2;
        numbers [3] = 11;
        numbers [4] = 1;

        System.out.println(numbers[0]);
        System.out.println(numbers[1]);
        System.out.println(numbers[2]);
        System.out.println(numbers[3]);
        System.out.println(numbers[4]);

        System.out.println();

        // Sama saab teha ka for tsükliga:

        for (int i = 0; i < 5; i++) {
            System.out.println(numbers[i]);
        }
        System.out.println();

        // Prindi numbrid tagurpidises järjekorras

        for (int i = 4; i >= 0; i--) {
            System.out.println(numbers[i]);
        }
        System.out.println();

        // Prindi numbrid, mis on suuremad kui 2

        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] > 2) {
                System.out.println(numbers[i]);
            }
        }
        System.out.println();

        // Prindi kõik paarisarvud

        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] % 2 == 0) {
                System.out.println(numbers[i]);
            }
        }
        System.out.println();

        // Prindi kõik paaritud arvud

        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] % 2 != 0) {
                System.out.println(numbers[i]);
            }
        }
        System.out.println();

        // Prindi tagantpoolt 2 esimest paaritut arvu

        int counter = 0; // kuskil on vaja hoida, mitu kordust meil on, et tagantpoolt lugeda
        for (int i = numbers.length - 1; i >=0; i--) { // int i = numbers.length MIINUS 1, sest numbers.length on 5, kuid suurim indeks on 4
            if (numbers [i] % 2 != 0) {
                System.out.println(numbers[i]);
                counter++;
                if (counter == 2) {
                    break; // hea näide, kuidas break kasutada. Kui oleks continue, siis prindiks välja kõik paaritud arvud
                }
            }
        }
        System.out.println();

        // Loo teine massiiv 3-le numbrile ja pane sinna esimesest
        // massiivist 3 esimest numbrit.
        // Prindi teise massiivi elemendid ekraanile.

        int [] secondNumbers = new int[3];// Kõigepealt loome massiivi

        // Saab teha nii...

        secondNumbers [0] = numbers[0];
        secondNumbers [1] = numbers[1];
        secondNumbers [2] = numbers[2];

        // Aga saab sama teha ka for tsükliga:

        for (int i = 0; i < secondNumbers.length ; i++) {
            secondNumbers[i] = numbers[i];
        }

        // Ja nüüd prindime teise massiivi numbrid välja (kuigi juba ka eelmisele tsüklile saab prindi lisada):

        for (int i = 0; i < secondNumbers.length ; i++) {
            System.out.println(secondNumbers[i]);
        }
        System.out.println();

        // Loo kolmas massiiv 3-le numbrile ja pane sinna esimesest massiivist 3 numbrit tagant poolt alates (1, 11, -2)

        // Näites kasutame siiski teist massiivi.
        // Peaks olema selline tulemus:

//        secondNumbers[0] = numbers[4];
//        secondNumbers[1] = numbers[3];
//        secondNumbers[2] = numbers[2];


        for (int i = 0; i < secondNumbers.length ; i++) {
            secondNumbers[i] = numbers[numbers.length - i - 1]; // SecondNumbers [0] võrdub numbers [length 5 - 0 -1 = 4] jne
        }

        for (int i = 0; i < secondNumbers.length ; i++) {
            System.out.println(secondNumbers[i]);
        }
        System.out.println();

        // Teine variant on käsitsi kirjutada (kui ei õnnestu ülevalolevat algoritmi välja mõelda):

        int [] thirdNumbers = new int[3];

        for (int i = 0, j = numbers.length-1; i < thirdNumbers.length ; i++, j--) {
            // j = numbers.lenght -1 on sama, mis j = 4.
            // Aga kui nt 10 kaupa tahame, paneme lõppu counteriks j+=10
            thirdNumbers[i] = numbers[j];
        }

        for (int i = 0; i < thirdNumbers.length; i++) {
            System.out.println(thirdNumbers[i]);
        }
        System.out.println();

        for (int i = 0, j = numbers.length-1; i < thirdNumbers.length ; i++, j -= 2) {
            // Väljastab arvud üle ühe, seega j tuleb panna võrduma j -=2
            thirdNumbers[i] = numbers[j];
        }

        for (int i = 0; i < thirdNumbers.length; i++) {
            System.out.println(thirdNumbers[i]);
        }
    }
}
