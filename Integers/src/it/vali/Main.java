package it.vali;

import java.util.DoubleSummaryStatistics;

public class Main {

    public static void main(String[] args) {

        // muutujat deklareerida ja talle väärtus anda saab ka kahe sammuga (mitte otse, ühe sammuga). St väärtuse saab anda ka hiljem
        int number;
        number = 3;

        String word;
        word = "Kala";

        int secondNumber = -7;

        System.out.println(number);
        System.out.println(secondNumber);
        System.out.println(number + secondNumber);
        System.out.println(number * secondNumber);

        // Arvude 3 ja -7 korrutis on 21

        System.out.println("Arvude " + number + " ja " + secondNumber + " korrutis on " + (number * secondNumber));

        // %n tekitab platvormi-spetsiifilise reavahetuse (Windows \r\n ja Linux/Mac \n).
        // Selle asemel saab kasutada ka System.lineSeparator()

        System.out.printf("Arvude %d ja %d korrutis on %d\n", // paneme reavahetuse, sest muidu prindib otse eelmise outputi otsa. Muide, \r tähendab ajalooliselt "returni", mis viib kursori tagasi SAMA rea algusesse. Seetõttu peaks kasutama alati \r ja \n koos (nt Notepad seda vaikimisi teeb)
                number, secondNumber, number * secondNumber); // samas võib "number * secondNumberi" asemel ka luua uue muutuja int = product ja siis asendada "number * secondNumber" "productiga"

        int product = number * secondNumber;

        System.out.printf("Arvude %d ja %d korrutis on %d\n",
                number, secondNumber, product);

        int a = 3;
        int b = 14;

        System.out.println("Arvude summa on " + a + b); // kui liita kokku String ja integer, saame kokku alati Stringi
        // Stringi liitmisel numbriga teisendatakse number Stringiks ja liidetakse kui liitsõna. Ülaltoodud tehte tulemuseks on seega 314, mitte 17
        System.out.println("Arvude summa on " + (a + b)); // aitab see, kui liitmistehe teha enne sulgudes ära
        System.out.printf("Arvude %d ja %d summa on %d%n", number, secondNumber, number + secondNumber);

        System.out.println("Arvude " + a + " ja " + b + " jagatis on " + a/b); // vastus tuleb täisosa (komakohast kõik paremale "süüakse" ära)
        System.out.printf("Arvude %d ja %d jagatis on %d%n", a, b, a / b); // kahe täisarvu jagamisel on tulemus jagatise täisosa ehk kõik pärast koma "süüakse" ära
        System.out.printf("Arvude %d ja %d jagatis on %d%n", b, a, b / a);


        System.out.println("Arvude " + a + " ja " + b + " jagatis on " + (double)a/b); // tuleb castida doubleks

        int maxInt = 2147483647; // kui võtta max number ja liita sellele 1 juurde,
        // siis hakkab ta uuesti miinusest (ehk inti puhul väikseimast numbrist ehk -2147483648) lugema
        System.out.println();

        int c = maxInt + 1;
        int d = maxInt + 2;
        System.out.println(c);
        System.out.println(d);

        int minInt = -2147483648;
        int e = minInt - 1; // ja samamoodi hakkab lugema plussist ehk max numbrist, kui väikseimast numbrist üritada lahutada
        System.out.println(e);

        short f = 32199;
        // long väärtust andes peab numbrile L tähe lõppu lisama
        long g = 800000000L;
        long h = 234;

        System.out.println(g + h);
    }
}
