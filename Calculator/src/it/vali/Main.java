package it.vali;
import java.lang.Math;

public class Main {

    public static void main(String[] args) {
        int sum = sum(4, 5);
        System.out.printf("Arvude 4 ja 5 summa on %d%n", sum);
        System.out.printf("Arvude 10 ja 8 vahe on %d%n", substract(10, 8));
        System.out.printf("Arvude 10 ja 8 korrutis on %d%n", multiply(10, 8));
        System.out.printf("Arvude 10 ja 8 jagatis on %f%n", divide(10, 8));

        int[] numbers = new int[4];

        numbers[0] = 1;
        numbers[1] = -2;
        numbers[2] = 6;
        numbers[3] = -3;

        sum = sum(numbers);
        System.out.printf("Massiivi arvude summa on %d%n", sum);

        numbers = new int[]{2, 5, 12, -12};
        sum = sum(new int[]{2, 3, 12, 8});
        System.out.printf("Massiivi arvude summa on %d%n", sum);
        sum = sum(new int[]{2, 3});

        int[] reversed = reverseNumbers(numbers);
        // Sama asi lühemalt:
        printNumbers(reverseNumbers(numbers));

        printNumbers(new int[]{1, 2, 3, 4, 5});
        printNumbers(reverseNumbers(new int[]{1, 2, 3, 4, 5}));

//    Saab ka nii:    sum(reverseNumbers(new int[] {1, 2, 3, 4, 5}));

        printNumbers(convertToIntArray(new String[]{"2", "-12", "1", "0", "17"})); // Loon jooksu pealt massiivi,
        // ise panen elemendid sisse ja see kõik lähebki otse parameetriks

        // ...(new String[]) lahti kirjutault:
        String[] numbersAsText = new String[]{"2", "-12", "1", "0", "17"};
        int[] numbersAsInt = convertToIntArray(numbersAsText);
        printNumbers(numbersAsInt);

        String[] sentence = new String[]{"Taevas", "lendas", "kollane", "õhupall"};
        printSentenceSpace(sentence);

        // System.out.printf("Sõnade massiiv liidetuna sõnadeks on %s", arrayToString(new String [] ("elas", "metsas", "mutionu"))
        // System.out.printf("Sõnade massiiv liidetuna sõnadeks on %s", arrayToString(" ja ", new String [] ("elas", "metsas", "mutionu"))

        double averageSum = averageSumOfNumbers(new int[]{2, 5, 12, -12});
        System.out.printf("Massiivi arvude keskmine on %.2f%n", averageSum); // meetodi nimi averageSumOfNumbers (new int [] { 1, 2, 3, 4});

        int a = 15;
        int b = 60;
        // Kui tahame kasutada protsendi märki printf sees, kasutame topelt % , ehk %%
        System.out.printf("Arv %d moodustab arvust %d %.2f%% protsenti%n",
                a, b, howManyPercent(a, b));

        double radius = 10.25;
        System.out.printf("Ringi raadiusega %.2f ümbermõõt on %.2f%n",
                radius, circleCircumference(radius));
    }

    // Meetod, mis liidab 2 täisarvu kokku ja tagastab nende summa
    static int sum(int a, int b) { // Kui on sees int, saab IDE juba aru, et peab olema return-lause juures
        int sum = a + b;
        return sum;
    }

    // substract
    static int substract(int a, int b) {
//        int substract = a - b;
        return a - b;
    }

    // multiply
    static int multiply(int a, int b) {
//        int multiply = a * b;
        return a * b;
    }

    // divide
    static double divide(int a, int b) {
//        int divide = a / b;
        return (double) a / b;
    }

    // Meetod, mis võtab parameetriks täisarvude massiivi ja liidab elemendid kokku ning tagastab summa
    static int sum(int[] numbers) {
        int sum = 0;
        for (int i = 0; i < numbers.length; i++) {
            sum += numbers[i];
        }
        return sum;
    }

    // Meetod, mis võtab parameetriks täisarvude massiivi, pöörab tagurpidi ja tagastab selle
    // 1 2 3 4 5
    // 5 4 3 2 1

    static int[] reverseNumbers(int[] numbers) { // Tagastustüübiks on nüüd mitte enam int, vaid MASSIIV.
        int[] reverseNumbers = new int[numbers.length];

        for (int i = 0; i < numbers.length; i++) {
            reverseNumbers[i] = numbers[numbers.length - i - 1]; // Sarnased näited all! Teine variant (näide, kus pannakse kaks massiivi kokku!) Kolmas variant on ka j-ga teha
        }
        return reverseNumbers;
    }
/*
//
//    for (int i = 0; i < secondNumbers.length ; i++) {
//        secondNumbers[i] = numbers[numbers.length - i - 1];
//    }
//        System.out.println();
//
//        for (int i = 0; i < secondNumbers.length ; i++) {
//        System.out.println(secondNumbers[i]);
//    }
//        System.out.println();
//
//    // Teine variant on käsitsi kirjutada (kui ei õnnestu ülevalolevat algoritmi välja mõelda):
//
//    int [] thirdNumbers = new int[3];
//
//
//        for (int i = 0, j = numbers.length-1; i < thirdNumbers.length ; i++, j--) { // j = numbers.lenght -1 on sama, mis j = 4. Aga kui nt 10 kaupa tahame, paneme lõppu counteriks j+=10
//        thirdNumbers[i] = numbers[j];
//    }
//
//        for (int i = 0; i < thirdNumbers.length; i++) {
//        System.out.println(thirdNumbers[i]);
//
//    }
    }

 */

    // Meetod, mis prindib välja täisarvude massiivi elemendid
    static void printNumbers(int[] numbers) {
        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);
        }
    }

    // Meetod, mis võtab parameetriks stringi massiivi
    // (eeldusel, et tegelikult seal massiivis on numbrid stringidena)
    // ja teisendab numbrite massiiviks ja tagastab selle

    static int[] convertToIntArray(String[] numbersAsText) { // ootab massiivi! anname selle üleval
        int[] numbers = new int[numbersAsText.length];
        for (int i = 0; i < numbersAsText.length; i++) {
            numbers[i] = Integer.parseInt(numbersAsText[i]);
        }
        return numbers;
    }
    // Meetod, mis võtab parameetriks stringi massiivi ning tagastab lause,
    // kus iga stringi vahel on tühik

    /**
     * Jagab lause tühikute abil sõnadeks. Stringist tehti stringi massiiv. // Saab ette anda, mitu korda tükeldatakse. Nt 3 tükeldab kuni kolm esimest tühikut
     * Split tükeldab Stringi ette antud sümbolite kohalt ja tekitab sõnade massiivi
     *
     * @param words
     */
    static void printSentenceSpace(String[] words) {
        String sentence = "";                               // Tekitasin tühja stringi
        for (int i = 0; i < words.length; i++) {            // Tsükkel selleks, et tühja stringi lisadamassiivist elemente koos tühikutega
            sentence = sentence.concat(words[i] + " ");     // väärtustasin stringi sentenci nii, et sentencile lisandus juurde liidetud sõnad koos tühikuetga
        }
        System.out.println(sentence);
    }

    static String arrayToString(String[] sentence) {
        return String.join(" ", sentence);
    }

    // Meetod, mis võtab parameetriks stringi massiivi ning teine parameeter on
    // sõnade eraldaja.
    // Tagastada lause.  Vaata eelmist ülesannet,
    // lihtsalt tühiku asemel saad ise valida, mille pealt sõnu eraldab

    static String arrayToString(String delimiter, String[] sentence) {
        return String.join(", ", sentence);
    }


    /**
     * Meetod, mis leiab numbrite massivist keskmise ning tagastab selle
     *
     * @param numbers
     * @return averageSum
     */
    static double averageSumOfNumbers(int[] numbers) {
        int sum = 0;
        for (int i = 0; i < numbers.length; i++) {
            sum += numbers[i];
        }
        return sum;
    }

    // Aga saab ka nii:

    static double averageSum(int[] numbers) {
        return sum(numbers) / (double)numbers.length;
    }

    // Meetod, mis arvutab, mitu % moodustab esimene arv teisest.

    static double howManyPercent(int a, int b) {
        //double percent = ((double)a * 100) / b;
        return ((double) a * 100) / b; // a / (double)b * 100
    }

    // Meetod, mis leiab ringi ümbermõõdu raadiuse järgi

    static double circleCircumference(double radius) {
//        double circumference = (double) radius * 2 * Math.PI; PIKK VARIANT
        return radius * 2 * Math.PI;

    }
}









