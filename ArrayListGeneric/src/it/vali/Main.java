package it.vali;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        // wGeneric array list on selline kollektsioon,
        // kus objekti loomise hetkel peame määrama,
        // mis tüüpi elemente see sisaldama hakkab

        // See on tavaline ArrayList:
//        List list = new ArrayList();

        // Nüüd aga on see:
        List<Integer> numbers = new ArrayList<Integer>(); // tähendab, et kõik listi sees olevad elemendid on Integerid.

        // ei saa panna stringe:
       // numbers.add("tere");

        numbers.add(Integer.valueOf("10")); // niimoodi teisendada aga saab
        numbers.add(1);
        numbers.add(2);

        List<String> words = new ArrayList<String >(); // siia uude listi saab lisada ainult sõnasid
        words.add("Tere");
        words.add("Head aega!");





    }
}
