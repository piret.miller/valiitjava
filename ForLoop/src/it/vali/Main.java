package it.vali;

public class Main {

    public static void main(String[] args) {
        // Tsükleid on erinevaid. For tsükkel on selline tsükkel, kus korduste arv on teada.


        // Lõpmatu for tsükkel
//        for( ; ; ) {
//            System.out.println("Väljas on ilus ilm");
//        }

//        for (int i = 0; i < ; i++) { // Shortcut on "fori" + Enter
//        }
        // 1) Esimene komponent int = 0. Siin saab luua muutujaid ja neid algväärtustada.
        // Luuakse täisarv i, mille väärtus hakkab tsükli sees muutuma.
        // 2) Teine komponent i < 3 on tingimus, mis peab olema tõene, et tsükkel käivituks ja korduks.
        // Seni kuni i on väiksem 3-st, see tsükkel kordub. Nii kui i võrdub 3-ga, tsükkel lõpetab töö.
        // 3) Kolmas komponent i++ on tegevus, mida iga tsükli korduse lõpus korratakse.
        // i++ on sama, mis kirjutada i = i + 1
        // i-- on sama, mis i = i - 1
        // Näide kommentaari kirjutamisest mitmel real:

            /* thrtyhrehtgire
            hrtyhjryhorsyhojs
             */
        // ülemist laadi väga ei kasutata, sest on lihtne vigu teha,
        // lisaks ei saa kommentaari kommentaari sees kasutada
        // <-- seda laadi kommentaare saab teha kiirelt Ctrl + numbrite juures olev kaldkriips
        // (enne aktiveerid soovitud piirkonna)


        for (int i = 0; i < 3; i++) { // Lihtsalt infoks, et kui on looksulud,
            // siis looksulu ees ei ole KUNAGI semikoolinit! Sageli kiputakse panema liiga sageli semikooloneid
            System.out.println("Väljas on ilus ilm");
        }
        System.out.println();

        // Prindi ekraanile numbrid 1 kuni 10
        // kõigepealt luuakse muutuja i ja pannakse ta võrduma 0-ga.
        // Seejärel tehakse kontroll, kas i on väiksem 10-st. Kui jah, siis tuleb teha kordus (tekst prinditakse välja).
        // Seejärel kutsutakse counter välja ja suurendatakse i ühe võrra suuremaks.
        // Nüüd tehakse uuesti kontroll, et kas i on väiksem 10-st (i on nüüd 1).
        // Kuna 1 on endiselt väiksem kui 10, siis tsükkel kordub.
        // Kui lõpuks i enam ei ole väiksem 10-st, muutub condition false-ks ning printimist ei toimu.
        // Samuti ei toimu enam ka i-le juurdeliitmist.

        // Kolm varianti:

        for (int i = 0; i < 10; i++) {
            System.out.println(i + 1);
        }
        System.out.println();

        for (int i = 1; i < 11; i++) {
            System.out.println(i);
        }
        System.out.println();

        for (int i = 1; i <= 10; i++) { // see variant on parim, sest ilus on vaadata neid numbreid, mida reaalselt ülesande püstituses oli vaja
            System.out.println(i);
        }
        System.out.println();

        // Prindi ekraanile numbrid 24-st 167-ni
        for (int i = 24; i <= 167; i++) {
            System.out.println(i);
        }
        System.out.println();

        // Prindi ekraanile numbrid 18-st 3-ni
        for (int i = 18; i >= 3; i--) {
            System.out.println(i);
        }
        System.out.println();

        // Prindi ekraanile numbrid 2 4 6 8 10
        for (int i = 2; i <= 10; i = i + 2) {
            System.out.println(i);
        }
        System.out.println();

        for (int i = 2; i <= 10; i += 2) { // lühem variant counterile i = i + 2
            System.out.println(i);
        }
        System.out.println();

        for (int i = 16; i >= 0; i -= 4) { // lühem variant counterile i = i - 4
            System.out.println(i);
        }
        System.out.println();

        // Ja samuti saab ka korrutada ja jagada:
        // i = i * 3 => i *=3
        // i = i / 2 => i /=2

        // Prindi ekraanile numbrid 10 kuni 20 ja 40 kuni 60
        // Esimene lahendus:

        for (int i = 10; i <= 60; i++) {
            if (i <= 20 || (i >= 40 && i <= 60)) { // ... aga tegelikult võib i <=60 ka ära jätta, sest see on juba enne paika pandud, et i max on 60
                    System.out.println(i);
            }
        }
        System.out.println();


        // Teine lahendus:

        for (int i = 10; i <= 60; i++) {
            if (i == 21) { // Kui i jõuab 21-ni, siis lihtsalt ütlen i-le, et muutu nüüd 40-ks! Nii prindibki
                i = 40;
            }
            System.out.println(i);
        }
        System.out.println();

        // Prindi kõik arvud, mis jaguvad 3-ga vahemikus 10 kuni 50
        // a % 3 (protsendimärk leiab jäägi). Nt on 4 % 3 => jääk 1
        // 6 % 3 => jääk 0
        // if (a%3 == 0)

        for (int i = 10; i <= 50; i++) {
            if (i % 3 == 0) { // Jääk peab võrduma 0-ga! Nt kui i = 10, siis jääk on 1, 11-ga on jääk 2, 12-ga on jääk 0.
                System.out.println(i);
            }
        }
        System.out.println();

        for (int i = 10; i <= 50; i++) { // paaris arvud
            if (i % 2 == 0) {
                System.out.println(i);
            }
        }
        System.out.println();

        for (int i = 10; i <= 50; i++) { // paaritud arvud
            if (i % 2 != 0) {
                System.out.println(i);
            }
        }
        System.out.println();

        // teine variant on jääk panna võrduma 1-ga
        System.out.println();
        for (int i = 10; i <= 50; i++) { // paaritud arvud
            if (i % 2 == 1) {
                System.out.println(i);
            }
        }
    }
}
