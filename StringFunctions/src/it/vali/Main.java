package it.vali;

public class Main {

    public static void main(String[] args) {
        String sentence = "kevadel elutsedes metsas Mutionu keset kuuski noori vanu."; // Defineerime lause

        // Sümbolite indeksid TEKSTIS algavad samamoodi indeksiga 0, nagu MASSIIVIDES.

        // Leia üles esimene tühik, mis on tema indeks? Kasutame meetodit indexOf

        int spaceIndex = sentence.indexOf(" "); // leiab alati esimese indexi (nt sõna Mutionu indeks on 25 , st tähe M indeks on 25)
        System.out.println(spaceIndex);
        // meetod indexOf tagastab -1, kui otsitavat fraasi (sümbolit või sõna) ei leitud
        // ning indeksi (kust sõna algab), kui fraas leitakse.
        // Väga hea meetod, kui on vaja leida tekstist mingit juppi

        spaceIndex = sentence.indexOf("v"); // otsime, kus asub esimene v-täht ja mis on tema indeks
        System.out.println(spaceIndex);

        // Otsime teise tühiku asukoha:

        spaceIndex = sentence.indexOf(" ");
        int secondSpaceIndex = sentence.indexOf(" ", spaceIndex + 1); // Defineerime teise indeksi tüübi
        // ning liidame olemasolevale tühiku indeksile + 1,
        // st hakkame uut tühikut otsima asukohaga alates eelmisest indeksist pluss 1.
        System.out.println(secondSpaceIndex);
        System.out.println();

        // Prindi välja kõigi tühikute indeksid lauses. Hea näide while tsükli kasutamiseks- me ei tea ette, mitu kordust tuleb teha.

        spaceIndex = sentence.indexOf(" "); // kõigepealt defineerime taas otsitava

        // Nii trükib välja ka -1, mida me tegelikult ei taha (ning ei tüki esimese tühiku indeksit):
        while (spaceIndex != -1) {
            spaceIndex = sentence.indexOf(" ", spaceIndex + 1);
            System.out.println(spaceIndex);
        }
        System.out.println();

        // ... seega tuleb tõsta println rea võrra ülespoole! Siis ei haarata kaasa enam viimast -1 (olukorda, kus tsükkel enam ei leidnud tühikut)

        spaceIndex = sentence.indexOf(" ");

        while (spaceIndex != -1) {
            System.out.println(spaceIndex);
            spaceIndex = sentence.indexOf(" ", spaceIndex + 1);
        }
        System.out.println();

        // Analoogiline meetod on veel olemas, lastIndexOf:

        spaceIndex = sentence.lastIndexOf(" ");

        while (spaceIndex != -1) {
            System.out.println(spaceIndex);
            spaceIndex = sentence.lastIndexOf(" ", spaceIndex - 1); // ehk hakkame tagant poolt tühikute asukohtasid lugema
        }
        System.out.println();

        // Prindi lause esimesed 4 tähte. Saame kasutada substringi meetodit:

        String part = sentence.substring(0, 4); // meetodi esimene number määrab algusindexi,
        // teine määrab lõppindexi, mis jääb prindist VÄLJA. Saab kasutada, kui TEAME sõna alguse ja lõpu asukohta
        System.out.println(part);

        // Siin aga otsime teksti esimest sõna. Peame seega märkima, et otsigu alates 0 kuni tühikuni.
        // võime enne defineerida spaceIndex = sentence.indexOf(" ");
        // ja seejärel meetodisse kirjutada (0, spaceIndex), aga seda saab teha ka ühes lauses koos
        part = sentence.substring(0, sentence.indexOf(" "));
        System.out.println(part);

        // Prindi lause teine sõna
        // On vaja teist muutujat!

        spaceIndex = sentence.indexOf(" ");
        secondSpaceIndex = sentence.indexOf(" ", spaceIndex + 1);
        String secondWord = sentence.substring(spaceIndex + 1, secondSpaceIndex);

        System.out.println(secondWord);

        // Leia esimene k-tähega sõna ja et leiaks ka siis, kui see on lause esimene sõna

        String firstLetter = sentence.substring(0, 1); // substringiga saab sõna osa kätte, antud juhul esimene täht
        String kWord = "";
        if (firstLetter.equals("k")) {
            spaceIndex = sentence.indexOf(" ");
            kWord = sentence.substring(0, spaceIndex);
        } else {
            int kIndex = sentence.indexOf(" k") + 1;
            if (kIndex != 0) {
                spaceIndex = sentence.indexOf(" ", kIndex); // esimene tühik pärast kIndexi asukohta (from kIndex)

                kWord = sentence.substring(kIndex, spaceIndex);
            }
        }
        if (kWord.equals("")) {
            System.out.println("Puudub k-tähega algav sõna");
        } else {
            System.out.printf("Esimene k-tähega algav sõna on %s%n", kWord);
        }
        System.out.println();


        // Leia, mitu sõna mul lauses on. Peab tulema 8

        spaceIndex = sentence.indexOf(" ");
        int spaceCounter = 0;
        while (spaceIndex != -1) { // ehk kuni programm veel leiab tühikuid
//            System.out.println(spaceIndex); Selle jätame nüüd välja
            spaceIndex = sentence.indexOf(" ", spaceIndex + 1);
            spaceCounter++;
        }
        System.out.printf("Sõnade arv lauses on %d%n", spaceCounter + 1);

        System.out.println();

        // Leia, mitu k-tähega algavat sõna on lauses.
        // Kui muidu lugesime tühikud üle ja liitsime ühe juurde, siis nüüd otsime " k".
        // do while-ga poleks siin pointi. Tuleks algusest int kIndeks panna -1
        // ja esimene kord loopi minnes saaks -1-st 0. (sest on +1 sees).
        // Esimene kord otsiks 0-st. Kokkuvõttes do whilega oli 8 rida koodi, muidu aga 5

        int kWordCounter = 0;
        sentence = sentence.toLowerCase(); // teen selle lause väikesteks tähtedeks ja nüüd kõik, mis allpool muutub, muudab sedasama lauset
        int kIndex = sentence.indexOf(" k");

        while (kIndex != -1) { // ehk kuni programm veel leiab tühikuid
            kIndex = sentence.indexOf(" k", kIndex + 1);
            kWordCounter++;
        }
        if(sentence.substring(0, 1).equals("k")) {
            kWordCounter++;
        }
        System.out.printf("Lauses on kokku %d k-tähega algavat sõna%n", kWordCounter);
    }
}
