package it.vali;

import java.util.Scanner;

public class Main {
// Küsi kasutajalt 2 arvu, seejärel küsi kasutajalt, mis tehet ta soovib teha,
// a) liitmine, b) lahutamine, c) korrutamine, d) jagamine).
// Prindi kasutajale ette vastus
// Lisada tsükkel ja küsida lõpus, kas kasutaja soovib veel arvutada

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        do {
            System.out.println("Ütle esimene arv");
            int firstNumber = Integer.parseInt(scanner.nextLine());
            System.out.println("Ütle teine arv");
            int secondNumber = Integer.parseInt(scanner.nextLine());

            boolean wrongAnswer;

            do {
                System.out.println("Mis tehet sa soovid nendega teha");
                System.out.println("a) liitmine");
                System.out.println("b) lahutamine");
                System.out.println("c) korrutamine");
                System.out.println("d) jagamine");

                String usersChoise = scanner.nextLine();
                wrongAnswer = false;


                if (usersChoise.toLowerCase().equals("a")) {
                    System.out.printf("Arvude %d ja %d summa on %d%n", firstNumber, secondNumber, sum(firstNumber, secondNumber));
                } else if (usersChoise.toLowerCase().equals("b")) {
                    System.out.printf("Arvude %d ja %d vahe on %d%n", firstNumber, secondNumber, substract(firstNumber, secondNumber));
                } else if (usersChoise.toLowerCase().equals("c")) {
                    System.out.printf("Arvude %d ja %d korrutis on %d%n", firstNumber, secondNumber, multiply(firstNumber, secondNumber));
                } else if (usersChoise.toLowerCase().equals("d")) {
                    System.out.printf("Arvude %d ja %d jagatis on %.2f%n", firstNumber, secondNumber, divide(firstNumber, secondNumber));
                } else {
                    System.out.println("Selline tehe puudub");
                    wrongAnswer = true; // Ainus plokk, kus ta true's saab. Mujal jääb false'ks
                }

            } while (wrongAnswer);
            System.out.println("Kas jätkame arvutamist? Jah/ei");

        } while (scanner.nextLine().toLowerCase().equals("jah"));
    }


    static int sum(int firstNumber, int secondNumber) {
        int sum = firstNumber + secondNumber;
        return sum;
    }

    static int substract(int firstNumber, int secondNumber) {
        return  firstNumber - secondNumber;
    }

    static int multiply(int firstNumber, int secondNumber) {
        return firstNumber * secondNumber;
    }

    static double divide(int firstNumber, int secondNumber) {
        return (double) firstNumber / secondNumber;
    }

} // do while- tahame, et üks kord alati teeks ära ja siis alles kontrolliks.
// Teeme main methodi juures prindid aktiivseks. Code --> surround with --> do-while