package it.vali;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        try {
            FileReader fileReader = new FileReader("C:\\Users\\opilane\\Documents\\input.txt");

            BufferedReader bufferedReader = new BufferedReader(fileReader);

            // readLine loeb iga kord välja kutsudes järgmise rea

            String line = bufferedReader.readLine();

            // Kui readLine avastab, et järgmist rida tegelikult ei ole, siis tagastab see meetod null

            // line = bufferedReader.readLine(); Selle asemel kasutame while loopi
            //  System.out.println(line);

            while (line != null) { // Esmalt kontrollib, kas üldse on ridu failis
                System.out.println(line); // Prindib esimese rea
                line = bufferedReader.readLine(); // Paneb iga rea järgmise reaga võrduma
            } // Kui rida on failis null, siis hüppab tsüklist välja. Või kui fail ongi tühi, siis ei lähegi tsüklisse


            // DO-WHILE NÄIDE: (pole siiski hetkel ni mõttekas)

//            String line = null;

//            do {
//                line = bufferedReader.readLine();
//                if (line != null); {
//                    System.out.println(line); // Prindib esimese rea
//                }
//                 // Kui rida on failis null, siis hüppab tsüklist välja. Või kui fail ongi tühi, siis ei lähegi tsüklisse
//            } while (line != null);


             bufferedReader.close();
             fileReader.close();

        } catch (FileNotFoundException e) { // Alguses see...
            e.printStackTrace();
        } catch (IOException e) { // ... ja siis see, üldisem. On viisakas
            e.printStackTrace();
        }
    }
}
