package it.vali;

// Enum on tüüp, kus saab defineerida erinevaid lõplikke valikuid
// Tegelikult salvestatakse enum alati int-na (selgitus materjalidesse saadetud)

import javax.print.DocFlavor;

enum Color {
    BLACK,
    WHITE,
    GREY,
}
enum ScreenType {
    LCD, TFT, OLED, AMOLED
}

public class Monitor {
    private String manufacturer; // Kui märgime "private", siis ei saa teises klassis seda meetodit välja kutsuda
    private double diagonal; // " inch, tollides
    private Color color; // Stringi asemel lõime enum paketi/tüübi, mille täitsime värvidega. Lisame selle värvi tüübiks
    private ScreenType screenType;

    public int getYear() {
        return year;
    }

    private int year = 2000;


    // Saab ka automaatselt: aktiivseks private muutujal nt "color"--> code--> generate--> Getter-Setter

    public String getManufacturer() {
        return manufacturer;
    }
    public void setManufacturer(String manufacturer) {
        if (manufacturer == null // Nulli kontroll peab alati esimene olema! Kui ei ole null, siis läheb järgmist tingimust võrdlema.
                // Kui null on nt viimane siis viskab NullPointer Exceptioni,
                // sest IDE saab juba ise aru, et väärtus ei saa olla null, kui ta on eelmised tingimused juba läbinud!
                || manufacturer.equals("Huawei")
                || manufacturer.equals("")
        ) {
            this.manufacturer = "Tootja puudub";
        } else {
            this.manufacturer = manufacturer;
        }
    }
    // Ära luba seadistada monitori tootjaks Huawei
    // Kui keegi soovib seda tootjat monitori tootja panna,
    // pannakse hoopis tootjaks tekst "Tootja puudub"

    // Keela ka tühja tootjanime lisamine"", null



    public double getDiagonal() { //Get-meetodi abil saab enne seadistused ära teha (enne küsimist)
        if(diagonal == 0) {
            System.out.println("Diagonaal on seadistamata");
        }
        return diagonal;
    }
    public void setDiagonal(double diagonal) { // Set-meetodi abil saab panna nt mingeid filtreid pale jne
        // this tähistab seda konkreetset objekti
        if(diagonal < 0) {
            System.out.println("Diagonaal ei saa olla negatiivne");
        }
        else if (diagonal > 100) {
            System.out.println("Diagonaal ei saa olla suurem kui 100\"");
        } else {
            this.diagonal = diagonal;
        }
    }

    public Color getColor () {
        return color;
    }
    public void setColor (Color color) {
        this.color = color;
    }
// Kui ekraanitüüp on seadistamata (null), siis tagasta tüübiks LCD
    public ScreenType getScreenType () {
        if(screenType == null) { // Nulliga võrreldes ära kasuta .equals, vaid ==
            return ScreenType.LCD;
        }
        return screenType;
    }
    public void setScreenType (ScreenType screenType) {
        this.screenType = screenType;
    }




    public void printInfo() {
        System.out.println();
        System.out.println("Monitori info: ");
        System.out.printf("Tootja: %s%n", manufacturer);
        System.out.printf("Diagonaal: %.1f%n", diagonal);
        System.out.printf("Värv: %s%n", color);
        System.out.printf("Ekraani tüüp: %s%n", getScreenType()); // Muutsime ekraanitüübi "Screentype"'st getScreenType'ks, sest siis küsib läbi meetodi ning paneb tõesti 0-i võrduma LCD-ga
        System.out.printf("Aasta: %d%n", year); // Yearile on ligipääs vaid klassi sees!
        System.out.println();
    }

    // Tee meetod, mis tagastab ekraani diagonaali sentimeetrites

    public double DiagonalToCm() {
        return diagonal *2.54;
    }
}
