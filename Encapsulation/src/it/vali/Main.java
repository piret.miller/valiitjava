package it.vali;

public class Main {

    public static void main(String[] args) {
        // int vaikeväärtus 0
        // boolean vaikeväärtus false
        // double vaikeväärtus 0.0
        // float vaikeväärtus 0.0f
        // String vaikeväärtus null
        // Objektide (Monitor, FileWriter) vaikeväärtus null
        // Massiivi int[] arvud; vaikeväärtus null
        Monitor firstMonitor = new Monitor();
        firstMonitor.setDiagonal(-1000);
        System.out.println(firstMonitor.getDiagonal());

        firstMonitor.setDiagonal(200);
//        firstMonitor.color = Color.GREY;
        System.out.println(firstMonitor.getDiagonal());

        System.out.println(firstMonitor.getYear());
        firstMonitor.setManufacturer("Huawei");
        firstMonitor.printInfo();

        firstMonitor.getScreenType();
        firstMonitor.printInfo();
        firstMonitor.setManufacturer(null);
    }
}
