package it.vali;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        // Kui panna üks primitiiv (value type) tüüpi muutuja võrduma
        // teise muutujaga, siis tegelikult tehakse arvuti mällu uus muutuja
        // ja väärtus kopeeritakse sinna

        // Sama juhtub ka primitiiv (value type) tüüpi muutuja kaasa andmisel
        // meetodi parameetriks (tegelikult tehakse arvuti mällu uus muutuja
        // ja väärtus kopeeritakse sinna)
        int a = 3; // teen muutuja a
        int b = a; // panen b temaga võrduma. Tegelikult on kaks erinevat muutujat
        a = 7;
        System.out.println(b);

        increment(b);
        System.out.println(b);

        // Kui panna Reference-type viit-tüüpi muutuja panna võrduma teise muutujaga,
        // siis tegelikult jääb mälus ikkagi alles ainult 1 muutuja,
        // lihtsalt teine muutuja hakkab viitama samale kohale mälus (samale muutujale).
        // Meil on kaks muutjat, aga tegelikult on nad täpselt sama objekt

        // Sama juhtub ka Reference-type viit-tüüpi muutuja kaasa andmisel...

        Point pointA = new Point();
        pointA.x = 10;
        pointA.y = 3;

        Point pointB = pointA; // pointA ja pointB on objektid
        pointB.x = 7;

        System.out.println(pointA.x); // on nüüd sama, mis pointB.x, st 7

        List<Point> points = new ArrayList<Point>();
        points.add(pointA);
        points.add(pointB);

        pointA.y = -4;

        System.out.println(points.get(0).y);
        System.out.println(points.get(1).y);
        System.out.println(pointB.y);

        pointA.printNotStatic(); // kutsub objekti pointA meetodit välja (peab objekti looma, sets pole Point classis static!

        Point.printStatic(); // Ei pea midagi välja kutsuma, sest meetod on Point classis static

        Point pointC = new Point();
        pointC.x = 12;
        pointC.y = 20;
        Point.increment(pointC);
        System.out.println(pointC.x);

        System.out.println();

        int[] thirdNumbers = new int[] { 1, 2, 3 };
        increment(thirdNumbers);
        System.out.println(thirdNumbers[0]);

        Point pointD = new Point();
        pointD.x = 5;
        pointD.y = 6;
        pointD.increment();
        System.out.println(pointD.y); // Vastus tuleb 7, sest oleme teinud Point klassi meetodi, mis suurendaks x ja y ühe võrra.

    }

    public static void increment(int a) {
        a++;
        System.out.printf("a on nüüd %d%n", a);

        int[] numbers = new int[] { -5 };

        int[] secondNumbers = numbers;
        numbers[0] = 3;
        System.out.println(secondNumbers[0]);
    }

    // Suurendame kõiki elemenete 1 võrra
    public static void increment(int[] numbers) {
        for (int i = 0; i < numbers.length; i++) {
            numbers[i]++;
        }
    }
}
