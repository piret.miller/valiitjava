package it.vali;

public class Point {
    public int x; // hetkle ei te eget-set meetodeid, sets pole mõtet. Pole loogikat ning seetõttu koodui paemaks ei teeks
    public int y;

    public static void printStatic() { // Staatilised meetodid on sellised meetodid, mis ei ole objekti endaga seotud
        System.out.println("Olen static punkt");
    }

    public void printNotStatic() {
        System.out.println("Olen punkt");
    }

    public void increment() { // ei pea parameetreid x ja y panema, sest need on siin klassis juba olemas
        x++;
        y++;
    }
    // suurendame x ja y koordinaate 1 võrra
    public static void increment(Point point) {
        point.x++;
        point.y++;

    }
}
