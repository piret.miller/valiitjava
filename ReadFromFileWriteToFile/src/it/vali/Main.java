package it.vali;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;

public class Main {

    public static void main(String[] args) {
        // Loe failist input.txt iga teine rida ning kirjuta need read
        // faili output.txt

        try {
            // Notepad vaikmisi kasutab ANSI encoding-ut.
            // Selleks, et FileReader oskaks seda korrektselt lugeda (täpitähti),
            // peame talle ette ütlema, et loe seda ANSI encodingus
            // Cp1252 on java-s ANSI encoding
            FileReader fileReader = new FileReader("C:\\Users\\opilane\\Documents\\input.txt", Charset.forName("Cp1252"));

            // Faili kirjutades on javas vaikeväärtud UTF-8.
            FileWriter fileWriter = new FileWriter("C:\\Users\\opilane\\Documents\\output.txt", Charset.forName("UTF-8"));
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            String line = bufferedReader.readLine(); // readLine loeb iga kord välja kutsudes järgmise rea

            int lineNumber = 1;

            while (line != null) { // Esmalt kontrollib, kas üldse on ridu failis
                if (lineNumber % 2 == 1) {
                    fileWriter.write(line + System.lineSeparator()); // enam ei pane System.out.println, vaid otse prindin uude faili
                }
                line = bufferedReader.readLine(); // Paneb iga rea järgmise reaga võrduma
                lineNumber++;
            } // Kui rida on failis null, siis hüppab tsüklist välja.
            // Või kui fail ongi tühi, siis ei lähegi tsüklisse

            bufferedReader.close(); // Loogiline on, et kõigepealt bufferFile kinni
            fileReader.close(); // ... seejärel fileReaderi kinni.
            fileWriter.close();// ...ja siis see.

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}