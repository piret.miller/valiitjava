package it.vali;

import java.lang.Math;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {

    public static void main(String[] args) {
	// 1. Ülesanne
        // Deklareeri muutuja, mis hoiaks endas PI arvulist väärtust vähemalt 11 kohta pärats koma.
        // Korruta selle muutuja väärtus kahega ja prindi standardväljundisse.

        // 2. ülesanne
        //Kirjuta meetod, mis tagastab boolean-tüüpi väärtuse ja mille sisendparameetriteks on
        // kaks täisarvulist muutujat. Meetod tagastab tõeväärtuse vastavalt sellele,
        // kas kaks sisendparameetrit on omavahel võrdsed või mitte.
        // Meetodi nime võid ise välja mõelda.
        // Kutsu see meetod main()-meetodist välja ja prindi tulemus standardväljundisse
        System.out.println(equalNumbers(3,3));

        // 3. ülesanne
        // Kirjuta meetod, mille sisendparameetriks om Stringide massiiv ja mis tagastab täisarvude massiivi.
        // Tagastatava massiivi iga element sisaldab endas vastava sisendparameetrina vastu võetud massiivi elemendi (stringi) pikkust.
        // Meetodi nime võid ise välja mõelda.

        String[] words = new String[] {"2", "2", "3", "4", "5"};
        int[] numbers = stringArrayToIntArray(words);
        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);

        }

        // 4. ülesanne
        // Kirjuta meetod, mis võtab sisendparameetriks aastaarvu täisarvulisel kujul (int)
        // vahemikus 1-2018 ja tagastab täisarvu (byte) vahemikus 1-21 vastavalt sellele,
        // mitmendasse sajandisse antud aasta kuulub.
        // Meetodi nime võid ise välja mõelda.
        // Kui funktsioonile antakse ette parameeter, mis on suurme kui 2018 või väiksme kui 1,
        // tuleb tagastada väärtus -1.
        // Kutsu see meetod välja main()-meetodist järgmiste erinevate sisendväärtustega:
        // 0, 1, 128, 598, 1624, 1827, 1996, 2017. Prindi need väärtused välja

        System.out.printf("Pakutud aasta kuulub %d. sajandisse%n", inWhichCentury(1624));



        // 5. ülesanne
        // Defineeri klass Country, millel oleksid meetodid getPopulation,
        // setPopulation, getName, setName ja list riigis enim kõneldavate keeltega.
        // Override'i selle klassi toString() meetid nii, et
        // selle meetodi väljakutsumine tagastaks teksti, mis
        // sisaldaks endas kõiki parameetreid väljaprindituna.
        // Tekita antud klassist üks objekt ühe vabalt valitud riigi andmetega ja
        // prindi selle riigi info välja .toString() meetodi abil.


        // 1. Ülesanne
        // Deklareeri muutuja, mis hoiaks endas PI arvulist väärtust vähemalt 11 kohta pärast koma.
        // Korruta selle muutuja väärtus kahega ja prindi standardväljundisse.

        final double PI = Math.PI;
        DecimalFormat numberFormat = new DecimalFormat("#.00000000000");
        double multiply = PI * 2;
        System.out.println(numberFormat.format(multiply));


    }
    // 2. ülesanne
    //Kirjuta meetod, mis tagastab boolean-tüüpi väärtuse ja mille sisendparameetriteks on
    // kaks täisarvulist muutujat. Meetod tagastab tõeväärtuse vastavalt sellele,
    // kas kaks sisendparameetrit on omavahel võrdsed või mitte.
    // Meetodi nime võid ise välja mõelda.
    // Kutsu see meetod main()-meetodist välja ja prindi tulemus standardväljundisse

        static boolean equalNumbers (int firstNumber, int secondNumber) {
        //boolean isEqual;
        if(firstNumber == secondNumber) { // if returniga ei ole else vaja!
            //isEqual = true;
            return true;
        }         //isEqual = false;
            return false;
        //return isEqual;
    }
    // 3. ülesanne
    // Kirjuta meetod, mille sisendparameetriks om Stringide massiiv ja mis tagastab täisarvude massiivi.
    // Tagastatava massiivi iga element sisaldab endas vastava sisendparameetrina vastu võetud massiivi elemendi (stringi) pikkust.
    // Meetodi nime võid ise välja mõelda.

    static int[] stringArrayToIntArray(String[] numbersAsText) {
        int[]numbers = new int[numbersAsText.length];
        for (int i = 0; i < numbersAsText.length ; i++) {
            numbers[i] = Integer.parseInt(numbersAsText [i]);
        }
        return numbers;
    }
    static int generateAsInt (int count) {
        int number = 0; // alguses on tühi
        for (int i = 0; i < count ; i++) {
            number = number + number;

        }
        return number;
    }

    // 4. ülesanne
    // Kirjuta meetod, mis võtab sisendparameetriks aastaarvu täisarvulisel kujul (int)
    // vahemikus 1-2018 ja tagastab täisarvu (byte) vahemikus 1-21 vastavalt sellele,
    // mitmendasse sajandisse antud aasta kuulub.
    // Meetodi nime võid ise välja mõelda.
    // Kui funktsioonile antakse ette parameeter, mis on suurme kui 2018 või väiksme kui 1,
    // tuleb tagastada väärtus -1.
    // Kutsu see meetod välja main()-meetodist järgmiste erinevate sisendväärtustega:
    // 0, 1, 128, 598, 1624, 1827, 1996, 2017. Prindi need väärtused välja

    static int inWhichCentury(int year) {
        //List<Integer> years = new ArrayList<Integer>();

        if(year > 2018 || year < 1) {
            return -1;
        }
        byte century = (byte) (year / 100 + 1); // saame kätte sajandi (int sööb komakohad ära

//        for (int i = 2018; i >= 1 ; i++) {
//            years.add(i);
//        }

        return century;
    }
    // 5. ülesanne
    // Defineeri klass Country, millel oleksid meetodid getPopulation,
    // setPopulation, getName, setName ja list riigis enim kõneldavate keeltega.
    // Override'i selle klassi toString() meetod nii, et
    // selle meetodi väljakutsumine tagastaks teksti, mis
    // sisaldaks endas kõiki parameetreid väljaprindituna.
    // Tekita antud klassist üks objekt ühe vabalt valitud riigi andmetega ja
    // prindi selle riigi info välja .toString() meetodi abil.



}
