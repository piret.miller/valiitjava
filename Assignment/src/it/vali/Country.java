package it.vali;

import java.util.List;

public class Country {
    private String population;
    private String name;
    private List<String> mostPopularLanguages;

    public String getPopulation() {
        return population;
    }

    public void setPopulation(String population) {
        this.population = population;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getMostPopularLanguages() {
        return mostPopularLanguages;
    }

    public void setMostPopularLanguages(List<String> mostPopularLanguages) {
        this.mostPopularLanguages = mostPopularLanguages;
    }
}
