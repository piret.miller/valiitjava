package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // Kõigepealt küsitakse külastaja nime
        // kui inimene on listis, siis öeldakse kasutajale "Tere tulemast, Piret"
        // ja küsitakse külastaja vanust.
        // Kui kasutaja on alaealine, siis teavitatakse teda, et sorry, sa ei saa sisse
        // muul juhul öeldakse, et "Meeldivat klubielamust!"

        // Kui kasutaja ei olnud listis,
        // küsitakse kasutajalt ka tema perekonnanime.
        // Kui perekonnanimi on listis, siis öeldakse tere tulemast, perenimi
        // muul juhul öeldakse, et "Ma ei tunne sind"

        String savedFirstName = "Piret";
        String savedLastName = "Miller";

        System.out.println("Mis on su eesnimi?");
        Scanner scanner = new Scanner(System.in);
        String guestFirstName = scanner.nextLine(); // Tõstutundlikkus? Kutsume välja toLowerCase või toUpperCase


        if (guestFirstName.toLowerCase().equals(savedFirstName.toLowerCase())) { // Stringide võrdlemiseks on Javas kasutusel .equals.
            // Otse stringe märgi == abil võrrelda ei saa! (mujal võib saada, nt C#-s)
            System.out.println("Tere tulemast, " + savedFirstName);
            System.out.println("Kui vana sa oled?");
            int age = Integer.parseInt(scanner.nextLine()); // Kuna küsime sisendit stringina, siis peame vastuse teisendama integeriks
            if (age < 18) {
                System.out.println("Sorry, oled veel liiga noor");
            } else {
                System.out.println("Meeldivat klubielamust!");
            }

        } else {
            System.out.println("Mhh.. Sinu eesnime meil listis ei ole.");
            if (!(guestFirstName.equals(savedFirstName))) {
                System.out.println("Mis on su perekonnanimi?");
                String guestLastName = scanner.nextLine();
                if (guestLastName.toUpperCase().equals(savedLastName.toUpperCase())) {
                    System.out.printf("Tere, %si sugulane, head klubielamust!%n", savedFirstName);
                } else {
                    System.out.println("Sind ma küll ei tunne!");
                }
            }
        }
    }
}
