package it.vali;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main {

    public static void main(String[] args) {
        int[] numbers = new int[5]; // Kui ma elementidele väärtuseid sisse ei anna, siis märgitakse vaikeväärtusteks 0, st prindib 5 nulli

        for (int i = 0; i <numbers.length; i++) {
            System.out.println(numbers[i]);
        }
	// Lisa massiivi 5 numbrit
        numbers[0] = -4;
        numbers[1] = 2;
        numbers[2] = 0;
        numbers[3] = 14;
        numbers[4] = 7;

        // eemalda siit teine number
        numbers[1] = 0;
        for (int i = 0; i <numbers.length; i++) {
            System.out.println(numbers[i]);
        }
        System.out.println();

        numbers[4] = numbers [3];
        numbers[3] = 5;

        for (int i = 0; i <numbers.length; i++) {
            System.out.println(numbers[i]);
        }
        System.out.println();

        numbers[3] = numbers[4];
        numbers[4] = 0;

        for (int i = 0; i <numbers.length; i++) {
            System.out.println(numbers[i]);
        }
        System.out.println();

        numbers[0] = numbers[3];
        for (int i = 0; i <numbers.length; i++) {
            System.out.println(numbers[i]);
        }
        System.out.println();

        int x = numbers[0];
        numbers[0]=numbers[3];
        numbers[3] = x;

        System.out.println();

        // Tavalisse arraylisti võin lisada ükskõik, mis tüüpi elemente
        // aga elemenete küsides sealt, pena ma teadma, mis tüüpi element
        // kus täpselt asub ning pean siis selleks tüübiks küsimisel ka castima

        List list = new ArrayList();
        list.add("tere");
        list.add(23);
        list.add(false);

        double money = 24.55;
        Random random = new Random(); // tavaline objekt, nagu car-gi

        list.add(money);
        list.add(random);

        for (int i = 0; i < list.size() ; i++) {
            System.out.println(list.get(i));
        }

        int a = (int)list.get(1); // peame ette kirjutama (int), muidu arvab, et on objekt
        String word = (String) list.get(0); // lambipirni alt "cast"

        int sum = 3 + (int)list.get(1);

        String sentence = list.get(0) + " hommikust"; // on piisavalt tark, et teisendab ise stringiks ja saab välja printida!
        System.out.println(sentence);

        if(Integer.class.isInstance(a)) {
            System.out.println("listis indeksiga 1 on int");
        }

        if(String.class.isInstance(list.get(0))) {
            System.out.println("listis indeksiga 0 on string");
        }

        // Aga selline kontrollimine on väga ajamahukas ja tülikas.
        // st pole mõtet ArrayListi kasutada,
        // kui ei ole teada kindlalt, kus mingi element asub.

        list.add(2, "head aega"); // läheb false (praegune index 2) asemele, false lükkub edasi ühe koha võrra
        for (int i = 0; i < list.size() ; i++) {
            System.out.println(list.get(i));
        }

        // list.addAll(new int[] { 1, 2, 3 }) //Massiivi lisada ei saa!

        List otherList = new ArrayList();
        // Saab ühe kaupa lisada...
        otherList.add(1);
        otherList.add("maja");
        otherList.add(true);
        otherList.add(2.342333);

        list.addAll(otherList); // .. ja saab ka kogu listi korraga lisada
        System.out.println();


        for (int i = 0; i < list.size() ; i++) {
            System.out.println(list.get(i));
        }

        // Ja saan ka ühte listi teise listi sisse panna:

        list.addAll(3, otherList); // teine list on listis alates indexist 3
        for (int i = 0; i < list.size() ; i++) {
            System.out.println(list.get(i));
        }

        if(list.contains(money)) { // nii saan ostida, ka slist sisaladab mingit konkreetset otsitavat
            System.out.println("money asub listis");
        }

        System.out.printf("money asub listis indeksiga %d%n", list.indexOf(money)); // Saan kätte indexi asukoha otsitavat fraasil
        System.out.printf("23 asub listis indeksiga %d%n", list.indexOf(23));
        System.out.printf("44 asub listis indeksiga %d%n", list.indexOf(44)); // kui ei leia otsitavat, tagastab -1

        list.isEmpty(); // kui list on tühi, tagastab true

        list.remove(money); // võrreldes massiiviga, kus on suht raske midgai kustutada, saab siin seda väga lihtsalt teha- kõik kohad nihkuva dlihtsalt edasi/tagasi
        for (int i = 0; i < list.size() ; i++) {
            System.out.println(list.get(i));
        }
        System.out.println();

        list.remove(5); // võrreldes massiiviga, kus on suht raske midgai kustutada, saab siin seda väga lihtsalt teha- kõik kohad nihkuva dlihtsalt edasi/tagasi
        for (int i = 0; i < list.size() ; i++) {
            System.out.println(list.get(i));
        }
        System.out.println();

        list.set(0, "tore"); // saab otse määrata uue väärtuse.
        // set kirjutab üle, add lisab juurde (vaikimisi lõppu, aga saab ka asukoha määrata)
        // saab asendada ka numbrit stringiga jne (P.S.- SEE VÕIB OHTLIK OLLA JA HILJEM SEGADUST TEKITADA, KUI KOOD NT KATKI LÄHEB VMS)
        for (int i = 0; i < list.size() ; i++) {
            System.out.println(list.get(i));
        }
        System.out.println();
    }
}
