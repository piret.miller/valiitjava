package it.vali;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class Main {

    public static void main(String[] args) {

        //The java.util.Map interface represents a mapping between a key and a value.
        // The Map interface is not a subtype of the Collection interface.
        // Therefore it behaves a bit different from the rest of the collection types.
        // A Map cannot contain duplicate keys and each key can map to at most one value.
        // Maps are perfect to use for key-value association mapping such as dictionaries.
        // The maps are used to perform lookups by keys or when someone wants to retrieve and update elements by keys. Some examples are:
        //
        //* A map of error codes and their descriptions.
        //* A map of zip codes and cities.
        //* A map of managers and employees. Each manager (key) is associated with a list of employees (value) he manages.
        //* A map of classes and students. Each class (key) is associated with a list of students (value).
        Map<String, String> linkedMap = new LinkedHashMap<String, String>();

        // Map järjekorda ei salvesta, st väljastab keysid ja valuesid suvalises (?) järjekorras
        // Kui tahame, et säiliks lisamise järjekord, kasutame LinkedHashMapi

        // Sõnaraamat
        // key => value
        // Maja => House
        // Isa => Dad
        // Puu => Tree

        linkedMap.put("Maja", "House"); // .add asemel on siin .put. map asemel nüüd linkedMap
        linkedMap.put("Isa", "Dad");
        linkedMap.put("Puu", "Tree");
        linkedMap.put("Sinine", "Blue");

        // Oletame, et tahan teada, mis on inglise keeles Puu

        String translation = linkedMap.get("Puu"); // get tagastab Stringi
        System.out.println(translation);

        Map<String, String> idNumberName = new HashMap<String, String>(); // Nüüd on Map, st ei tagastata tähestikulises ega sisestamise järjekorras
        idNumberName.put("48808220230", "Piret");
        idNumberName.put("49007160228", "Liisi");
        idNumberName.put("49007160228", "Kalle");

        // Kui kasutada put sama key lisamisel, kirjutatakse value üle (kuvandub viimane)
        // Key on unikaalne
        System.out.println(idNumberName.get("49007160228")); // Kuvatakse "Kalle" ehk viimane väärtus


        idNumberName.remove("48808220230");

        System.out.println(idNumberName.get("48808220230")); // Tagastab null, sest eemaldasime selle võtme

        // EST => Estonia
        // Estonia => +372

        // Loe lauses üle kõik erinevad tähed
        // ning prindi välja iga tähe järel, mitu tükki teda selles lauses oli

        // Char on selline tüüp, kus saab hoida üksikut sümbolit
        char symbolA = 'a';
        char symbolB = 'b';
        char newLine = '\n';


        String sentence = "elas metsas mutionu";
        Map<Character, Integer> letterCounts = new LinkedHashMap<Character, Integer>(); // koht, kus neid hoida, on nüüd olemas. HashMapi asemel panin nüüd LinkedHashMAp, siis prindib esinemise järjekorras tähed välja

        char[] characters = sentence.toCharArray(); // (?) Moodustame Chari jaoks Array nimega "characters" ja ütleme, et võtaks aluseks Strigni "sentence"
        for (int i = 0; i < characters.length; i++) {
            if(letterCounts.containsKey(characters[i])) { // kas Map lettercounts sisaldab võtmeid, mis on character Arrays?
                letterCounts.put(characters[i], letterCounts.get(characters[i]) + 1); // See rida küsib, mis enne väärtus oli (get.) ja liidab ühe juurde (.put)
            }else {
                letterCounts.put(characters[i], 1);
            }
        }
        // Map.Entry<Character, Integer> on klass, mis hoiab endas ühte rida map-is
        // ehk ühte key-value paari. Selleks ongi tüüp Entry loodud
        for (Map.Entry<Character, Integer> entry: letterCounts.entrySet()) { // Ei pea tegema kaks tsüklit korraga, saab ühes ja samas tsüklis küsida. Saab kasutada ainult mapi puhul, entry.
            System.out.printf("Tähte %s esines %d korda%n", entry.getKey(), entry.getValue());
        }
        System.out.println(letterCounts);


        // ekraanile prindib nii:
        // e 2
        // l 1
        // a 2
        // s 3
        // jne...
    }
}
