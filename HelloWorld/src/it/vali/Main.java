package it.vali;
// public -	tähendab, et class, meetod või muutuja on avalikult nähtav/ligipääsetav
// class - 	javas on see üksus/eraldi fail, mis sisaldab /grupeerib mingit funktsionaalsust
// 			Antud juhul classil "public" ei ole kohustuslik, sest see üks class ongi kogu programm.
// 			Kui aga classe on palju ja kui need ei ole public, siis ei saa classid omavahel suhelda!
// HelloWorld - klassi nimi, mis on Javas ALATI ka faili nimi
// static -	kui "static" kirjutada ette meetodile, tähendab see seda, et seda meetodit saab välja kutsuda ilma classist objekti loomata.
// 			Vastasel korral peame looma lisaks ka objekti.
// void - 	meetod ei tagasta midagi.
// 			Meetodile on võimalik kaasa anda ka parameetrid, mis pannakse sulgude sisse, eraldades komaga.
// String[]- tähistab stringi massiivi
// args -	massiivi nimi, sisaldab käsurealt kaasa pandud parameetreid
// System.out.println on Java meetod, millega saab välja printida rida teksti.
//			See, mis kirjutatakse sulgudesse jutumärkide sisse, prinditakse välja ja tehakse reavahetus
//Javas on classid alati suure algustähega.

public class Main {

    public static void main(String[] args) { // Javas on meetodid alati väikse tähega. Sulgudesse pannakse kirja meetodi parameetrid
	// Ka meetod algab ja lõppeb looksuluga. Main meetod peab alati olema public static void!
        System.out.println("Hello, World"); // Kõik asjad, mis mingit asja Javas teevad, lõppevad alati semikooloniga
    }
}
