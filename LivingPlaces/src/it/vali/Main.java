package it.vali;

public class Main {

    public static void main(String[] args) {

        LivingPlace livingPlace = new Farm();
        //livingPlace = new Zoo();



        Pig pig = new Pig();
        pig.setName("Kalle");
        Lion lion = new Lion();

        livingPlace.addAnimal(pig);
        livingPlace.addAnimal(lion);

        livingPlace.addAnimal(new Cat());
        livingPlace.addAnimal(new Horse());
        livingPlace.addAnimal(pig);
        livingPlace.addAnimal(new Pig());
        Cow cow = new Cow();
        cow.setName("Priit");

        livingPlace.addAnimal(cow);

        livingPlace.printAnimalCounts();
        livingPlace.removeAnimal("Cow");
        livingPlace.printAnimalCounts();
        livingPlace.removeAnimal("Pig");
        livingPlace.printAnimalCounts();
        livingPlace.removeAnimal("Pig");
        livingPlace.removeAnimal("Pig");
        livingPlace.printAnimalCounts();
        livingPlace.addAnimal(pig);
        livingPlace.printAnimalCounts();



        // Mõelge ja täiendage Zoo ja Forest klasse, et neil oleks nende kolme meetodi sisu
    }
}
