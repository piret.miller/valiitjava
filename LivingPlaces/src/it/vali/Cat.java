package it.vali;

public class Cat extends Pet{
    private boolean hasFur = true;


    public boolean isHasFur() {
        return hasFur;
    }

    public void setHasFur(boolean hasFur) {
        this.hasFur = hasFur;
    }

    public void catchMouse() {
        System.out.println("Püüdsin hiire kinni");
    }

    @Override
    public void eat() {
        System.out.println("Joon piima");
    }

    // Superiga ma saan otsida lähimat sugulast.
    // Otsib kõigeeal eelnevats klassist, kust ots epärineb.
    // Kui seal ei ole, siis vaatab veel eelnevat klassi,
    // kui sela ei ole, siis vaatab veel eelnevat klassi.
    // Otsib kogu ahela läbi, et leida sovivad printInfot.

    @Override
    public void printInfo() {
        super.printInfo(); // kutsu super (ehk kust ta pärineb, antud juhul Pet) välja (Petis kutsutakse omakorda super välja) Petis on nt "omaniku nimi" siin  välja kutsutud
        System.out.printf("Karvade olemasolu %s%n", hasFur); // Sega, Mainis lõpuks prindib välja mõlemad read-nii Pet kui Cati uued read
    }
}
