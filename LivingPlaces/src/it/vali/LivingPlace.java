package it.vali;

public interface LivingPlace {

    void addAnimal(Animal animal);
    void printAnimalCounts();
    void removeAnimal(String animalType);


}
