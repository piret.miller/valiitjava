package it.vali;

public class CarnivoreAnimal extends WildAnimal {
    private boolean hasClaws;

    public boolean isHasClaws() {
        return hasClaws;
    }

    public void setHasClaws(boolean hasClaws) {
        this.hasClaws = hasClaws;
    }
    public void eatMeat() {
        System.out.println("Mulle meeldib liha süüa");
    }

}
