package it.vali;

public class Dog extends Pet { // Lihtsalt infoks, et kui tahame nt klassi nime muuta (kirjutasime valesti vms), siis saame muuta nii, et klassi nime aktiivseks --> parem klõps --> refactor --> rename. Muudab igal pool ära (mis ongi õige)
    private boolean hasTail = true;

    public boolean isHasTail() {
        return hasTail;
    }

    public void setHasTail(boolean hasTail) {
        this.hasTail = hasTail;
    }

    public void playWithCat(Cat cat) {
        System.out.printf("Mängin kassiga %s%n", cat.getName()); // ei saa panna cat.name, sest see on privaatne muutuja. Seetõttu ongi vaja enne getter-setter teha ja saame get-iga selle nime tuua
    }

    @Override
    public void eat() {
        System.out.println("Närin konti");

    }

    @Override // Kirjytame koera vanuse üle
    public int getAge() {
        int age = super.getAge(); // Kutsun välja koera vanuse
        if (age == 0) { // Siin otsustan, et kui on 0, tagastab 1
            return 1;
        }
        return age;

    }

    public double getWeight() {
        if(weight == 0);
        return 1;
    }

    public void printInfo() {
        super.printInfo(); // kutsu super (ehk kust ta pärineb, antud juhul Pet) välja (Petis kutsutakse omakorda super välja) Petis on nt "omaniku nimi" siin  välja kutsutud
        System.out.printf("Saba olemas: %s%n", hasTail); // Sega, Mainis lõpuks prindib välja mõlemad read-nii Pet kui Cati uued read
    }


}
