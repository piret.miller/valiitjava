package it.vali;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Farm implements LivingPlace{
    private List<FarmAnimal> animals = new ArrayList<FarmAnimal>(); // List on interface, Arraylist on lihtsalt üks klass.
    // Seega neid ei saa panna omavahel võrduma! Aga ArrayList implementeerib Listist meetodeid

    // Siin hoitakse infot, palju loomi igas farmis on
    private Map<String, Integer> animalCounts = new HashMap<String , Integer>();

    // Siin hoitakse infot, palju meil igat looma farmis mahub
    private Map<String, Integer> maxAnimalCounts = new HashMap<String , Integer>();

    public Farm() { // teeme tühja konstruktori ja täidame maxAnimalCountsi ära
        maxAnimalCounts.put("Pig", 5);
        maxAnimalCounts.put("Cow", 3);
        maxAnimalCounts.put("Sheep", 15);

    }
    // Selleks, et farmi loomi lisada, tuleks teha lisamise meetodi


    @Override
    public void addAnimal(Animal animal) { // teeme public, et saaks väljaspool ka lisada. Esilagu tüübiks, et kõik loomad
        // Esmalt kontrollime et kas ikka on farmiloom
        // Kas animal on tüübist Farmanimal või pärineb sellest tüübist
        if(!FarmAnimal.class.isInstance(animal)) {
            System.out.println("Farmis saavad elada ainult farmi loomad");
            return; // kuna jõudsime kohta, kust edasi minna ei saa, saame paanna returni. Kui ei teeks returni, peaksime if-is minema aina sügavame sisse
        }

        String animalType = animal.getClass().getSimpleName(); // Saame küsida, kuhu klassi mingi loom kuulub
        //System.out.println(animalType);


        if (!maxAnimalCounts.containsKey(animalType)) {
            System.out.println("Farmis üldse sellistele loomadele kohta pole");
            return;
        }


        if(animalCounts.containsKey(animalType)) {
            int animalCount = animalCounts.get(animalType);
            int maxAnimalCount = maxAnimalCounts.get(animalType);
            if(animalCount >= maxAnimalCount) { // nii ei ole else plokki vaja
                System.out.println("Farmis on sellele loomale kõik kohad juba täis");
                return;
            }
            // animals.add((FarmAnimal)animal);
            animalCounts.put(animalType, animalCounts.get(animalType) + 1);
            // Sellist looma veel ei ole farmis
            // kindlasti sellele loomale kohta on
        } else {
            animalCounts.put(animalType, 1); // kogus on õige
        }
        animals.add((FarmAnimal)animal); // On listis olemas nüüd
        System.out.printf("Farmi lisati loom %s%n", animalType);
    }

    // Tee meetod, mis prindib välja kõik farmis elavad loomad ja mitu neid on
    // Pig 2
    // Cow 3
    @Override
    public void printAnimalCounts() {
        //Map<String, Integer> animalCounts = new HashMap<String, Integer>();
        for (Map.Entry<String, Integer> entry: animalCounts.entrySet()) { // Ei pea tegema kaks tsüklit korraga, saab ühes ja samas tsüklis küsida. Saab kasutada ainult mapi puhul, entry.
            System.out.println(entry.getKey() + " " + entry.getValue());
        }
        //System.out.println(animalCounts);
    }
    // Tee meetod, mis eemaldab farmist looma

    @Override
    public void removeAnimal(String animalType) {
        // kuidas leida nimekirjast loom, kelle nimi on Kalle?
        //Üks foreach variant on see:
//        for (FarmAnimal animal: animals) {
//            if(animal.getName().equals("Kalle")); {
//            }
//        }
        // Teine variant on see:
        boolean animalFound = false;
        for (FarmAnimal farmAnimal: animals) { // anna mulle listist Farmanimal üks animal ja pane talle nimeks animal
            if(farmAnimal.getClass().getSimpleName().equals(animalType)) {
                animals.remove(farmAnimal);
                System.out.printf("Farmist eemaldati loom %s%n", animalType);

                if(animalCounts.get(animalType) == 1) { // kuni viimas eloomani
                    animalCounts.remove(animalType); // kui viimane loom sellest tüübist eemaldatakse, siis eemaldatakse üldse mapist ära see tüüp

                } else {
                    animalCounts.put(animalType, animalCounts.get(animalType) - 1); // Kui teda oli 2 või rohkem, siis a lihtsalt vähndan kogust
                }
                animalFound = true;

                // Kui see oli viimane loom, siis eemalda see rida animalCounts mapist,
                // muul juhul vähenda animalCounts mapis seda kogust
                break; // nii kui esimese looma eemaldab, hüppab välja tsüklist. Kui break poleks, kustutaks kõik seda tüüpi loomad ära
            }
        }
        if(!animalFound) {
            System.out.println("Farmis antud loom puudub");
        }

        // For-tsükliga tehes oleks nii:
//        for (int i = 0; i < animals.size(); i++) {
//            if (animals.get(i).getClass().getSimpleName().equals(animalType)) {
//                animals.remove(animals.get(i));
//            }
//        }

    }
    // Täienda meetodit nii, et kui ei leitud ühtegi sellest tüübist looma
    // prindi "Farmis selline loom puudub".
}