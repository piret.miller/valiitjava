package it.vali;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Zoo implements LivingPlace {

    private List<Animal> animals = new ArrayList<Animal>(); // List on interface, Arraylist on lihtsalt üks klass.
    // Seega neid ei saa panna omavahel võrduma! Aga ArrayList implementeerib Listist meetodeid

    // Siin hoitakse infot, palju loomi igas farmis on
    private Map<String, Integer> animalCounts = new HashMap<String , Integer>();

    // Siin hoitakse infot, palju meil igat looma farmis mahub
    private Map<String, Integer> maxAnimalCounts = new HashMap<String , Integer>();

    public  Zoo() { // kostruktor
        maxAnimalCounts.put("Lion", 5);
        maxAnimalCounts.put("Fox", 3);
    }
    @Override
    public void addAnimal(Animal animal) {
        // Esmalt kontrollime et kas ikka onloom
        // Kas "animal" on tüübist Pet või pärineb sellest tüübist
        if(Pet.class.isInstance(animal)) { // isInstance tagastab kas true või false- kui on pet (Kõigepealt ette tüüp)
            System.out.println("Lemmikloomi loomaaeda vastu ei võeta");
            return; // kuna jõudsime kohta, kust edasi minna ei saa, saame paanna returni. Kui ei teeks returni, peaksime if-is minema aina sügavame sisse
        }
        String animalType = animal.getClass().getSimpleName(); // Igalt objektilt saame küsida,
        // kuhu klassi mingi loom kuulub
        //System.out.println(animalType);

        Class animalclass = animal.getClass(); // Saab küsida selle konkreets eklassi käest konkreetseid asju. it.vali.cat või it.vali.horse


        if (!maxAnimalCounts.containsKey(animalType)) {
            System.out.println("Loomaaias üldse sellistele loomadele kohta pole");
            return;
        }
        if(animalCounts.containsKey(animalType)) {
            int animalCount = animalCounts.get(animalType);
            int maxAnimalCount = maxAnimalCounts.get(animalType);
            if(animalCount >= maxAnimalCount) { // nii ei ole else plokki vaja
                System.out.println("Loomaaias on sellele loomale kõik kohad juba täis");
                return;
            }
            // animals.add((Animal)animal);
            animalCounts.put(animalType, animalCounts.get(animalType) + 1);
            // Sellist looma veel ei ole farmis
            // kindlasti sellele loomale kohta on
        } else {
            animalCounts.put(animalType, 1); // kogus on õige
        }
        animals.add((Animal)animal); // On listis olemas nüüd
        System.out.printf("Loomaaeda lisati loom %s%n", animalType);

    }

    @Override
    public void printAnimalCounts() {
        for (Map.Entry<String, Integer> entry: animalCounts.entrySet()) { // Ei pea tegema kaks tsüklit korraga, saab ühes ja samas tsüklis küsida. Saab kasutada ainult mapi puhul, entry.
            System.out.println(entry.getKey() + " " + entry.getValue());
        }

    }

    @Override
    public void removeAnimal(String animalType) {
        boolean animalFound = false;
        for (Animal animal: animals) { // anna mulle listist Animal üks animal ja pane talle nimeks animal
            if(animal.getClass().getSimpleName().equals(animalType)) {
                animals.remove(animal);
                System.out.printf("Loomaaiast eemaldati loom %s%n", animalType);

                if(animalCounts.get(animalType) == 1) { // kuni viimas eloomani
                    animalCounts.remove(animalType); // kui viimane loom sellest tüübist eemaldatakse, siis eemaldatakse üldse mapist ära see tüüp

                } else {
                    animalCounts.put(animalType, animalCounts.get(animalType) - 1); // Kui teda oli 2 või rohkem, siis a lihtsalt vähndan kogust
                }
                animalFound = true;

                // Kui see oli viimane loom, siis eemalda see rida animalCounts mapist,
                // muul juhul vähenda animalCounts mapis seda kogust
                break; // nii kui esimese looma eemaldab, hüppab välja tsüklist. Kui break poleks, kustutaks kõik seda tüüpi loomad ära
            }
        }
        if(!animalFound) {
            System.out.println("Loomaaias antud loom puudub");
        }

    }
}
