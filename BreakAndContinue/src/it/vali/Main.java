package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        // Küsime kasutajalt PIN-koodi.
        // Kui see on õige, siis ütleme "Tore!"
        // Kui on vale, siis küsime uuesti.
        // Kokku küsime 3 korda
        Scanner scanner = new Scanner(System.in);
        String realPin = "1234";

        for (int i = 0; i < 3; i++) {
            System.out.println("Palun sisesta pin-kood!");
            String enteredPin = scanner.nextLine(); // enam Scanner scanner = new Scanner(System.in) siia ei kirjuta, sest see on juba tsüklist välja kirjutatud (peabki olema nii)
            // Kui siin oleks continue, siis küsiks programm 3 korda "Palun sisesta pin-kood" ja seejärel ei teeks enam midagi.
            // Kui siin oleks break, siis hüppaks programm pärast 1-te küsimist otse for loopi lõppu ning samuti ei teeks enam midagi.
            if (enteredPin.equals(realPin)) {
                System.out.println("Tore! Pin-kood on õige!");
                break; // Break hüppab tsüklist välja
            }
        }

//        int retriesLeft = 3;
//        boolean pinWasCorrect = false;

//        do {
//            System.out.println("Palun sisesta pin-kood!");
//            String enteredPin = scanner.nextLine();
//            retriesLeft--; // vähendab pini sisestamise kordade arvu iga kord ühe võrra
//
//            // Üks variant, kuidas välja hüpata ilma breakita:
////            if(retriesLeft == 0);
////            break;
//
//            // Teine võimalus:
//            while (!scanner.nextLine().equals(realPin) && retriesLeft > 0) {
//
//                if (!scanner.nextLine().equals(realPin)) {
//                    System.out.println("Panid 3 korda vale pin-koodi!");
//                } else {
//                    System.out.println("Tore! Pin-kood on õige!");
//                }
//            }
//        }


        // prindi välja 10 kuni 20 ja 40 kuni 60

        // continue jätab selle tsüklikorduse katki ja läheb järgmise korduse juurde
        for (int i = 10; i <=60 ; i++) {
            if (i > 20 && i < 40) {
                continue; // kui tahan jätta midagi vahele, aga mitte välja hüpata, nt pärast 3. kordust panen continue, siis läheb programm edasi 4. korduse juures.
            } // Antud näitel tahame välja printida arvud 10 kuni 20 ja 40 kuni 60,
            // siis kui i vastab if tingimusele (on suurem kui 20 ja väiksem kui 40,
            // siis programm jätabki need arvud vahele ja jätkab 40-st.
            // Kui siin oleks break, siis prinditaks välja vaid 10-20.
            System.out.println(i);
        }
    }
}