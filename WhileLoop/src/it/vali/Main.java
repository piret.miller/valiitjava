package it.vali;

import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class Main {

    public static void main(String[] args) {
        // While tsükli puhul ei ole korduste arv teada.
        // While tsükkel kestab seni, kuni väärtus muutub ebatõeseks.
        // Seni kuni condition on tõene, tsükkel kordub lõpmatult.
        // Lõpmatu tsükkel, kasutades while loopi.
        // Kasutatakse nt siis, et "iga minut vaata, kas on uut meili" või "iga öö tee uuendus"

//        while (true) {
//            System.out.println("Tere");
//        }

        // Kuidas while loopi kasutada?
        // Nt on meil mäng, et programm mõtleb ühe numbri ja küsib kasutajalt "Arva number ära".
        //  Seni kaua kuni kasutaja arvab valesti, ütleb "Proovi uuesti!"

        // Võib ise määrata arvu int = 7;,
        // kuid allolevad näited õpetavad,
        // kuidas seda teha meetodi randomi abiga nii,
        // et programm ise mõtleb arvu välja.

        Scanner scanner = new Scanner(System.in);
        do {
            Random random = new Random(); // 1. samm- objekti loomine
            int number = random.nextInt(5) + 1; //random.nextInt(5) genereerib numbri 0-4. Seetõttu lisame +1.

            // 80 kuni 100
            // int number = random.nextInt(20) + 80

            // Teine võimalus sama asja teha:
            // int number = ThreadLocalRandom.current().nextInt(1, 6);
            // Eelis on see, et saab määrata ka soovitud vahemiku, kuhu random number võiks jääda

            //Pane tähele, et automaatselt annab see random int nimetuseks "randomNum".
            // Selle peab siis ise vastavalt kas ära muutma (hetkel nt muudetud "numberiks",
            // sest meie while-loopis on sees "number", või muutma while-loopis int nimetus randomNum-ks.

            System.out.println("Mõtlesin välja ühe numbri 1st 5ni. Arva see number ära!");
            // String enteredNumber = scanner.nextLine; eraldi ei loo,
            // vaid proovime kohe int enteredNumer = scanner.nextLine();).
            // Seda muidugi IntelliJ ei luba, vaid soovitab wrappida.
            // Teeme seda ja teisendame enteredName'i Stringist intiks!
            // Seega scanner.nextLine läheb juba Interger.parseInt parameetriteks.
            int enteredNumber = Integer.parseInt(scanner.nextLine());

            while (enteredNumber != number) {
                System.out.println("Vale number, arva uuesti");
                enteredNumber = Integer.parseInt(scanner.nextLine()); // seda osa on vaja seetõttu,
                // et peame andma While loopile uue tingimuse, mida võrrelda!
                // Küsimegi seda nüüd uuesti kasutajalt.
                // Tööpõhimõte sarnaneb counteriga i++
            }
            System.out.println("Õige number, sinu võit!");
            System.out.println("Kas soovid veel mängida?");
        } while (!scanner.nextLine().toLowerCase().equals("ei"));
    }
}
