package it.vali;

public class Main {

    public static void main(String[] args) {
        double a = Double.parseDouble("12.12"); // s: VIST näitab, et see, mis oleme sinna sisse pannud, on string
        String number = "123.2233";
        double b = Double.parseDouble(number);

        System.out.println(a); // Kui tahame näha Java documantationit, siis klikkame soovitud sõnal parem klõpus --> Go to --> Declaration
        System.out.println(b);

        number = String.valueOf(a); // Vastukaaluks nt Integer.parseInteger teisendusele teisendab see teisendus arvudest Stringe
        System.out.println(number);

        long c = 3400000000000L;
        number = String.valueOf(c);
        System.out.println(number);

        number = "-2";
        byte d = Byte.parseByte(number);
        System.out.println(d);

        short e = Short.parseShort(number);
        System.out.println(e);

        float f = Float.parseFloat(number);
        System.out.println(f);
    }
}
