package it.vali;

public class Main {

    public static void main(String[] args) { // -2, 4, 10, 1, 0, 5

        int [] numbers = new int[] { 1, -4, -10, -12, 16, 14 }; // Massiivi elemente saab kirja panna ka ühe reaga,
        // st ei pea panema ühe kaupa kirja

        int max = numbers[0]; // Antud juhul on max -2
        for (int i = 1; i < numbers.length; i++) { // paneme i = 1, sest 0 on juba enne max-na ära deklareeritud ja sellega pole vaja enam võrrelda
            if(numbers[i] > max) {
                max = numbers[i]; // Kui leiame praegusest max-st suurema elemendi, siis saab sellest uus max jne.
            }
        }
        System.out.println(max);

        int min = numbers[0]; // Antud juhul on min -2
        for (int i = 1; i < numbers.length; i++) { // paneme i = 1, sest 0 on juba ära pandud ja sellega pole vaja enam võrrelda
            if(numbers[i] < min) {
                min = numbers[i];
            }
        }
        System.out.println(min);

        // Leia suurim paaritu arv. Mitmes number see on?

        max = numbers[0]; // Antud juhul on see -2. PEAB kindlasti maxi uuesti defineerima, sest vastasel korral võtab For loop max-ks eelmise maxi, milleks oli kuskil üleval pool 10
        int position = 1; // Näitame, et hetkel on deklareeritud max positsioonil 1

        for (int i = 1; i < numbers.length; i++) { // paneme i = 1, sest 0 on juba ära pandud ja sellega pole vaja enam võrrelda
            if (numbers[i] > max && numbers[i] % 2 != 0) {
                    max = numbers[i];
                    position = i +1; // i annaks vaid indexi, kuid kuna indexi positsioon on alati ühe võrra suurem, siis liidame juurde +1
            }
        }
        System.out.printf("Suurim paaritu number on %d ja ta on järjekorras nr %d. %n", max, position);


        max = Integer.MIN_VALUE; // Aga kui nt suurim arv ongi miinusmärgiga? Siis ei saa väärtustada max-i 0-ga. Nt kui me ei tea, mis on väikseim Int (aga alguses peame ju max-ks määrama kõige esimese või väikseima arvu), saame kasutada väikseimat lõpmatust!
        position = 1;
        for (int i = 1; i < numbers.length; i++) {
            if (numbers[i] > max && numbers[i] % 2 != 0) {
                max = numbers[i];
                position = i +1;
            }
        }
        System.out.println(max);
        System.out.println();

        // Kui paarituid arve üldse ei ole, prindi, et paaritud arvud üldse puuduvad.
        // System.out.println("Paaritud arvud puuduvad");

        max = Integer.MIN_VALUE;
        position = 1;
        boolean oddNumbersFound = false; // Hea näide, kus booleani kasutada

        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] > max && numbers[i] % 2 != 0) {
                max = numbers[i];
                position = i +1;
                oddNumbersFound = true;
            }
        }
        if (oddNumbersFound) {
            System.out.printf("Suurim paaritu number on %d ja ta on järjekorras nr %d. %n", max, position);
        }
        else {
            System.out.println("Paaritud arvud puuduvad");
        }
        System.out.println();

        // Teine võimalus on kasutada väärtust -1. See tähendab, et massiivis pole sellist arvu:

        max = Integer.MIN_VALUE;
        position = -1;
//        oddNumbersFound = false; Seda pole vaja enam!

        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] > max && numbers[i] % 2 != 0) {
                max = numbers[i];
                position = i + 1;
//                oddNumbersFound = true;
            }
        }
        if (position != -1) {
            System.out.printf("Suurim paaritu number on %d ja ta on järjekorras nr %d. %n", max, position);
        }
        else {
            System.out.println("Paaritud arvud puuduvad");
        }

        System.out.println();
    }
}