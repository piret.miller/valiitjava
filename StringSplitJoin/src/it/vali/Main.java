package it.vali;

public class Main {

    public static void main(String[] args) {
        String[] words = new String[]{"Põdral", "maja", "metsa", "sees"}; // Koostame sõnade massiivi


        for (int i = 0; i < words.length; i++) {
            System.out.println(words[i]);
        }
        System.out.println();

        // Prindi kõik sõnad massiivist, mis algavad m-tähega

        //Selguse mõttes võib teha esmalt pikema versiooni


        for (int i = 0; i < words.length; i++) {
            String firstLetter = words[i].substring(0, 1);
            if (firstLetter.toLowerCase().equals("m"))
                System.out.println(words[i]);
        }
        System.out.println();

        // Kuid saab ka lühemalt:

        for (int i = 0; i < words.length; i++) {
            if (words[i].substring(0, 1).equals("m"))
                System.out.println(words[i]);
        }
        System.out.println();

        // Prindi kõik a-tähega lõppevad sõnad

        // Kõigepealt leiame sõna pikkuse:

//        String word = "kala";
//        int length = word.length();
//        System.out.println(length);

//
        String lastLetter;

        for (int i = 0; i < words.length; i++) {
            lastLetter = words[i].substring(words[i].length() - 1); // ehk sõna pikkusest, nt 4-st lahutame -1. Saame kätte viimase tähe indeksi
            if (lastLetter.equals("a")) {
                System.out.println(words[i]);
            }
        }
        System.out.println();


        // Loe üle kõik sõnad, mis sisaldavad a-tähte ja prindi välja nende sõnade arv

        int aCounter = 0;

        for (int i = 0; i < words.length; i++) {
            if (words[i].indexOf("a") != -1) {
                aCounter++;
            }
        }
        System.out.println(aCounter);
        System.out.println();


        // Prindi välja kõik sõnad, kus on 4 tähte

        for (int i = 0; i < words.length; i++) {
            if (words[i].length() == 4) {
                System.out.println(words[i]);
            }
        }
        System.out.println();

        // Prindi välja kõige pikem sõna

        String longestWord = words[0];
        for (int i = 1; i < words.length; i++) {
            if (words[i].length()>longestWord.length()) {
                longestWord = words[i];
            }
        }
        System.out.println(longestWord);
        System.out.println();

        // Prindi välja sõnad, kus on esimene ja viimane täht sama

        for (int i = 0; i < words.length; i++) {
            String firstLetter = words[i].substring(0,1);
            lastLetter = words[i].substring(words[i].length() -1);

            if(firstLetter.toLowerCase().equals(lastLetter))
                System.out.println(words[i]);
        }
        System.out.println();

        // aga saab ka nii:

        for (int i = 1; i < words.length; i++) {
            if(words[i].substring(0, 1).equals(words[i].substring(words[i].length() - 1))) {
                System.out.println(words[i]);
            }
        }
    }
}