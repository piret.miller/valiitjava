package it.vali;

public class Main {

    // Meetodid on mingid koodiplokid/koodi osad, mis grupeerivad mingit teatud
    // kindlat funktsionaalsust.

    // Kui koodis on korduvaid koodi osasid, võiks mõelda,
    // et äkki peaks nende kohta tegema eraldi meetodi

    public static void main(String[] args) {
        printHello();
        printHello(4);
        printText("Hei!");
        printText("Kuidas läheb?", 3);
        printText("2", 2); // Need on neli võimalust kutsuda välja meetod printText nelja erinevat moodi
        printText(2, "2");
        printText("hei!", 1, true);
        printText("hei!", 1, false);

    }
    // Lisame meetodi, mis prindib ekraanile "Hello"
    private static void printHello() {
        System.out.println("Hello");
    }

    // Lisame meetodi, mis prindib Hello etteantud arv kordi

    static void printHello(int howManyTimes) {
        for (int i = 0; i < howManyTimes ; i++) {
            System.out.println("Hello") ;
        }
    }

    // Lisame meetodi, mis prindib etteantud teksti välja
    // printText

    static void printText (String text) {
        System.out.println(text);
    }

    // Lisame meetodi, mis prindib etteantud teksti välja etteantud arv kordi
    // printText

    static void printText (String text, int howManyTimes) {
        for (int i = 0; i < howManyTimes ; i++) {
            System.out.println(text) ;
        }
    }

    static void printText (int year, String text) {
        System.out.printf("%d:, %s%n", year, text);
    }

    // Method OVERLOADING-  meil on mitu meetodit sama nimega,
    //                      aga erineva parameetrite kombinatsiooniga
    // Meetodi ülelaadimine
// Nt PrintText on neli erinevat võimalust välja printimseks. Nt on parameetrite järjekord muudetud


    // Lisame meetodi, mis prindib etteantud teksti välja etteantud arv kordi.
    // Lisaks saab öelda, kas tahame teha kõik tähed enne suurteks või mitte.

    static void printText (String text, int howManyTimes, boolean toUpperCase) {

        for (int i = 0; i < howManyTimes ; i++) {
            if(toUpperCase) {
                System.out.println(text.toUpperCase());
            }
            else {
                System.out.println(text);
            }
        }
    }
}