package it.vali;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

public class Main {

    static String pin;

    public static void main(String[] args) {
        // Hoiame PIN-koodi failis pin.txt
        // ja kontojääki balance.txt

        Scanner scanner = new Scanner(System.in);

        int balance = loadBalance();
        pin = loadPin();
        pin = "1234";

        // raha välja võtmine
        int amount = 100;
        balance -= 100;
        saveBalance(balance);


        int retriesLeft = 0;
        boolean entered = false;

        do {
            for (retriesLeft = 3; retriesLeft > 0; retriesLeft--) {
                System.out.printf("Sisesta PIN-kood%n");
                System.out.printf("Sul on jäänud %d katset", retriesLeft);
                String enteredPin = scanner.nextLine();
                entered = true;

                if (enteredPin.equals(pin)) {
                    System.out.println("Sisenesid pangaautomaati.");

                    break;
                } else {
                    System.out.println("Sisestatud PIN-kood on vale!");
                }
            }
            if (retriesLeft == 0) {
                System.out.println("Sinu pangakaart on blokeeritud!");
                break;
            }
        } while (!(entered));

        System.out.println("Vali soovitud toiming: ");
        System.out.println("a) Sularaha sissemakse");
        System.out.println("b) Sularaha väljamakse");
        System.out.println("c) PIN-koodi vahetus");
        System.out.println("d) Kontojäägi vaatamine");

        String usersChoise = scanner.nextLine();
        boolean wrongAnswer = false;

        switch (usersChoise) {
            case "a":
                int amount = Integer.parseInt(scanner.nextLine());

            case "b":
                System.out.println("Vali summa");
                System.out.println("5 €");
                System.out.println("10 €");
                System.out.println("20 €");
                System.out.println("Muu summa");
                String usersSumChoise = scanner.nextLine();
                String answer = scanner.nextLine();
                switch (answer) {
                    case "a":
                        amount = 5;
                        if(balance < amount) {
                            System.out.println("Pole piisavalt vahendeid");
                        }
                        else {
                            balance +=amount;
                            saveBalance(balance);
                            saveTransaction (amount, "Withdraw");

                        }
                        break;
                }

        }

            if (usersSumChoise.toLowerCase().equals("5")) {
                System.out.println("Väljastatud 5 eurot");

            // ...


        } else if (usersChoise.toLowerCase().equals("b")) {
            System.out.println("Sisesta raha");
            System.out.println("Kas soovid kviitungit ekraanile või paberile");
            String
        } else if (usersChoise.toLowerCase().equals("c")) {
        } else if (usersChoise.toLowerCase().equals("d")) {
        } else {
            System.out.println("Selline toiming puudub");
            wrongAnswer = true; // Ainus plokk, kus ta true's saab. Mujal jääb false'ks
        }

    }


    static void savePin(String pin){

        // pini muutmine

        pin = "2345";
        savePin(pin);

        // Toimub pin kirjutamine pin.txt faili
        try {
            FileWriter fileWriter = new FileWriter("pin.txt");
            fileWriter.write(pin + System.lineSeparator());
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("PIN-koodi salvestamine ebaõnnestus");
        }
    }
    static String loadPin() {
            // Toimub pin välja lugeminepin.txt failist
        // Toimub shadowing
        // Shadowing refers to the practice in Java programming of
        // using two variables with the same name within scopes that overlap.
        // When you do that, the variable with the higher-level scope is
        // hidden because the variable with lower-level scope overrides it.
        // The higher-level variable is then “shadowed.”

        String pin = null; // Pärast alumise ploki try-catchi sisse panekut pani IDE ise meile selle Stringi

        try {
            FileReader fileReader = new FileReader("pin.txt");
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            pin = bufferedReader.readLine();
            bufferedReader.close();
            fileReader.close();
        } catch (IOException e) {
            System.out.println("PIN-koodi laadimine ebaõnnestus");
        }
        return pin; // Kui tuli Exception, tagastab "nulli"
    }

    static void saveBalance(int balance){ // Meetodi nime muutmiseks (aktiivseks --> Refactor --> Rename. Saab korraga är amuuta nime

        // Toimub kontojäägi ülekirjutamine
        try {
            FileWriter fileWriter = new FileWriter("balance.txt");
            fileWriter.write(balance + System.lineSeparator());
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("Kontojäägi salvestamine ebaõnnestus");
        }
    }

    // Lisa konto väljavõtte funktsionaalsus.
    // System.out.println(currentDateTimeToString() + "Sularaha väljavõtt - 100 €"); // Näide, kuidas saame faili kuupäevaridu lisada

    static String currentDateTimeToString() { // Loome meetodi, et saaks hiljem meetodi lihtsalt välja kutsuda, et lisada juurd ekuupäev ja kellaaeg
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss"); // Kõigepealt annan formaadi
        Date date = new Date(); // Siis annan kuupäeva
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, 3);


        // System.out.println(dateFormat.format(date)); //2016/11/16 12:08:43 // Siis ütlen, mida annan kaasa. Meetodi sees EI PRINDI VÄLJA!
        return dateFormat.format(date);

    }

    static int loadBalance() {
            // Toimub balance välja lugemine balance.txt failist
        int balance = 0;
        try {
            FileReader fileReader = new FileReader("balance.txt");
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            balance = Integer.parseInt(bufferedReader.readLine());
            bufferedReader.close();
            fileReader.close();
        } catch (IOException e) {
            System.out.println("Kontojäägi laadimine ebaõnnestus");
        }
        return balance; // Kui tuli Exception, tagastab "nulli"

    }

    static boolean validatePin() {
        Scanner scanner = new Scanner(System.in);

        int retriesLeft = 0;

        for (retriesLeft = 3; retriesLeft > 0; retriesLeft--) {
            System.out.printf("Sisesta PIN-kood%n");
            System.out.printf("Sul on jäänud %d katset", retriesLeft);
            String enteredPin = scanner.nextLine();

            if (enteredPin.equals(pin)) {
                System.out.println("Sisenesid pangaautomaati.");
                return  true;

            } else {
                System.out.println("Sisestatud PIN-kood on vale!");
            }
        }
        if (retriesLeft == 0) {
            System.out.println("Sinu pangakaart on blokeeritud!");
            return false;
        }
        return false;
    }

    static void saveTransaction(int amount, String transaction) {
        try {
            FileWriter fileWriter = new FileWriter("transaction.txt", true);
            if(transaction.equals("Deposit")) {
                // Teine variant on kasutada String.formatit, on võimalik Stringi kokku panna
                // fileWriter.append(String.format("%d Raha sissemaks +%d%n" currentDateTimeToString(), amount));
                fileWriter.append(currentDateTimeToString() +"Raha sissemaks +" + amount + System.lineSeparator());
            }
            else {
                fileWriter.append(String.format("%s Raha väljamakse -%d%n", currentDateTimeToString(), amount));
            }
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("Tehingu salvestamine ebaõnnestus");
        }
    }
    static void printTransaction() {
        try {
            System.out.println("Konto väljavõte");
            FileReader fileReader = new FileReader("C:\\Users\\opilane\\Documents\\input.txt");
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            // readLine loeb iga kord välja kutsudes järgmise rea
            String line = bufferedReader.readLine();
            // Kui readLine avastab, et järgmist rida tegelikult ei ole, siis tagastab see meetod null


            while (line != null) { // Esmalt kontrollib, kas üldse on ridu failis
                System.out.println(line); // Prindib esimese rea
                line = bufferedReader.readLine(); // Paneb iga rea järgmise reaga võrduma
            } // Kui rida on failis null, siis hüppab tsüklist välja. Või kui fail ongi tühi, siis ei lähegi tsüklisse




            bufferedReader.close();
            fileReader.close();

        } catch (FileNotFoundException e) { // Alguses see...
            e.printStackTrace();
        } catch (IOException e) { // ... ja siis see, üldisem. On viisakas
            e.printStackTrace();
        }

    }
}
