package it.vali;

public class Dog extends Pet{ // Lihtsalt infoks, et kui tahame nt klassi nime muuta (kirjutasime valesti vms), siis saame muuta nii, et klassi nime aktiivseks --> parem klõps --> refactor --> rename. Muudab igal pool ära (mis ongi õige)
    private boolean hasTail = true;

    public boolean isHasTail() {
        return hasTail;
    }

    public void setHasTail(boolean hasTail) {
        this.hasTail = hasTail;
    }

    public void playWithCat(Cat cat) {
        System.out.printf("Mängin kassiga %s%n", cat.getName()); // ei saa panna cat.name, sest see on privaatne muutuja. Seetõttu ongi vaja enne getter-setter teha ja saame get-iga selle nime tuua
    }
}
