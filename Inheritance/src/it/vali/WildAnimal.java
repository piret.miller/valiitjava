package it.vali;

public class WildAnimal extends Animal {
    private boolean livesInWoods = true;

    public boolean isLivesInWoods() {
        return livesInWoods;
    }

    public void setLivesInWoods(boolean livesInWoods) {
        this.livesInWoods = livesInWoods;
    }
//    public void hideInBush(Cat cat) {
//        System.out.printf("Mängin kassiga %s%n", cat.getName()); // ei saa panna cat.name, sest see on privaatne muutuja. Seetõttu ongi vaja enne getter-setter teha ja saame get-iga selle nime tuua
//    }


}
