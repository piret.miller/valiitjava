package it.vali;

public class Pet extends DomesticAnimal {
    private String ownerName;

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }
    public void liveWithHuman() {
        System.out.println("Ma elan inimesega koos");
    }


}
