package it.vali;

public class Main {

    public static void main(String[] args) {
        Cat angora = new Cat();
        angora.setName("Miisu");
        angora.setAge(10);
        angora.setBreed("Angora");
        angora.setWeight(2.34);

        angora.printInfo();
        angora.eat();

        System.out.println();

        Cat persian = new Cat();
        persian.setName("Liisu");
        persian.setAge(1);
        persian.setBreed("Persian");
        persian.setWeight(3.11);

        persian.printInfo();
        persian.eat();

        System.out.println();

        Dog husky = new Dog();
        husky.setName("Luna");
        husky.setAge(3);
        husky.setBreed("Husky");
        husky.setWeight(15.5);

        husky.printInfo();
        husky.eat();

        System.out.println();

        FarmAnimal farmAnimal = new FarmAnimal();

        Cow cow = new Cow();
        cow.setName("Punik");
        cow.setAge(5);
        cow.setBreed("Punane");
        cow.setWeight(100);


        cow.printInfo();
        cow.eat();
        cow.giveMilk();

        System.out.println();

        Pig pig = new Pig();
        pig.setName("Notsu");
        pig.setAge(3);
        pig.setBreed("Villatõug");
        pig.setWeight(100);

        pig.printInfo();
        pig.eat();
        pig.bathInMud();
        pig.setLivesInFarm(true);


        System.out.println();

        persian.catchMouse();
        husky.playWithCat(persian);

	// Lisa pärinevusahelast puuduvad klassid
        // Igale klassile lisa 1 muutuja ja 1 meetod,
        // mis on ainult sellele klassile omane

        // Tehke objektid ja kutsuge välja meetodeid

        husky.setOwnerName("Piret");
        System.out.printf("%s omanik on %s%n", husky.getName(), husky.getOwnerName());

        persian.liveWithHuman();


    }
}
