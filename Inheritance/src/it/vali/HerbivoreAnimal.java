package it.vali;

public class HerbivoreAnimal extends WildAnimal {
    private boolean hasHorns;

    public boolean isHasHorns() {
        return hasHorns;
    }

    public void setHasHorns(boolean hasHorns) {
        this.hasHorns = hasHorns;
    }

    public void eatGrass() {
        System.out.println("Mulle meeldib rohtu süüa");
    }

}
