package it.vali;

// Lennuk laiendab/pärineb klassist Sõiduk ja kasutab/implementeerib liidest Flyer

public class Plane extends Vehicle implements Flyer, Driver{ // sunnin Plane'i omama Flyeris olevaid meetodeid
    private int maxDistance;


    @Override
    public void fly () {
        doChecklist();
        startEngine();
        System.out.println("Lennuk lendab");

    }
    private void doChecklist() {
        System.out.println("Täidetakse checklist");
    }

    private void startEngine() {
        System.out.println("Mootor käivitus");
    }

    @Override
    public void drive() {
        System.out.println("Lennuk sõidab");

    }

    @Override
    public int getMaxDistance() {
        return maxDistance;
    }

    @Override
    public void stopDriving(int afterDistance) {
        System.out.printf("Lennuk jäi pärast %d kilomeetrit seisma%n", afterDistance);


    }
}
