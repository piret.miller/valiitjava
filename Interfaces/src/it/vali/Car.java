package it.vali;

public class Car extends Vehicle implements Driver{

    private int maxDistance;
    private String make;


    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }


    @Override
    public void drive() { // Iterfaces määrad vaid, tagastus, para,meetrid
        System.out.println("Auto sõidab");
    }


    public Car() {
        this.maxDistance = 600;
    }

    public Car(int maxDistance) {
        this.maxDistance = maxDistance;
    }


    @Override
    public int getMaxDistance() {
        return maxDistance;
    }

    @Override
    public void stopDriving(int afterDistance) {
        System.out.printf("Sõiduk jäi pärast %d kilomeetrit seisma%n", afterDistance);

    }

    @Override
    public String toString() {
        return "Auto andmed: " + make + " " + maxDistance;
    }
}
