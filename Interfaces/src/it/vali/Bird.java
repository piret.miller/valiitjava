package it.vali;
// bird class kasutab/implementeerib liidest Flyer
public class Bird implements Flyer {
    @Override
    public void fly() {
        jumpUp();
        System.out.println("Lind lendab");

    }

    private void jumpUp() { // private, sest ei pea olema nähtav mujal. Mind ei huvita, et ta hüppab õhtu.Vaid seda on mul vaja teada, et ta lendab
        System.out.println("Lind hüppas õhtu");
    }
}
