package it.vali;

public interface Driver {
    void drive();
    int getMaxDistance();
    void stopDriving(int afterDistance);


}
