package it.vali;

public class Motorcycle extends Vehicle implements DriverInTwoWheels {

    private int maxDistance;

    @Override
    public void drive() {
        System.out.println("Mootorratas sõidab");
    }

    @Override
    public int getMaxDistance() {
        return getMaxDistance();
    }

    @Override
    public void stopDriving(int afterDistance) {
        System.out.printf("Mootorratas jäi pärast %d kilomeetrit seisma%n", afterDistance);
    }

    @Override
    public void driveInRearWheel() {

    }
}
