package it.vali;

// Interface ehk liides sunnib seda kasutavat/implementeerivat klassi omama
// liideses kirja pandud meetodeid (sama tagastuse tüübiga (void, int vms) ja sama parameetrite kombinatsiooniga)

// Iga klass, mis Interface'i kasutab, määrab ise ära meetodi sisu.
// Interfaceis sisu ei ole, on vaid tagastustüüp ja parameetrid.

public interface Flyer {

    void fly();


}
