package it.vali;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        // saan hoida lennukit ja lindu ühe smuutujas Flyer, olgugi, et neil on lendamiseks vaja teha täisti erinevaid toiminguid
        Flyer bird = new Bird();// panin muutuja tüübiks interdace'i enda, lõin interface'i tüüp objektid
        Flyer plane = new Plane();

        bird.fly();
        System.out.println();
        plane.fly();

        List<Flyer> flyers = new ArrayList<Flyer>(); // ... ja sana hoida täiesti erinevaid objekte ühes nimekirjas
        flyers.add(bird);
        flyers.add(plane);

        Plane boeing = new Plane();
        Bird pigeon = new Bird();
        flyers.add(boeing);
        flyers.add(pigeon);

        System.out.println(boeing);

        System.out.println();

        for (Flyer flyer: flyers) {
            flyer.fly();
            System.out.println();
        }

        Car car = new Car();
        car.stopDriving(200);

        Motorcycle motorcycle = new Motorcycle();
        motorcycle.stopDriving(250);

        car.setMake("Audi");
        System.out.println(car); // Print kutsub alati stringi välja


        // OK Lisa liides Driver, klass Car.
        // OK Mõtle, kas lennuk ja auto võiksid mõlemad kasutada Driver liidest?
        // OK Driver liides võiks sisaldada 3 meetodi struktuuri (interface ei sisalda ise meetodit, vaid meetodi struktuuri/kirjeldust) kirjeldust:
        // int getMaxDistance()
        // void drive()
        // void stopDriving(int afterDistance) - saad määrata, mitme meetri pärast lõpetab sõitmise
        //
        // Pane auto ja lennuk mõlemad kasutama seda liidest
        // Lisa mootorratas

    }
}
