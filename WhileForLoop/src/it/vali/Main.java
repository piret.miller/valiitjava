package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        for (int i = 1; i <= 5; i++) {
            System.out.println(i);
        }
        System.out.println();


// Ja nüüd sama asi while loopi abiga:
        int i = 1;
        while (i <= 5) {
            System.out.println(i);
            i++;
        }

        // Küsi kasutajalt, mis päev täna on
        // seni kuni ta ära arvab.

        Scanner scanner = new Scanner(System.in);
        String answer = ""; // Kui alguses määrame Stringi kohe tühjaks,
        // siis garanteerin kohe, et ta läheb tsükli sisse. Pmst simuleerin do while'i.

//        while (!answer.toLowerCase().equals("neljapäev")) {
//            System.out.println("Mõtlesin välja ühe päeva. Arva see päev ära!");
//            answer = scanner.nextLine(); // seda osa on vaja seetõttu, et peame andma While loopile uue tingimuse, mida võrrelda! Tööpõhimõte sarnaneb counteriga i++
//        }
//        System.out.println("Õige päev, sinu võit!");

        for ( ; (!answer.toLowerCase().equals("neljapäev")) ; ) { // for loopis on kõik kolm komponenti vabatahtlikud!
            System.out.println("Mõtlesin välja ühe päeva. Arva see päev ära!");
            answer = scanner.nextLine();
        }
        System.out.println("Õige päev, sinu võit!");
    }
}
