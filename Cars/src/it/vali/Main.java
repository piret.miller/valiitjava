package it.vali;

public class Main {

    public static void main(String[] args) {
        Car bmw = new Car(); // See ongi konstruktori väljakutsumine, meetod kutsub välja konstruktori
        bmw.startEngine();
        bmw.startEngine();
        bmw.stopEngine();
        bmw.stopEngine();
        bmw.accelerate(100);
        bmw.startEngine();
        bmw.accelerate(100);

        Car fiat = new Car();
        Car mercedes = new Car(); // Iga kord, kui objekti loon, kutsutakse konstruktor välja
        Car opel = new Car("Opel", "Vectra", 1999, 205, 5);
        opel.startEngine();
        opel.accelerate(205);
        Car peugeot = new Car(Fuel.DIESEL, true);
        peugeot = new Car ("Peogeot", "308", 2013, 180, 5 );
        peugeot.slowDown(50);
        peugeot.startEngine();
        peugeot.slowDown(50);
        peugeot.accelerate(100);
        peugeot.startEngine();
        peugeot.accelerate(150);
        peugeot.slowDown(50);
        peugeot.parking();
        Car seat = new Car("Seat", "Toledo", 2015, 200, 5);


        Person person1 = new Person("Piret", "Miller", Gender.FEMALE, 30);
        Person person2 = new Person("Mari", "Maasikas", Gender.FEMALE, 35);
        Person person3 = new Person("Mart", "Murakas", Gender.MALE, 20);
        Person person4 = new Person("Karl", "Suur", Gender.MALE, 25);
        Person person5 = new Person("Laura", "Laul", Gender.FEMALE, 27);
        Person person6 = new Person("Tom", "Katus", Gender.MALE, 37);


        seat.setDriver(person1);
        seat.setOwner(person2);
        System.out.printf("Sõiduki %s omaniku vanus on %d%n", seat.getMake(), seat.getOwner().getAge()); // Enne ei saa välja printida,
        // kui mul pole loodud uus objekt Car seat = new Car ja selle sulgudesse sisse pandud väärtused!

        opel.setMaxPassengers(5);
        seat.setMaxPassengers(5);

        opel.addPassengers(person3);
        opel.addPassengers(person4);

        opel.showPassengers();

        opel.removePassenger(person3);
        opel.showPassengers();

        Person juku = new Person("Juku", "Juurikas", Gender.MALE, 23);
        Person malle = new Person("Malle", "Maasikas", Gender.FEMALE, 30);

        System.out.println(juku);
        System.out.println(malle);

        System.out.println(opel); // ei näita miskipärats!!!! Tuleb cars-si meetod üle kirjutada!

        String word = "tere";
        String newWord = word + juku;
        System.out.println(newWord);


    }
}
