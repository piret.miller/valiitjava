package it.vali;

enum Gender {
    FEMALE, MALE, NOTSPECIFIED
}

public class Person {
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    private String firstName;
    private String lastName;
    private Gender gender;
    private int age;

    // Kui klassil ei ole defineeritud konstruktorit, siis tegelikult tehakse
    // nähtamatu parameetrita konstruktor, mille sisu on tühi.
    // Kui klassile ise lisada mingi konstruktor,
    // siis see nähtamatu parameetrita konstruktor kustutatakse.
    // Sellest klassist saab siis teha objekti ainult selle uue konstruktoriga:

//    public Person () {
//
//    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Person(String firstName, String  lastName, Gender gender, int age) {
        // konstruktor- public ette ja klassi nimi (pole voidi ega inti vms)
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.age = age;
    }

    @Override
    public String toString() {
        return String.format("Eesnimi on: %s, perekonnanimi on: %s, vanus on: %d", firstName, lastName, age); // pole %n vaja panna, sest String.format ise paneb selle
    }
}
