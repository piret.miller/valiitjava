package it.vali;

import java.sql.Driver;
import java.util.ArrayList;
import java.util.List;

enum Fuel { GAS , PETROL, DIESEL, HYBRID, ELECTRIC
}

public class Car {
    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    private String make;
    private String model;
    private int year;
    private Fuel fuel;
    private boolean isUsed;
    private boolean isEngineRunning;
    private int speed;
    private int maxSpeed;
    private Person driver; // Array pole siin hea lahendus, et nt luua Personeid, sest peaksime kohe ette teadma ja andma isikute arvu
    private Person owner;
    private int maxPassengers;
    
    
    public int getMaxPassengers() {
        return maxPassengers;
    }

    public void setMaxPassengers(int maxPassengers) {
        this.maxPassengers = maxPassengers;
    }


    private List<Person> passengers = new ArrayList<Person>();
    
    
    




    // Konstruktor constructor
    // on eriline meetod, mis käivitatakse klassist objekti loomisel
    // alguses on klass Car, mis on žabloon või mudel, et tee nende näitajate järgi auto.
    // Sellel klassil on konstruktor (koostaja, kokkupanija), mis ütleb, et kutsu selliste ja selliste omadustega auto välja.
    // Kui loon objekti, siis saan välja kutsuda selle objekti meetodeid (opel.slowDown jne)
    public Car() { // Selle konstruktori lõime küll ise, kuid kui ei looks, siis koostatakse vaikimisis tühi konstruktor
        System.out.println("Loodi auto objekt");
        maxSpeed = 200; // võib ka this.maxSpeed, kuid IDE saab ka ilma this.ita aru, millist muutujat mõtleme
        isUsed = true;
        fuel = Fuel.PETROL;
    }

    // Constuctor overloading
    public Car(String make, String model, int year, int maxSpeed, int maxPassengers) {
        this.make = make;
        this.model = model;
        this.year = year;
        this.maxSpeed = maxSpeed;

    }

    public Car (Fuel fuel, boolean isUsed) {
        this.fuel = fuel;
        this.isUsed = isUsed;
    }
    public Car (Person driver, Person owner) {
        this.driver = driver;
        this.owner = owner;
    }

    public void startEngine() {
        if(!isEngineRunning) {
            isEngineRunning = true;
            System.out.println("Mootor käivitus");
        } else {
            System.out.println("Mootor juba töötab");
        }
    }
    public void stopEngine() {
        if(isEngineRunning) {
            isEngineRunning = false;
            System.out.println("Mootor seiskus");
        } else {
            System.out.println("Mootor ei töötanudki");
        }
    }
    public void accelerate(int targetSpeed) {
        if(!isEngineRunning) {
            System.out.println("Auto mootor ei tööta, ei saa kiirendada");
        } else if (targetSpeed > maxSpeed) {
            System.out.println("Auto nii kiiresti ei sõida");
            System.out.printf("Auto maksimum kiirus on %d%n", maxSpeed);
        } else if (targetSpeed < 0) {
            System.out.println("Auto kiirus ei saa olla negatiivne");
        } else if (targetSpeed < speed) {
            System.out.println("Auto ei saa kiirendada madalamale kiirusele");
        } else {
            speed = targetSpeed;
            System.out.printf("Auto kiirendas kuni kiiruseni %d%n", targetSpeed);
        }
    }

    // Alates 06.05.2019
    public void slowDown (int targetSpeed) {
        if(!isEngineRunning) {
            System.out.println("Auto mootor ei tööta, ei saa aeglustada");
        } else if (targetSpeed > speed) {
            System.out.println("Auto ei saa aeglustada suuremale kiirusele kui praegune kiirus");
        } else if (targetSpeed < 0) {
            System.out.println("Auto lõppkiirus ei saa olla negatiivne");
        } else {
            speed = targetSpeed;
            System.out.printf("Auto aeglustas kuni kiiruseni %d%n", targetSpeed);
        }
    }
    public void parking () { // VAADATA ÕPETAJA NÄIDET!
        slowDown(0);
        if (speed == 0){
            isEngineRunning = false;
            System.out.println("Parkisid auto ära");
        }
    }

    // Lauri lahendus:
    public void park() {
        slowDown(0);
        stopEngine();
    }

    public Person getDriver() {
//        if(driver.equals("")) {
//            System.out.println("Juht on määramata");
//        }
        return driver;
    }
    public void setDriver(Person driver) {
        this.driver = driver;

    }
    public Person getOwner() {
//        if(owner.equals("")) {
//            System.out.println("Omanik on määramata");
//        }
        return owner;
    }
    public void setOwner(Person owner) {
        this.owner = owner;
    }

    public void addPassengers (Person passenger) { // parameetriks tüüp Person (üks klass Person, juhina omanikuna jne), muutujaks passenger
        if(passengers.size() < maxPassengers) {
            if (passengers.contains(passenger)) {
                System.out.printf("Autos juba on reisija %s%n", passenger.getFirstName());
                return; // return tähendab, et meetod lõpetab oma töö, ei jõua enam lõppu
            } else {
                passengers.add(passenger);
                System.out.printf("Autosse lisati sõitja %s%n", passenger.getFirstName()); // Tuleb enne Person klassis teha firtsName-le getter-setter
            }
        } else {
            System.out.println("Auto on juba täis");

        }
    }
    
    public void removePassenger (Person passenger) {
        if(passengers.indexOf(passenger) != -1) {
            System.out.printf("Autost eemaldati reisija nimega %s%n", passenger.getFirstName());
            passengers.remove(passenger);
        } else {
            System.out.println("Autos sellist reisijat ei ole");
        }
    }
    
    public void showPassengers() {
        // Foreach loop
        // Iga elemendi kohta listis passengers tekita objekt passenger
        // 1. kordus Person passenger on esimene reisija
        // 2. kordus Person passenger on teine reisija
        System.out.println("Autos on järgnevad reisijad:");
        for (Person passenger : passengers) { // see tükkel ongi mõeldud listide läbimiseks. Pärast koolonit tuleb listi nimi
            System.out.println(passenger.getFirstName());
        }
//        // sama asi for-tsükliga: (näeme, et foreach on selge ja paremini loetav)
//        for (int i = 0; i < passengers.size(); i++) {
//            System.out.println(passengers.get(i).getFirstName());
//        }
    }



    // slowDown (int targetSpeed)
    // park() mis tegevused oleks vaja teha (kutsu välja juba olemasolevaid meetodeid)
    // (aeglustuma 0-ni, mootori välja lülitama)
    // Lisa autole parameetrid driver ja owner (tüübist Person) ja nendele siis get ja set meetodid
    // Loo mõni auto objekt, kellel on määratud kasutaja ja omanik.
    // Prindi välja auto omaniku vanus

    // Lisa autole max reisijate arv (OK)
    // Lisa autole võimalus hoida reisijaid
    // Lisa meetodid reisijate lisamiseks ja eemaldamiseks autost
    // Kontrolli ka, et ei lisaks rohkem reisijaid kui mahub
}
