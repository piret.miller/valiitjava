package it.vali;

public class Animal extends Object{ // vaikimisi on javas olemas klass Object. Muutujad:
    private String name;
    private String breed;
    private int age;
    protected double weight; // protected tähendab, et on nähtav päritavate klasside sees (muidu ei oleks)

    public String getName() {
        if(name == null) {
            return "puudub";
        }
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public void eat() {
        System.out.println("Söön toitu");
    }

    public void printInfo() { // siia ei saa kirjutada kassile spetsiifilisi meetodeid, ainult üldiseid saab!
        System.out.println("Info: ");
        System.out.printf("Nimi %s%n", getName());
        System.out.printf("Tõug %s%n", breed);
        System.out.printf("Vanus %s%n", age);
        System.out.printf("Kaal %.2f%n", weight);


    }
}
