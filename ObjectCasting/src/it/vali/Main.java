package it.vali;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        // Suuremast väikemass esaab otse
        int a = 100;
        short b = (short)a;

        a = b;

        double c = a;
        a =(int)c;

        // Iga kassi võib võtta kui looma. Vastupidi aga ei saa, ilma castimata
        // implicit casting
        Animal animal = new Cat(); // Saame panna Animal tüüpi pärinevaid tüüpe

        // Iga loom ei ole kass:
        // explicit casting
        Cat cat = (Cat)animal; // Ainult siis saab tagasi castida, kui on olnud enne kass

        List<Animal> animals = new ArrayList<Animal>();


        // Saan listis hoida erinevaid loomi
        // kõiki, kes pärinevad Animalist

        Dog dog = new Dog();
        dog.setName("Naki");
        Fox fox = new Fox();

        animals.add(dog);
        dog.setName("Muki");
        animals.add(fox);
        animals.add(new Lion()); // Saab kohe uue objekti öluua ja listi kohe panna
        animals.get(animals.size() -1).setWeight(100); // vahetult pärast objekti lisamist listi SAAB muuta muutujaid! Pärast aga peak sjuba mingi tracki looma
        animals.add(new Pet());
        ((Pet)animals.get(animals.size() -1)).setOwnerName("Juhan"); // peab taas teisendama enne. Alles siis saab setOwnerName määrata

        animals.get(animals.size() -1).setName("Jaan"); // vahetult pärast objekti lisamist listi SAAB muuta muutujaid!

        animals.get(3).setName("Jaan"); // Kui teame peti indeksit, saame ka otse määrata indeksi numbri


        fox.setBreed("Hõberebane");

        animals.get(animals.indexOf(dog)).setName("Peeter"); // küsin koera (objekti) indeksi, siis küsin koera välja ja siis muudan ots enime ära. Nüüd on kõikide koerte nimi Peeter
        dog.printInfo();

        System.out.println();


        // Kutsu kõikide listis olevate loomade printInfo välja

        for (Animal animalInList: animals) { // tüüp Animal animalList, kust võtame: animalsis
            animalInList.printInfo();
            System.out.println();

        }
        for (Animal animalInList: animals) { // paneme kõik sööma
            animalInList.eat();
            System.out.println();
        }

        Animal secondAnimal = new Dog(); // Kuigi Dogil peaksid olemas olema nii Animali JA ka koera omadused, pole see seda mitte
        ((Dog)secondAnimal).setHasTail(true); // seetõttu peame teisendama tagsi uuesti koeraks.

        secondAnimal.printInfo();
    }
}
