package it.vali;

import java.util.List;

public class Language {
    private String languageName;
    private List<String> countryNames;


    public List<String> getCountryNames() {
        return countryNames;
    }

    public void setCountryNames(List<String> countryNames) {
        this.countryNames = countryNames;
    }

    public String getLanguageName() {
        return languageName;
    }

    public void setLanguageName(String languageName) {
        this.languageName = languageName;
    }

    @Override
    public String toString() {
        return String.join(", ", countryNames);
    }
}
