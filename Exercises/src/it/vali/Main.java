package it.vali;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
	// 1. ülesanne
        // Arvuta ringi pindala, kui teada on raadius
        // Prindi pindala välja ekraanile

        double radius = 10.25;
        System.out.printf("Ringi raadiusega %.2f pindala on %.2f cm2%n", radius, circleArea(radius));


        // 2. ülesanne
        // Kirjuta meetod, mis tagastab boolean-tüüp väärtuse ja mille
        // sisendparameetriteks on kaks stringi.
        // Meetod tagastab, kas tõene või vale selle kohta,
        // kas Stringid on võrdsed

        // Saab mitut moodi välja kuvama panna, nt enne luues String ja booleani muutujad ja need väärtustades.
        // boolean areEqual = equals(text, anotherText);

        System.out.println(equalTexts("Tere", "Piret"));

        // 3. ülesanne
        //Kirjuta meetod, mille sisendparameetriks on täisarvude massiiv
        // ja mis tagastab stringide massiivi.
        // Iga masiiivi elemenedi kohta olgu tagastatavas massivis sama palju a tähi.
        // kui massiiv on 3, 6, 7, hakkab lõpuks stringide massiiivis olema aaa, aaaaaa, aaaaaaa.

        int[] numbers = new int[] {3, 6, 7};
        String[] words = intArrayToStringArray(numbers);
        for (int i = 0; i < words.length; i++) {
            System.out.println(words[i]);
        }

        // 4. ülesanne
        // Kirjuta meetod, mis võtab sisendparameetrina aastaarvu
        // ja tagastab kõik sellel sajandil esinenud liigaastad.
        // Sisestada saab ainult aastaid vahemikus 500-2019.
        // Ütle veateade, kui aastaarv ei mahu vahemikku.

//        for (int year:leapYearsinCentury(1897)) {
//
//        }
        List<Integer> years = leapYearsinCentury(1400);
        for (int year : years) {
            System.out.println(year);

        }


        // 5. ülesanne
        // Defineeri klass Language, sellel klassil getLanguageName, setLanguageName ning list riikide nimedega,
        // kus seda keelt räägitakse. Kirjuta üle selle klassi meetod toString() nii,
        // et see tagastab riikide nimekirja, eraldades komaga.
        // Tekita antud klassist üks objekt ühe vabalt valitud keele andmetega ning prindi välja
        // selle objekti toString() meetodi sisu

        Language language = new Language();
        language.setLanguageName("English");


        List<String> countryNames = new ArrayList<String>();
        countryNames.add("USA");
        countryNames.add("UK");
        countryNames.add("Australia");
        countryNames.add("India");

        language.setCountryNames(countryNames);

        System.out.println(language.toString()); // aga kui jätame .toString() ära, siis println teeb selle meie eest (kutsub ise Stringi välja) ja prindib ikkagi välja





        // 1. ülesanne
        // Arvuta ringi pindala, kui teada on raadius
        // Prindi pindala välja ekraanile
    }
    static double circleArea(double radius) {
        return radius * radius * Math.PI;
        // või ka return Math.PI * Math.pow (radius, 2)
    }

    // 2. ülesanne
    // Kirjuta meetod, mis tagastab boolean-tüüpi väärtuse ja mille
    // sisendparameetriteks on kaks stringi.
    // Meetod tagastab, kas tõene või vale selle kohta,
    // kas Stringid on võrdsed

//    static boolean equalTexts (String text, String anotherText) {
//        //boolean isEqual;
//        if(text.equals(anotherText)) { // if returniga ei ole else vaja!
//            //isEqual = true;
//            return true;
//        }         //isEqual = false;
//            return false;
//        //return isEqual;
//    }

    // Lühem variant:
    static boolean equalTexts (String text, String anotherText) {
        return text.equals(anotherText);
    }


        // 3. ülesanne
    //Kirjuta meetod, mille sisendparameetriks on täisarvude massiiv
    // ja mis tagastab stringide massiivi.
    // Iga massiivi elemenedi kohta olgu tagastatavas massiivis sama palju a tähi.
    // kui massiiv on 3, 6, 7, hakkab lõpuks stringide massiiivis olema aaa, aaaaaa, aaaaaaa.

    static String[] intArrayToStringArray(int[] numbers) {
        String [] words = new String[numbers.length]; // Kui palju sõnu tuleb? Sama palju kui numbreidki

        for (int i = 0; i < words.length ; i++) { // aga võib olla ka numbers.length
           words[i] = generateAString(numbers[i]);
        }
        return words;
    }

    static String generateAString(int count) {
        String word = ""; // alguses on tühi
        for (int i = 0; i < count; i++) {
            word = word + "a";
        }
        return word;
       // return "a".repeat();
    }

    // 4. ülesanne
    // Kirjuta meetod, mis võtab sisendparameetrina aastaarvu
    // ja tagastab kõik sellel sajandil esinenud liigaastad.
    // Sisestada saab ainult aastaid vahemikus 500-2019.
    // Ütle veateade, kui aastaarv ei mahu vahemikku.

    // 1799
    static List<Integer> leapYearsinCentury(int year) {
        List<Integer> years = new ArrayList<Integer>(); // Loome esialgu tühja listi, kus aastaid hoida

        if(year < 500 || year > 2019) {
            System.out.println("Aasta peab olema 500 ja 2019 vahel");
            return years;
        }
        int centuryStart = year / 100 * 100;
        int centuryEnd = centuryStart + 99;
        int leapYear = 2020;


        for (int i = 2020; i >= centuryStart ; i-=4) {
            if(i <= centuryEnd && i % 4 == 0) {
                years.add(i); // Lisamine toimub alles siin, kui ma neid päriselt leian

            }

        } return years;


    }




    // 5. ülesanne
    // Defineeri klass Language, sellel klassil getLanguageName, setLanguageName ning list riikide nimedega,
    // kus seda keelt räägitakse. Kirjuta üle selle klassi meetod toString() nii,
    // et see tagastab riikide nimekirja, eraldades komaga.
    // Tekita antud klassist üks objekt ühe vabalt valitud keele andmetega ning prindi välja
    // selle objekti toString() meetodi sisu




}
