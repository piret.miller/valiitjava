package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        System.out.println("Sisesta number");

        Scanner scanner = new Scanner(System.in);
        int a = Integer.parseInt(scanner.nextLine());

        // Java on Type-safe language, st tüübikindel.
        // Igal muutujal on alati tüüp. Ei saa teha int a = 2.23 ja siis edasi a = "Tere" (Javascriptis ja Pythonis nt saab nii!)

        if(a == 3) { // looksulud ei ole tegelikult kohustuslikud,
            // töötab ka ilma, aga ainult siis, kui järgneb vaid üks tingimus. Viisakas on aga panna looksulud
            System.out.printf("Arv %d on võrdne kolmega%n", a);
        }

        if (a < 5) {
            System.out.printf("Arv %d on väiksem viiest%n", a);
        }

        if (a != 4) {
            System.out.printf("Arv %d ei võrdu neljaga%n", a);
        }

        if (a > 2) {
            System.out.printf("Arv %d on suurem kahest%n", a);
        }

        if (a >= 7) {
            System.out.printf("Arv %d on suurem või võrdne seitsmega%n", a);
        }

        if (a <= 7) {
            System.out.printf("Arv %d on väiksem või võrdne seitsmega%n", a);
        }

        if (!(a >= 7)) {
            System.out.printf("Arv %d ei ole suurem või võrdne seitsmega%n", a);
        }

        // arv on 2 ja 8 vahel
        if (a > 2 && a < 8) {
            System.out.printf("Arv %d on suurem kui kaks ja väiksem kui kaheksa%n", a);
        }

        // arv on väiksem kui 2 või arv on suurem kui 8
        if (a < 2 || a > 8) {
            System.out.printf("Arv %d on väiksem kui kaks või suurem kui kaheksa%n", a);
        }

        // kui arv on 2 ja 8 vahel või arv on 5 ja 8 vahel või arv on suurem kui 10
        if ((a > 2 && a < 8) || (a > 5 && a < 8) || (a > 10)) { // antud juhul töötaks ka ilma sulgudeta, sest && arvutatakse enne ||
            System.out.printf("Arv %d on 2 ja 8 vahel või 5 ja 8 vahel või suurem kui 10%n", a);
        }

        // kui arv ei ole 4 ja 6 vahel, aga on 5 ja 8 vahel või arv on negatiivne, aga pole suurem kui 14
        if ((!(a > 4 && a < 6) && (a > 5 && a < 8)) || ( a < 0 && !(a > 14))) {
            System.out.printf("Arv %d ei ole 4 ja 6 vahel, aga on 5 ja 8 vahel või arv on negatiivne, aga pole suurem kui 14%n", a);
        }

        // Kui tingimuste vahel on &&, siis kui üks tingimus on vale, siis edasi järgmisi tingimusi enam ei vaadata.
        // Kui kõikide tingimuste vahel on ||, siis piisab, kui kas või üks tingimus on tõene, edasi enam ei vaadata.
    }
}
