package it.vali;

public class Main {

    public static void main(String[] args) {
	// Casting on pmst teisendamine ühest arvutüübist teise
        byte a = 3;

        // Kui üks numbritüüp mahub teise sisse, siis toimub automaatne teisendamine.
        // ingl implicit casting
        short b = a;

        // Kui üks numbritüüp ei pruugi mahtuda teise sisse,
        // siis peab kõigepealt ise veenduma, et see number mahub sinna teise numbritüüpi
        // ja kui mahub, siis peab ise seda teisendama
        // ingl explicit casting

        short c = 300; // short mahutab 16 biti, -32768 kuni 32767
        byte d = (byte)c; // byte mahutab 8 biti, -128 kuni 127.

        // Kuna aga arv 300 ei mahu byte sisse ära, kuvab tulemuseks 44.
        // Programm hakkab otsast peale lahutama: 300 - 128 -128 jne. Esimene positiivne arv on 44:

        System.out.println(d);

        long e = 10000000000L;
        int f = (int) e;

        System.out.println(f);

        long g = f; // alati tasub väiksemast suuremasse numbrisse panna, sest peab olema veeendunud, et tulemus peab mahtuma ära
        g = c;
        g = d;

        float h = 123.23424F;
        double i = h;

        System.out.println(h); // Float mahub doublesse

        double j = 55.11111111111111;
        float k = (float) j;

        System.out.println(k); // Double aga floati ei mahu, st see kaotab täpsuses. Antud juhul toimub ümardamine

        double l = 12E50; // 12 korda 10 astmel 50 (12 * 10^50)
        float m = (float)l;
        System.out.println(m); // Tulemus: infinity ehk lõpmatus

        int n = 3; // Siin ei hakka Java "karjuma", sest täpsuses ju ei kaotata: iga naturaalarv on täisarv (mitte vastupidi)
        double o = n;

        double p = 2.99;
        short q = (short) p; // nüüd hakkab Java "karjuma", sest täpsuses läheb kaduma! Tuleb castida

        System.out.println(q); // Vastuseks on 2, sest kõik pärast koma kaob ära

        // String r = "3"; // Nii ei saa! Java annab kohe teada, et Stringi ei saa sellisel moel castida integeriks!
        // int s = (int)r;

        // int t = 3 // Sama lugu- ka vastupidi ei saa castida. Ainult numbrilisi tüüpe saab omavahel castida!
        // String u = (String)t;

        int r = 2;
        int s = 9;
        // int / int = int
        // int / double = double
        // double / int = double
        // double + int = double
        // double / float = double

        System.out.println(r /(double)s);

        System.out.println(r /(float)s); // teisendab samuti komakohtadeks, kuid täpsus on väiksem

        float t = 12.55555F;
        double u = 23.555555;
        System.out.println(t / u);
    }
}
