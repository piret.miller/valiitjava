package it.vali;

public class Main {

    public static void main(String[] args) {
        // Deklareerime/defineerime tekstitüüpi (String) muutuja (variable),
        // mille nimeks paneme name ja väärtuseks Piret
        String name = "Piret";
        String lastName = "Miller";

        System.out.println("Hello, " + name + " " + lastName);
    }
}
