﻿-- See on lihtne hello world teksti päring, mis tagastab ühe rea ja ühe veeru
-- (veerul puudub pealkiri). Selles veerus ja reas saab olema tekst Hello World
SELECT 'Hello World';
 -- Aga saab ka nii:
/*SELECT 3;
*/

-- Täisarvu saab küsida ilma '' märkideta
SELECT 3;

-- ei tööta PostgreSQL'is. Näiteks MSSql'is töötab
SELECT 'raud' + 'tee'; 

-- Srandardmeetod CONCAT töötab kõigis erinevates SQL serverites 
SELECT CONCAT('all', 'maa', 'raud', 'tee', 'jaam', '.', 2, 0, 0, 4);

-- Kui tahan erinevaid veerge, siis panen väärtustele koma vahele
SELECT 'Peeter', 'Paat', 23, 75.45, 'Blond'

-- As märksõnaga saab anda antud veerule nime.
-- saab ka ilma AS-ta, kuid ilma selleta on kergem vigu teha

SELECT 
	'Peeter' AS Eesnimi, 
	'Paat' AS Perenimi,
	23 AS vanus,
	75.45 AS kaal,
	'Blond' AS juuksevärv,
	
-- Tagastab praeguse kellaaja ja kuupäeva mingis vaikimisi formaadis
SELECT NOW();

-- Kui tahan konkreetset osa sellest, näiteks aastat või kuud, siis:
SELECT date_part ('year', NOW());

-- Kuupäeva formaatimine Eesti kuupäeva formaati. Võib panna vahele punkte, komasid jne
SELECT to_char(NOW(), 'HH24:mi:ss DD.MM.YYYY');

-- Interval laseb lisada või eemaldada mingit ajaühikut, annab perioodi
SELECT NOW() + interval '1 day ago';
-- või
SELECT NOW() - interval '1 day'; -- mõlemad võtavad ühe päeva maha
SELECT NOW() + interval '2 centuries, 3 years, 2 months, 1 weeks, 3 days, 4 seconds';

-- Kui tahan mingit kuupäeva osa ise etteantud kuupäevast
SELECT date_part ('month', DATE '2019-01-01');

-- Kui tahan kellaajast saada näiteks minuteid
SELECT date_part ('minutes', TIME '10:10');

-- Tabeli loomine
CREATE TABLE student (
	id serial PRIMARY KEY, -- MySQL-is bigint(20) unsigned NOT NULL AUTO_INCREMENT. Unsigned tähedab, et negatiivseid numbreid ei saa siia panna
	-- serial tähendab, et tüübiks on int, mis hakkab ühe võrra suunenema
	-- PRIMARY KEY (primaarvõti) tähendab, et see on unikaalne väli tabelis
	first_name varchar(64) NOT NULL, -- Ei tohi tühi olla
	last_name varchar(64) NOT NULL,
	height int NULL, -- tohib tühi olla
	weight numeric(5,2) NULL,
	birthday date NULL
);

-- Tabelist kõikide ridade kõikide veergude küsimine
-- * siin tähendab, et anna kõik veerud
SELECT * FROM student;


-- Nii saab sünniajast vanuse kätte:
SELECT 
	first_name AS eesnimi, 
	last_name AS perekonnanimi,
	date_part ('year', NOW()) - date_part('year', birthday) AS vanus
FROM 
	student;

-- Saame liita kaks veergu kokku	
SELECT CONCAT(first_name, ' ', last_name) AS täisnimi FROM student;

-- Kui tahan filtreerida ja otsida mingi tingimuse järgi:
SELECT 
	*
FROM 
	student
WHERE
	height = 180;
	
-- Kui tahame otsida mitme tingimusega korraga:
-- AND:
SELECT
	*
FROM
	student
WHERE
	first_name = 'Peeter' 
	AND last_name = 'Tamm'
	
-- OR:
SELECT
	*
FROM
	student
WHERE
	first_name = 'Peeter' 
	OR middle_name = 'Malle'
	
-- Anna Peeter või Mari:
SELECT
	*
FROM
	student
WHERE
	first_name = 'Peeter' 
	AND last_name = 'Tamm'
	OR first_name = 'Mari'

-- Küsi tabelist eesnime ja perekonnanime järgi 
-- mingi Peeter Tamm ja mingi Mari Maasikas (antud juhul pole sulgusid tegelikult vaja, sest AND arvutatakse alati enne OR-i)

SELECT
	*
FROM
	student
WHERE
	(first_name = 'Peeter' AND last_name = 'Tamm')
	OR 
	(first_name = 'Mari' AND last_name = 'Maasikas');
	
-- Anna mulle õpilased, kelle pikkus jääb 170-180 vahele
SELECT
	*
FROM
	student
WHERE
	height >= 170 OR height <= 180
	
-- Anna mulle õpilased, kelle pikkus on suurem kui 170 või väiksem kui 150:
SELECT
	*
FROM
	student
WHERE
	height >= 170 OR height <= 150
	
-- Anna mulle õpilaste eesnimed ja pikkused, kelle sünnipäev on jaanuaris
EXTRACT(MONTH FROM birthday) = 01; -- nii töötab ka


SELECT
	height, first_name
FROM
	student
WHERE
	date_part ('month', birthday) = 1;

-- Anna mulle õpilased, kelle middle_name on null (määramata)
SELECT
	*
FROM
	student
WHERE
	middle_name IS NULL
	
	
-- Anna mulle õpilased, kelle middle_name on null (määramata) ja kaal on null
SELECT
	*
FROM
	student
WHERE
	middle_name IS NULL AND weight IS NULL
	
	
-- Anna mulle õpilased, kelle middle_name on null (määramata) ja kaal on määratud
SELECT
	*
FROM
	student
WHERE
	middle_name IS NULL AND weight IS NOT NULL
	
	
-- Anna mulle õpilased, kelle pikkus ei ole 180 cm
SELECT 
	*
FROM 
	student
WHERE
	height != 180
	
	
-- Aga saab ka nii võrdust välistada:
SELECT 
	*
FROM 
	student
WHERE
	height <> 180
	
-- Või nii:
SELECT 
	*
FROM 
	student
WHERE
	NOT (height = 180);
	
	
-- Anna mulle õpilased, kelle pikkus on 169, 149 või 171
SELECT 
	*
FROM 
	student
WHERE
	--height = 169 OR height = 149 OR height = 171
	height IN (169, 149, 171)
	

-- Anna mulle õpilased, kelle eesnimi on Peeter, Mari või Kalle

SELECT 
	*
FROM 
	student
WHERE
	--first_name = 'Peeter' OR first_name = 'Mari' OR first_name = 'Kalle'
	 first_name IN ('Peeter', 'Mari', 'Kalle')
	 

-- Anna mulle õpilased, kelle eesnimi ei ole Peeter, Mari või Kalle
SELECT 
	*
FROM 
	student
WHERE
	 NOT (first_name IN ('Peeter', 'Mari', 'Kalle'))
	 

-- Anna mulle õpilased, kelle sünnikuupäev on kuu esimene, neljas või seitsmes päev

SELECT
	*
FROM
	student
WHERE
	date_part('day', birthday) IN (1, 4, 7)
	

-- Anna mulle õpilased, kelle sünnikuupäev EI OLE kuu esimene, neljas või seitsmes päev
SELECT
	*
FROM
	student
WHERE
	date_part('day', birthday) NOT IN (1, 4, 7)
	
	
-- Kõik WHERE võrdlused jätavad välja NULL väärtusega read
SELECT
	*
FROM
	student
WHERE
	height > 0 OR height <= 0 -- Kõikidest võrdlemistest jäävad nullid välja
	

-- Null-väärtused saab kätte nii: (aga võrdluses null ei osale)
SELECT
*
FROM
student
WHERE
height > 0 OR height <= 0 OR height IS NULL


-- Anna mulle õpilased pikkuse järjekorras, lühemast pikemaks
SELECT
	*
FROM
	student
ORDER BY
	height
	
	
-- Kui pikkused on võrdsed, järjesta kaalu järgi
SELECT
	*
FROM
	student
ORDER BY
	height, weight
	
	
-- Eesnime ja perekonnanime järgi
SELECT
	*
FROM
	student
ORDER BY
	first_name, last_name


-- Kui tahan tagurpidises järjekorras, siis lisandub sõna DESC (Descending)
-- On olemas ka ASC (Ascending), mis on vaikeväärtus
SELECT
	*
FROM
	student
ORDER BY
	first_name DESC, last_name DESC
	
-- Kui tahan, et hoopis kaalu järgi kahanevalt:
SELECT
	*
FROM
	student
ORDER BY
	first_name, weight DESC, last_name
	
	
-- ID järgi sorteerimine 
-- (esialgu võtab aluseks eesnime, kui aga eesnimesid on mitu samasugust, 
-- siis järjestab ID alusel)
SELECT
	*
FROM
	student
ORDER BY
	first_name, id DESC, last_name
	
	
-- Anna mulle vanuse järjekorras vanemast nooremaks õpilaste pikkused,
-- mis jäävad 160 ja 170 vahele
SELECT
	*
FROM
	student
WHERE
	height >= 160 OR height <= 170
ORDER BY
	birthday
	
-- Algustähe järgi otsimine 
-- (aga ka '%p%', mis tähendab, et p on sees (kas alguses või lõpus) 
-- või '%p', mis tähendab, et p peab olema viimane täht)
SELECT
	*
FROM
	student
WHERE
	first_name LIKE '%P'
	
	
-- Kui tahan otsida sõna seest mingit sõnaühendit
SELECT
	*
FROM
	student
WHERE
	first_name LIKE 'Ka%' -- eesnimi algab 'Ka'
	OR first_name LIKE '%ee%' -- eesnimi sisaldab 'ee'
	OR first_name LIKE '%ri' -- eesnimi lõppeb 'ri' 


-- Tabelisse uute kirjete lisamine
INSERT INTO student 
	(first_name, last_name, height, weight, birthday, middle_name)
VALUES
	('Allan', 'Peterson', 175, 87.55, '1974-02-11', 'Kristjan'),
	('Laura', 'Laul', 160, 53.55, '1988-08-22', NULL),
	('Kert', 'Keerutaja', 189, 123.45, '1990-01-15', NULL)
	

-- Osa väljade sisestamine
INSERT INTO student 
	(first_name, last_name)
VALUES
	('Tuule', 'Lipp')
	

-- Tabelis kirje muutmine: UPDATE lausega peab olema ettevaatlik!
-- Alati peab kasutama WHERE lause lõpus. UPDATE ja WHERE alati koos!!!
-- NIIMOODI TEHA EI TOHI!:
UPDATE
	student
SET
	height = 172 -- muudab KÕIGIL isikutel pikkuse ära
	

-- Seega tuleb kasutada WHERE:
UPDATE
	student
SET
	height = 172
WHERE 
	id = 5
	
	
-- Või ka nii:
UPDATE
	student
SET
	height = 173
WHERE 
	first_name = 'Kalle' AND last_name = 'Kalamees'
	
	
-- Muuda kõigi õpilaste pikkus ühe võrra suuremaks
UPDATE
	student
SET
	height = height +1
	
-- Mitme välja muutmine id järgi:
UPDATE
	student
SET
	height = 190,
	weight = 100,
	middle_name = 'Joanna',
	birthday = '1990-10-10'
WHERE 
	id = 14
	

-- Suurenda hiljem kui 1999 õpilastel sünnipäev ühe päeva võrra
UPDATE
	student
SET
	birthday = birthday + interval '1 days'
WHERE
	date_part('year', birthday) > 1990
	
	
-- Kustutamisega olla ETTEVAATLIK!
-- Alati kasuta WHERE'i!
DELETE FROM
	student
WHERE
	id = 14
	
	
-- CRUD operations
-- Kui sa saad andmeid lisada, muuta ja kustutada, siis on see CRUD
-- Create (Insert), Read (Select), Update, Delete


-- Loo uus tabel loan, millel on väljad: 
-- amount (reaalarv, numeric(11,2)), start_date, due_date, student_id, 

CREATE TABLE loan (
	id serial PRIMARY KEY,
	-- serial tähendab, et tüübiks on int, mis hakkab ühe võrra suunenema
	-- PRIMARY KEY (primaarvõti) tähendab, et see on unikaalne väli tabelis
	student_id int NOT NULL,
	amount numeric(11,2) NOT NULL,
	start_date DATE NOT NULL, -- Ei tohi tühi olla
	due_date DATE NOT NULL
);


-- Lisa neljale õpilasele laenud. Kahele neist lisa veel üks laen
INSERT INTO loan
	(student_id, amount, start_date, due_date)
VALUES
	(3, 400, '2019-05-14', '2022-05-13'),
	(4, 500, '2019-05-14', '2021-05-13'),
	(5, 600, '2019-05-14', '2023-05-13'),
	(3, 900, '2018-01-01', '2018-12-31'),
	(4, 500, '2018-10-10', '2019-12-31'),
	(6, 700, NOW(), '2019-12-31'),
	
	
-- Anna mulle kõik õpilased koos oma laenudega
SELECT 
	*
FROM 
	student
JOIN 
	loan
	ON student.id = loan.student_id
	
	
-- Annab vaid studenti tabeli veerud
SELECT 
	student.*
FROM 
	student
JOIN 
	loan
	ON student.id = loan.student_id
	

-- Kui aga huvitab vaid õpilase nimi ja laenusumma:
SELECT 
	student.first_name, -- Töötab hetkle ka ilma student.-ta. Kuid ainult siis saab kasutada, kui first_name ei ole mitte üheski teises tabelis olemas
	student.last_name,
	loan.amount
FROM 
	student
JOIN 
	loan
	ON student.id = loan.student_id
	
	
-- Anna mulle kõik õpilased koos oma laenudega,
-- aga ainult sellised laenud, mis on suuremad kui 500.
-- Järjesta laenu koguse järgi suuremast väiksemaks
-- INNER JOIN on selline tabelite liitmine, kus liidetakse
-- ainult need read, kus on võrdsed student.id = loan.student_id
-- ehk need read, kus tabelite vahel on seos.
-- Ülejäänud read ignoreeritakse

SELECT 
	student.first_name,
	student.last_name,
	loan.amount
FROM 
	student
INNER JOIN -- INNER JOIN on vaikimisi JOIN. INNER sõna võib ära jätta (nagu ASC-gi) 
	loan
	ON student.id = loan.student_id
WHERE -- WHERE on alati ENNE kui ORDER BY. Üldisemast detailsema poole
	loan.amount > 500 -- Kindluse mõttes loan. juurde!
ORDER BY
	loan.amount DESC
	
	
-- Või kui soovime ka nime järgi järjestada:
-- ORDER BY
	--student.last_name, loan.amount DESC
	

-- Loo uus tabel loan_type, milles väljad name ja description

CREATE TABLE loan_type (
	id serial PRIMARY KEY,
	name varchar(30) NOT NULL,
	description varchar(500) NULL
);


-- Uue välja lisamine olemsaolevale tabelile
ALTER TABLE loan
ADD COLUMN loan_type_id int

-- Tabeli kustutamine
DROP TABLE student;


-- Lisame laenutüübid:
INSERT INTO loan_type
	(name, description)
VALUES
	('Õppelaen', 'See on suhteliselt hea laen'),
	('SMS-laen', 'See on väga halb laen'),
	('Väikelaen', 'See on väga kõrge intressimääraga laen'),
	('Kodulaen', 'See on suhteliselt madala intressimääraga laen');
	

-- Kolme tabeli INNER JOIN

-- INNER JOINI puhul joinimiste järjekord ei ole oluline


-- LEFT JOIN puhul võetakse JOINi esimesest (vasakust) tabelist kõik read
-- ning teises tabelis (paremas) näidatakse puuduatel kohtadel NULL
SELECT 
	s.first_name, s.last_name, l.amount
FROM
	student AS s
LEFT JOIN
	loan AS l
	ON s.id = l.student_id
	

-- See on sama...:
SELECT 
	s.first_name, s.last_name, l.amount
FROM
	student AS s
RIGHT JOIN
	loan AS l
	ON s.id = l.student_id



-- ... mis see:
SELECT 
	s.first_name, s.last_name, l.amount
FROM
	loan AS l
LEFT JOIN
	student AS s
	ON s.id = l.student_id
	
	
	
-- LEFT JOIN puhul on järjekord väga oluline
SELECT 
	s.first_name, s.last_name, l.amount, lt.name
FROM
	student AS s
LEFT JOIN
	loan AS l
	ON s.id = l.student_id
LEFT JOIN
	loan_type AS lt
	ON lt.id = l.loan_type_id
	
	
-- Saab teha kõiki kombinatsioone kahe tabeli vahel
SELECT 
	s.first_name, st.first_name
FROM
	student AS s
CROSS JOIN
	student AS st
WHERE
	s.first_name != st.first_name
	
	
-- FULL OUTER JOIN on sama, mis LEFT JOIN + RIGHT JOIN
SELECT 
	s.first_name, s.last_name, l.amount
FROM
	student AS s
FULL OUTER JOIN
	loan AS l
	ON s.id = l.student_id
	
	
-- Anna mulle kõikide kasutajate perekonnanimed, kes võtsid SMS-laenu ja 
-- kelle laenu summa on üle 100 euro. Tulemused järjesta laenu võtja vanuse järgi
-- väiksemast suuremaks 

SELECT 
	s.last_name
FROM
	student AS s
JOIN
	loan AS l
	ON s.id = l.student_id
JOIN
	loan_type AS lt
	ON lt.id = l.loan_type_id
WHERE
	lt.name = 'SMS-laen'
	AND
	l.amount > 100
ORDER BY
	s.birthday 
	
	
-- Aggregate functions (nt MIN, MAX, SUM, AVG, COUNT)
-- Agregaatfukntsiooni selectis välja kutsudes kaob võimalus samas select lauses
-- küsida mingit muud välja tabelist, sest agregaatfunktsiooni tulemus on alati vaid üks number
-- ja seda ei saa kuidagi näidata koos väljadega, mida võib olla mitu rida. 
-- Nt kui küsin average, siis ma ei saa eristada averagesid nt eesnimede või perenimede järgi

-- Keskmise leidmine. Jäetakse välja read, kus height on NULL
SELECT 
	AVG(height)
FROM
	student
	
	
-- Kui palju on üks õpilane keskmiselt laenu võtnud
-- Arvestatakse ka neid õpilasi, kes ei ole laenu võtnud (amount on NULL). 
-- Enne arvutamist lihtsalt teisendattakse NULL 0-ks
SELECT 
	--AVG(loan.amount)
	
	AVG(COALESCE(loan.amount, 0)) -- kaks parameetrit- üks on see, mis võib null olla, teine on see, mida tahame, et 0-ks muutuks
FROM
	student
LEFT JOIN
	loan
	ON student.id = loan.student_id
	
	
-- MAX ja MIN leidmine
SELECT 
	
	AVG(COALESCE(loan.amount, 0)), -- kaks parameetrit- üks on see, mis võib null olla, teine on see, mida tahame, et 0-ks muutuks
	MAX(loan.amount)
FROM
	student
LEFT JOIN
	loan
	ON student.id = loan.student_id


-- Saab ilusama sõnastuse ise juurde lisada:
SELECT 
	--AVG(loan.amount)
	
	ROUND(AVG(COALESCE(loan.amount, 0)),0) AS "Keskmine laenusumma",-- kaks parameetrit- üks on see, mis võib null olla, teine on see, mida tahame, et 0-ks muutuks
	MIN(loan.amount) AS "Maksimaalne laenusumma",
	MAX(loan.amount) AS "Minimaalne laenusumma"
FROM
	student
LEFT JOIN
	loan
	ON student.id = loan.student_id
	

-- Või nt selline näide:

SELECT 
	
	ROUND(AVG(COALESCE(loan.amount, 0)),0) AS "Keskmine laenusumma",-- kaks parameetrit- üks on see, mis võib null olla, teine on see, mida tahame, et 0-ks muutuks
	MIN(loan.amount) AS "Maksimaalne laenusumma",
	MAX(loan.amount) AS "Minimaalne laenusumma",
	COUNT(*) AS "Kõikide ridade arv",
	COUNT(loan.amount) AS "Laenude arv", -- jäetakse välja read, kus loan.mount on NULL
	COUNT(student.height) AS "Mitmel õpilasel on pikkus"
FROM
	student
LEFT JOIN
	loan
	ON student.id = loan.student_id
	
	

-- Kasutades GROUP BY, jäävad SELECT päringu jaoks alles vaid need väljad, mis on 
-- GROUP BY-s ära toodud (s.first_name, s.last_name)
-- Teisi välju saab ainult kasutada agregaatfunktsioonide sees
-- Kui muidu võtaks summat või keskmist, võtan üle kõikide ridade. 
-- Kuid on võimalik ka nö vahesummat saada, mis on nt selle konkreetse inimese summad
-- või selle ja selle aasta summad. 
-- Nö vahetehted, vahesummad. Siin saamegi kasutada GROUP BY.
SELECT
	s.first_name, 
	s.last_name, 
	SUM(l.amount),
	AVG(l.amount),
	MIN(l.amount),
	MAX(l.amount)
FROM
	student AS s
LEFT JOIN
	loan AS l
	ON s.id = l.student_id
GROUP BY
	s.first_name, s.last_name
	
	
-- Anna mulle laenude summad laenu tüüpide järgi (Hea näide GROUP BY kasutamisest!)
SELECT 
	lt.name, SUM(l.amount) -- Sõltub, mis järjekorras veerud ilmuvad
FROM
	loan AS l
JOIN
	loan_type AS lt
	ON lt.id = l.loan_type_id
GROUP BY
	lt.name	
	

-- Tekita mingile õpilasele 2 sama tüüpi laenu
-- Anna mulle laenude summad grupeerituna õpilase ja laenu tüübi kaupa
-- Mari õppelaen 2400 (1200+1200)
-- Mari väikelaen 2000
-- Jüri õppelaen 2000
SELECT 
	s.first_name, s.last_name, lt.name, SUM(l.amount), COUNT(l.amount)  -- Sõltub, mis järjekorras veerud ilmuvad 
FROM
	student AS s
JOIN
	loan AS l
	ON s.id = l.student_id
JOIN
	loan_type AS lt
	ON lt.id = l.loan_type_id
GROUP BY
	s.first_name, s.last_name, lt.name
	
	
-- Kui tahan ka neid sünniaastaid, kes ei võtnud laenu
SELECT 
	date_part('year', s.birthday), SUM(l.amount)  -- Sõltub, mis järjekorras veerud ilmuvad 
FROM
	student AS s
LEFT JOIN
	loan AS l
	ON s.id = l.student_id
GROUP BY
	date_part('year', s.birthday)
	
	
-- Kui tahan ka neid sünniaastaid, kes ei võtnud laenu
-- Anna mulle laenude, mis ületavad 1000 eurot, summad sünniaastate järgi
SELECT 
	date_part('year', s.birthday), SUM(l.amount)  -- Sõltub, mis järjekorras veerud ilmuvad 
FROM
	student AS s
LEFT JOIN
	loan AS l
	ON s.id = l.student_id
GROUP BY
	date_part('year', s.birthday)
-- HAVING on nagu WHERE, aga pärast GROUP BY kasutamist.
-- Filtreerimisel saad kasutada ainult neid välju, mis on GROUP BY-s
-- ja agregaatfunktsioone
HAVING -- Kui tahan filtreerimist teha pärast GROUP BY'd
	date_part('year', s.birthday) IS NOT NULL
	AND SUM(l.amount) > 1000
	
-- Arvesta ainult neid, kes on kaks korda laenu võtnud



-- Kui tahan ka neid sünniaastaid, kes ei võtnud laenu
-- Anna mulle laenude, mis ületavad 300 eurot, summad sünniaastate järgi
SELECT 
	s.first_name, s.last_name, date_part('year', s.birthday), SUM(l.amount)  -- Sõltub, mis järjekorras veerud ilmuvad 
FROM
	student AS s
LEFT JOIN
	loan AS l
	ON s.id = l.student_id
GROUP BY
	date_part('year', s.birthday), s.first_name, s.last_name
-- HAVING on nagu WHERE, aga pärast GROUP BY kasutamist.
-- Filtreerimisel saad kasutada ainult neid välju, mis on GROUP BY-s
-- ja agregaatfunktsioone
HAVING -- Kui tahan filtreerimist teha pärast GROUP BY'd
	date_part('year', s.birthday) IS NOT NULL
	--AND SUM(l.amount) > 300 -- anna ainult need read, kus laenusumma on suurem kui 300
	--AND COUNT(l.amount) = 2 -- anna ainult need read, kus laene kokku oli 2
ORDER BY
	date_part('year', s.birthday)
	
	

-- Anna mulle laenude, mis ületavad 300 eurot, summad laenu algusaasta järgi
SELECT 
	s.first_name, s.last_name, date_part('year', l.start_date), SUM(l.amount)  -- Sõltub, mis järjekorras veerud ilmuvad 
FROM
	student AS s
LEFT JOIN
	loan AS l
	ON s.id = l.student_id
GROUP BY
	date_part('year', l.start_date), s.first_name, s.last_name
-- HAVING on nagu WHERE, aga pärast GROUP BY kasutamist.
-- Filtreerimisel saad kasutada ainult neid välju, mis on GROUP BY-s
-- ja agregaatfunktsioone
HAVING -- Kui tahan filtreerimist teha pärast GROUP BY'd
	date_part('year', l.start_date) IS NOT NULL
	--AND SUM(l.amount) > 300 -- anna ainult need read, kus laenusumma on suurem kui 300
	--AND COUNT(l.amount) = 2 -- anna ainult need read, kus laene kokku oli 2
ORDER BY
	date_part('year', l.start_date)
	
	
	
-- Anna mulle, mitu laenu mingist tüübist on võetud j amis on nende summa
-- SMS-laen 2 tk, summa 1000
-- Kodulaen 1 tk, summa 800 jne
SELECT 
	lt.name, COUNT(l.amount), SUM(l.amount)
FROM
	loan_type AS lt -- oleks LEFT JOINi puhul vasakul pool. By default on INNER JOIN, FROM-i poolt hakatakse vaatama
LEFT JOIN -- Kui oleks LEFT JOIN, siis toob sisse ka need laenud, mida võetud veel ei ole (sum NULL)
	loan AS l
	ON l.loan_type_id = lt.id
GROUP BY
	lt.name
	
	
-- Mis aastal sündinud võtsid suurima summa laene?
SELECT
	date_part('year', s.birthday), SUM(l.amount)
FROM
	student AS s
JOIN
	loan AS l
	ON l.student_id = s.id
GROUP BY
	date_part('year', s.birthday)
ORDER BY
	SUM(l.amount) DESC
LIMIT 1 -- ütleb, et anna ainult esimene rida 



-- Anna mulle õpilaste eesnime esitähe esnemise statistika
-- ehk mitme õpilase eesnimi algab mingi tähega
-- m 3
-- a 2
-- l 4

-- Mis sõnast, mitmendast tähest ja mitu tähte. Algab 1-st. Nt 'kala'
SELECT 
	SUBSTRING(s.first_name, 1, 1), COUNT(SUBSTRING(s.first_name, 1, 1)) -- Töötab ka nii: COUNT(first_name)-- COUNT(SUBSTRING(s.first_name, 1, 1))
FROM
	student AS s
GROUP BY
	SUBSTRING(s.first_name, 1, 1)
ORDER BY
	SUBSTRING(s.first_name, 1, 1)
	
	
-- Kui tahame väljastada "Hello, PostgreSQL 233"
SELECT FORMAT('Hello, %s %s','PostgreSQL', 233)


-- Otsime tühiku asukohta:
SELECT POSITION (' ' in 'Piret Miller')

-- Otsi esimene sõna:
SELECT SUBSTRING ('Piret Miller', 1, POSITION(' ' in 'Piret Miller'))

-- Otsi teine sõna:
SELECT SUBSTRING ('Piret Miller', POSITION(' ' in 'Piret Miller') + 1, 10000) -- Pikkuseks on suvaline pikk number, aga EI PEA panema seda viimast parameetrit!


-- Subquery or Inner query or Nested query- üks päring on teise sees
-- Anna mulle õpilased, kelle pikkus vastab keskmisele õpilaste pikkusele
SELECT 
	first_name, last_name
FROM
	student
WHERE
	height = (SELECT ROUND(AVG(height))FROM student) --või height, 0 või , 2 (kaks komakohta)



-- Subquery or Inner query or Nested query- üks päring on teise sees
-- Anna mulle õpilased, kelle pikkus vastab keskmisele õpilaste pikkusele
-- Sealt edasi anna mulle õpilased, kelle eesnimi on keskmise pikkusega õpilaste keskmine nimi
SELECT 
	first_name, last_name
FROM
	student
WHERE
	first_name IN
(SELECT 
	middle_name
FROM
	student
WHERE
	height = (SELECT ROUND(AVG(height))FROM student))  --või height, 0 või , 2 (kaks komakohta)
	
	
-- Lisa kaks vanimat õpilast töötajate tabelisse
INSERT INTO employee (first_name, last_name, birthday, middle_name)

SELECT first_name, last_name, birthday, middle_name FROM student ORDER BY birthday LIMIT 2 -- See ongi VALUE asemel
	

-- Lisa kaks noorimat õpilast töötajate tabelisse
INSERT INTO employee (first_name, last_name, birthday, middle_name)

SELECT first_name, last_name, birthday, middle_name FROM student ORDER BY birthday DESC LIMIT 2


-- Kuidas Eclipses siduda projekti pom.xml file-ga ehk gonfifailiga:
-- Projekti nimel parem klõps- Configure- Convert to Maven Projekt

