package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = 0; // proovime arvu jagada 0-ga
        try {
            int b = 4 / a; // Viskab exceptioni ArithmeticException. Seega, Surroundime selle Try-Catchiga (teeme aktiivseks --> Code--> Surround With --> Try-Catch

            String word =null; // Proovime leida sõna pikkust, mis võrdub 0-ga
            // (st pikkust leida on tegelikult võimatu).
            word.length(); // Püütakse kinni Runtime Exceptionis,
            // sest kuulub sinna hulka (vaata netist!!!)
            // Sealjuures, kui siin viga tekib, siis edasi järgmise tehteni programm ei jõuagi.
            // Kui tahame, et kood aga edasi jookseks ja järgmise tehte ka teeks,
            // siis tuleb järgmised tehted tõsta välja, allapoole ja teha neile uued catchid
        }
        // Kui on mitu catch plokki, siis otsib ta esimese catch ploki,
        // mis oskab antud Exceptioni kinni püüda. Edasi järgmise catchini enam ei lähe
        catch (ArithmeticException e) {
            if(e.getMessage().equals("/ by zero")) {
                System.out.println("Nulliga ei saa jagada");
            }
            else {
                System.out.println("Esines aritmeetiline viga");
                System.out.println(e.getMessage());
            }
        }
        catch (RuntimeException e) {
            System.out.println("Esines reaalajas esinev viga");
        }
        // Exception on klass, millest kõik erinevad Exceptioni tüübid pärinevad.
        // Mis omakorda tähendab, et püüdes kinni selle üldise Exceptioni,
        // püüame me kinni kõik Exceptionid
        catch (Exception e) { // ehk et liigume detailsemast üldisema poole.
            // Kirjutame kõik Exceptionid välja ja lõppu paneme juurde üldise Exceptioni.
//            e.printStackTrace();
            System.out.println("Esines mingi viga"); // ... ehk et kasutajani ei jõua ükski päris viga. Kõige üldisemasse Exceptionisse kirjutame lihtsalt, et esines mingi viga (sest me täpsemalt ei pruugi teada, mis viga)
        }

        // Küsime kasutajalt numbri ja kui number ei ole õiges formaadis
        // (ei ole tegelikult number), siis ütleme veateate

        // P.S.- lisasin Scanneri küll üles, Main meetodi alla, kuid selle võib ka siia lisada.

        boolean correctNumber = false; // Deklareerime kõigepealt Booleani (oli õige formaat)
        // ning väärtustame selle kõigepealt valeks

        // Do-while sobib siia seetõttu, et tahame kõigepealt teha
        // kindlasti ära vähemalt ühe kontrolli.
        // Alles seejärel peaks selguma, kas on vaja tegevust korrata
        do {
            System.out.println("Ütle üks number");
            try {
                int answer = Integer.parseInt(scanner.nextLine()); // Teisendame vastuse kohe integeriks.
               // break; // Breakini jõuab alles siis, kui õnnestus teisendamine, st vastus oli tõesti numbriformaadis.
                // Võib breaki kasutada, aga pigem vältida, kui saab.
                // Kui Breakini jõuame, siis while-tsüklisse enam ei lähe, sest Boolean osutus kohe tõeseks
                correctNumber = true; // Try vaatab, kas int answer oli tõesti intina sisestatud.
                // Kui oli, siis Boolean correctNumber muutub tõeseks ja catchi ega While-tsüklisse ei jõua.
            } catch (NumberFormatException e) { // IDE ise panigi kohe õige Exceptioni.
                // See on veelgi detailsem kui Arithmetic Exception, öeldes isegi ära selle, et numbri formaat on vale
                System.out.println("Number oli vigane. Sisesta reaalne number");
            }
        } while (!correctNumber); // peame mõtlema mingi tingimuse, mis oleks väär seni,
        // kuni juhtub Exception (st kui enam Exceptionit ei ole, siis muutub lõpuks trueks
        // ja ei korda enam tsüklit).
        // Kuni on väär, tsükkel kordab ennast lõputult või seni,
        // kuni lõpuks sisestatakse numbriformaadis vastus.

        // Loo täisarvude massiiv 5 täisarvuga ning ürita sinna lisada kuues täisarv
        // Näita veateadet

        int [] numbers = new int[5];  // Loon täisarvu massiivi 5 elemendiga.
        // Võib ka int[] numbers = new int []{ 2, 7, -2, 11, 1 };
        // Sellisel juhul proovime samuti Try plokis lihtsalt ühte elementi juurde panna, nt: numbers [5] = 2.
        numbers [0] = 2;
        numbers [1] = 7;
        numbers [2] = -2;
        numbers [3] = 11;
        numbers [4] = 1;

        try {
            numbers [5] = 2; // peame kinni püüdma Exceptioni "ArrayIndexOutOfBoundsException"

        } catch (ArrayIndexOutOfBoundsException e) { // Esmalt tuli üldine Exception.
            // Kopime asemele detailse Exceptioni
            //e.printStackTrace(); // Selle võtame nüüd välja ja asendame oma enda koostatud lausega
            System.out.println("Indeksit, kuhu tahtsid väärtust määrata, ei eksisteeri, st on massiivi piiridest välas");
        }
        catch (Exception e) { // Lisame lõppu ka üldise Exceptioni ehk et liigume detailsemast üldisema poole.
            // Seega, kirjutame kõik Exceptionid välja ja lõppu paneme juurde üldise Exceptioni.
            // Nii on kindel, et kõik Exceptionid püütakse kinni!
//            e.printStackTrace(); // ... asendame taas omakoostatud lausega:
            System.out.println("Esines mingi viga"); // ... ehk et kasutajani ei jõua ükski päris viga
        }
    }
}
