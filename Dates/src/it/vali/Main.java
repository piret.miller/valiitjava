package it.vali;

import java.io.CharArrayReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Main {

    public static void main(String[] args) {

        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss EEEEE"); // Kõigepealt annan formaadi

        Calendar calendar = Calendar.getInstance(); // Küsin kalendri formaadi endale selle meetodiga

        calendar.add(Calendar.DATE, 3); // Kui panin -1, saan tänase. Ütlen, et seadista päev eilseks

        // Get

        Date date = calendar.getTime();

        System.out.println(dateFormat.format(date));

        //calendar.add(Calendar.YEAR, 1);
        date = calendar.getTime();
        System.out.println(dateFormat.format(date));


        // Prindi ekraanile selle aasta järele jäänud kuude esimese kuupäeva nädalapäevad


        System.out.println(calendar.get(Calendar.MONTH));

        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), 1);
//        date = calendar.getTime();
//        System.out.println(dateFormat.format(date));

        int monthsLeftThisYear = 11 - calendar.get(Calendar.MONTH);

        DateFormat weekdayFormat = new SimpleDateFormat("dd.MM.yyyy EEEEE");

//        for (int i = 0; i < monthsLeftThisYear ; i++) {
//            calendar.add(Calendar.MONTH, 1);
//            date = calendar.getTime();
//            System.out.println(weekdayFormat.format(date));
//        }

        int currentYear = calendar.get(Calendar.YEAR);
        calendar.add(Calendar.MONTH, 1); // alendrit nihutan kuu võrra ja tsüklis kontrollin, et kas pärast nihutamist on aasta ka suurenenud

        while (calendar.get(Calendar.YEAR) == currentYear) {
            date = calendar.getTime();
            System.out.println(weekdayFormat.format(date));
            calendar.add(Calendar.MONTH, 1);

        }
    }
}
