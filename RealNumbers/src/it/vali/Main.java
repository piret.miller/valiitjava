package it.vali;

public class Main {

    public static void main(String[] args) {
        float a = 123.34f;
        double b = 123.4343; // pane tähele, et floatile ja longile tuleb tähed järele kirjutada,
        // sest need ei ole vaikimisi andmetüübid Javas.
        // Double puhul aga nt ei ole vaja midagi juurde kirjutada

        System.out.println("Arvude " + a + " ja " + b + " summa on " + (a + b));
        System.out.println("Arvude " + a + " ja " + b + " jagatis on " + (a / b));
        System.out.printf("Arvude %e ja %e jagatis on %e%n", a, b, (a/b)); // annab kümnendastmel
        System.out.printf("Arvude %f ja %f jagatis on %f%n", a, b, (a/b)); // annab komakohtadeks 6 kohta
        System.out.printf("Arvude %g ja %g jagatis on %g%n", a, b, (a/b)); // ümardab vähemtäpsema järgi
        System.out.println();


        // näeme, et double annab rohkem komakohti
        float c = 130;
        float e = 9;

        double f = 130;
        double g = 9;

        System.out.println("Arvude " + c + " ja " + e + " jagatis on " + c / e);
        System.out.printf("Arvude %f ja %f jagatis on %f%n", c, e, c / e);

        System.out.println("Arvude " + f + " ja " + g + " jagatis on " + f / g);
        System.out.printf("Arvude %g ja %g jagatis on %g%n", f, g, f / g);

        // ja taas näeme, et double annab rohkem komakohti
        System.out.println(c / e);
        System.out.println(f / g);

        System.out.println(c * e);
        System.out.println(f * g);

        float h = 130000f;
        float i = 23000000f;

        double j = 130000;
        double k = 23000000;

        System.out.println(h / i);
        System.out.println(j / k);
    }
}
