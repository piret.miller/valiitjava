package it.vali;

import java.util.Locale;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // Küsi kasutajalt 2 numbrit
        // prindi välja nende summa

        Scanner scanner = new Scanner(System.in);

        System.out.println("Sisesta esimene number");

        int a = Integer.parseInt(scanner.nextLine()); // Algselt annab IDE teada, et inputist saadud Stringi ei saa kuvada intina.
        // Seetõttu pakub ise punase lambipirni alt lahendui.
        // Tuleb valida 3. variant "Wrap using integer parseInt"

        System.out.println("Sisesta teine number");

        int b = Integer.parseInt(scanner.nextLine()); // Tuleb valida 3. variant "Wrap using integer parseInt"

        System.out.println("Arvude " + a + " ja " + b + " summa on " + (a + b));
        System.out.printf("Arvude %d ja %d summa on %d%n", a, b, a + b);

        // Küsi nüüd 2 reaalarvu ja prindi nende jagatis

        System.out.println("Sisesta esimene reaalarv");

        double c = Double.parseDouble(scanner.nextLine());

        System.out.println("Sisesta teine reaalarv");

        double d = Double.parseDouble(scanner.nextLine());

        System.out.println("Arvude " + c + " ja " + d + " jagatis on " + (c / d));
        System.out.printf("Arvude %.2f ja %.2f jagatis on %.2f%n", c, d, c / d); // tekitatakse kaks komakohta ja ühtlasi paneb koma tähiseks koma, mitte punkti

        // Kui ma tahan näha, et 0,50 asemel oleks 0.50,
        // siis ütlen eraldi ette, et kasuta USA local-it

        System.out.println(String.format(Locale.US, "%.2f", c / d));

        // Kui aga nt püüda sisestada numbri asemel nt teksti, siis viskab ette Exception teate (eesti k erind).
        // Näitab ära Exceptioni tüübi ja et viga tuleb otsida sisestatud inputist.
        // Viimasel real näitab ära vea asukoha rea, st juhatab meid vigase reani (saab otse peale klikkida)
        // Hallid lingid Exceptionis avatuna näitavad Java documentationis olevat infot
    }
}

