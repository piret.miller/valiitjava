package it.vali;

// Import tähendab, et antud klassile Main lisatakse ligipääs Java Class Libraryle/paketile
 // java.util paiknevale klassile Scanner. Java.util on pakett, mis koosneb paljudest klassidest, üheks sealseks klassiks on Scanner.
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	    // loome Scanner tüübist/klassist objekti nimega scanner, mille kaudu saab kasutaja sisendit lugeda.
        // Objekt tahab saada parameetrit, selleks on System.in
        Scanner scanner = new Scanner(System.in); // uus objekt

        System.out.println("Tere, mis on sinu nimi?");
        String name = scanner.nextLine();

        System.out.println("Tere, " + name );

        System.out.println("Mis on su perekonnanimi?");
        String lastName = scanner.nextLine();

        System.out.println("Tere, " + name + " " + lastName);

        System.out.println("Mis on su lemmik värv?");
        String color = scanner.nextLine();

        System.out.println("Tere, " + name + " " + lastName + ", " + color + " on väga ilus värv!");

        System.out.printf("Tere, %s %s, %s on väga ilus värv!\n", // \n tekitab reavahetuse. printf seda ise automaatselt ei tee!
                name, lastName, color);

        // Sõnade liitmiseks ja printimiseks saab kasutada ka StringBuilder objekti:

        StringBuilder builder = new StringBuilder();
        builder.append("Tere, ");
        builder.append(name);
        builder.append(" ");
        builder.append(lastName);
        builder.append(", ");
        builder.append(color);
        builder.append(" on väga ilus värv!");

        String fullText = builder.toString(); // teeme kõigepealt eraldi muutuja...
        System.out.println(fullText); // ... ja siis prindime selle loodud muutuja välja

        // ... või paneme objekti kohe printima

        System.out.println(builder.toString());

        // Kolmas variant on kasutada String.formatit:

        String text = String.format("Tere, %s %s, %s on väga ilus värv!", // kui me ei taha välja printida teksti, vaid tahame, et oleks lihtsalt String tekst, siis saab teha lihtsalt stringi niimoodi jupi kaupa
                name, lastName, color);

        System.out.println(text); // eelmist rida saab hiljem nt niimoodi välja printida

        // Nii System.out.printf kui ka String.format kasutavad enda siseselt StringBuilderit. Pluss-märgiga liitmine on lihtalt mälu halvasti kasutamine!


        // Lühidalt- System.out.printf kutsub välja String.formati plus System.out.println(), String format aga omakorda kutsub välja StringBuilderi.
        // Lõpptulemuseks peaksime saama viis täpselt ühesugust väljaprinditud rida, kuid viimased kolm võtavad vähem mälumahtu kui esimene, lihtsalt + märkidega liidetud print

// kui tahame nt kõiki \n korraga ära kustutada, siis Edit --> Find --> Replace --> \n (ülemisele väljale) --> (alumise välja jätame tühjaks, sest me ei soovi millegagi asendada) --> Replace all.
    }
}
