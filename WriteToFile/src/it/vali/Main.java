package it.vali;

import java.io.FileWriter; // io = input out
import java.io.IOException; // input-output exception- midagi on sisendi või väljundiga viga

public class Main {

    public static void main(String[] args) {
        // Try plokis otsitakse/oodatakse exceptionit/erindit (erand, viga)
        try {
            // FileWriter on selline klass, mis tegeleb faili kirjutamisega
            // sellest klassist objekti loomisel antakse talle ette faili asukoht.
            // Faili asukoht võib olla ainult faili nimega kirjutatud: output.txt
            // Sel juhul, kui ma ei ütle asukohta, kirjutatakse faili, mis asub samas kaustas, kus meie main.class.
            // Main.class asub source kaustas--> out --> production --> (Faili nimi) --> it --> vali-->
            // või täispika asukohaga c:\\users\\opilane\\document\\output.txt.

            // See kood, mis mul try ploki sees, kui seal kuskil mingil real tekib Exception, siis teeb seda, mida ütlen

            FileWriter fileWriter = new FileWriter("C:\\Users\\opilane\\Documents\\output.txt");
            // teeb faili samasse kausta, kus .class fail ise on
            // Tekitab faili etteantud kohta etteantud nime ja formaadiga

            fileWriter.write(String.format("Elas metsas Mutionu%n")); // Kirjutame faili sisse teksti. Kasutame ka ridade eraldajaid.
            fileWriter.write("Keset kuuski noori vanu" + System.lineSeparator()); // See eraldaja on antud juhul universaalne
            fileWriter.write("kadak-põõsa juure all\r\n");

            fileWriter.close(); // Hästi oluline lisada! Muidu fileWriter ei tea, millal lõpetada!
            // Siis hakkab võtma liiga palju mälu ning seda ei saa salvestada Notepadi, st teised arendajad ei saa samuti failile ligi

        // Catch plokis püütakse kinni kindlat tüüpi Exception või kõik Exceptionid,
            // mis pärinevad antud Exceptionist (n-ö ülem-Exception).

        } catch (IOException e) {
            //e.printStackTrace();
            // e.printStackTrace tähendab, et prinditakse välja meetodite väljakutsumise hierarhia/ajalugu
            System.out.println("Viga: Antud failile ligipääs ei ole võimalik"); // Võin ise kirjutada mingi viisaka teksti vea kohta e.printStackTrace asemel
        }
    }
}
