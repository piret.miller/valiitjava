package it.vali;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        try {
            FileWriter fileWriter = new FileWriter("output.txt", true);
            // FileWriter'i konstruktor, mis võtab vastu vaid ühe parameetri,
            // faili nime, kirjutab olemasoleva faili üle.
            // FileWriteril on aga ka konstruktor, mis võtab vastu kaks parameetrit: faili nime ja booleani.
            // Boolean määrab, kas olemasolevasse faili juurde lisada või fail üle kirjutada.
            fileWriter.append("Tere\r\n");
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("Faili kirjutamisel tekkis viga");
            // e.printStackTrace();
        }

        // 1. Koosta täisarvude massiiv 10 arvust ning seejärel kirjuta faili kõik suuremad arvud kui 2
        int[] numbers = new int [] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

        try {
            FileWriter fileWriter = new FileWriter("numbers.txt");
            for (int i = 0; i < numbers.length; i++) {
                if (numbers [i] > 2) {
                    fileWriter.write(numbers[i] + "\r\n");
                }
            }
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // 2. Küsi kasutajalt kaks arvu.
        // Küsi seni kuni mõlemad on korrektsed (on numbriks teisendatavad)
        // ning nende summa ei ole paaris arv.

        Scanner scanner = new Scanner(System.in);
        int firstNumber = 0;
        int secondNumber = 0;

        do {
            boolean correctNumber =false;
            do {
                System.out.println("Ütle esimene number");
                try {
                    firstNumber = Integer.parseInt(scanner.nextLine()); // Teisendame vastuse kohe integeriks.
                    correctNumber = true;
                    // Try vaatab, kas int answer oli tõesti intina sisestatud.
                    // Kui oli, siis Boolean isCorrectNumber muutub tõeseks ja catchi ega While-tsüklisse ei jõua
                } catch (NumberFormatException e) { // IDE ise panigi kohe õige Exceptioni.
                    // See on veelgi detailsem kui Arithmetic Exception, öeldes isegi ära selle, et numbri formaat on vale
                    System.out.println("Number oli vigane. Sisesta reaalne number");
                }
            } while (!correctNumber); // peame mõtlema mingi tingimuse,
            // mis oleks väär seni, kuni juhtub Exception.

            correctNumber = false;

            do {
                System.out.println("Ütle teine number");
                try {
                    secondNumber = Integer.parseInt(scanner.nextLine());
                    correctNumber = true;
                } catch (NumberFormatException e) {
                    System.out.println("Number oli vigane. Sisesta reaalne number");
                }
            } while (!correctNumber);
        } while ((firstNumber + secondNumber) % 2 == 0);

        // Kui on väär, tsükkel kordab ennast lõputult või seni,
        // kuni lõpuks sisestatakse numbriformaadis vastus.

        // 3. Küsi kasutajalt mitu arvu ta tahab sisestada,
        // seejärel küsi ühe kaupa kasutajalt need arvud
        // ning kirjuta nende arvude summa faili,
        // nii et see lisatakse alati juurde (Append)
        // Tulemus: iga programmi käivitamise järel on failis
        // kõik eelnevad summad kirjas.

        System.out.println("Mitu arvu sa soovid sisestada?");
        int howManyNumbers = Integer.parseInt(scanner.nextLine());
        int total = 0;

        for (int i = 0; i < howManyNumbers ; i++) {
            try {
                System.out.printf("Sisesta number %d%n", i +1 );
                int userNumbers = Integer.parseInt(scanner.nextLine());
                total = total + userNumbers;

            } catch (NumberFormatException e) {
                    System.out.println("Number oli vigane. Palun sisesta reaalne arv.");
            }
        }
        try {
            FileWriter fileWriter = new FileWriter("sum.txt", true);
            fileWriter.append(total + System.lineSeparator());
            fileWriter.close();

        } catch (IOException e) {
            System.out.println("Faili kirjutamisel tekkis viga");
            // e.printStackTrace();
        }
    }
}
