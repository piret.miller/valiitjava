package it.vali;

public class Main {

    public static void main(String[] args) {
        Monitor firstMonitor = new Monitor(); // Nüüd saame luua objekti
        Monitor secondMonitor = new Monitor();
        Monitor thirdMonitor = new Monitor();

        // Loome objektidele sisu ka. Punktiga saab välja kutsuda tema meetodeid ja väärtuseid

        firstMonitor.manufacturer = "Philips";
        firstMonitor.color = Color.WHITE;
        firstMonitor.diagonal = 27;
        firstMonitor.screenType = ScreenType.AMOLED;

        secondMonitor.manufacturer = "LG";
        secondMonitor.color = Color.BLACK;
        secondMonitor.diagonal = 24;
        secondMonitor.screenType = ScreenType.LCD;

        // Nii oleks üks meetod lisada uus monitor:

//        thirdMonitor.manufacturer = "Samsung";
//        thirdMonitor.color = Color.GREY;
//        thirdMonitor.diagonal = 26;
//        thirdMonitor.screenType = ScreenType.OLED;

        System.out.println(firstMonitor.manufacturer);
        System.out.println(secondMonitor.screenType);

        firstMonitor.color = Color.BLACK; // Saab uuesti deklareerida, et muuta parameetreid

        Monitor[] monitors = new Monitor[4];

        monitors[0] = firstMonitor;
        monitors[1] = secondMonitor;
        monitors[2] = thirdMonitor;
        monitors[3] = new Monitor();
        monitors[3].manufacturer = "Samsung"; // aga saab ka otse siia luua uue Monitori massiivi
        monitors[3].color = Color.GREY;
        monitors[3].diagonal = 26;
        monitors[3].screenType = ScreenType.OLED;


        // Lisa massiivi 3 monitori
        // Prindi välja kõikide monitoride diagonaal ja tootja, mille diagonaal on suurem kui 25 tolli

        for (int i = 0; i < monitors.length ; i++) {
            System.out.println(monitors[i].manufacturer);
        }
        for (int i = 0; i < monitors.length ; i++) {
            if(monitors[i].diagonal > 25) {
                System.out.println(monitors[i].manufacturer);
            }
        }

        // Leia monitori värv kõige suuremal monitoril

        Monitor maxSizeMonitor = monitors[0]; // Teen uue muutuja ja panen võrduma esimesel kohal oleva monitoriga

        for (int i = 0; i < monitors.length; i++) {
            if(monitors[i].diagonal > maxSizeMonitor.diagonal) {
                maxSizeMonitor = monitors[i];
            }
//            System.out.printf("Monitor %s on suurima ekraaniga ja see on %s värvi%n", monitors[i], monitors[i].color);
        }
        System.out.println(maxSizeMonitor.color);
        System.out.println(maxSizeMonitor.manufacturer);
        System.out.println(maxSizeMonitor.screenType);

        maxSizeMonitor.printInfo();
        firstMonitor.printInfo();
        secondMonitor.printInfo();

        System.out.printf("Monitori diagonaal cm-tes on %.2f%n", maxSizeMonitor.DiagonalToCm());
//        thirdMonitor.printInfo();
//
//
//
//        double max = monitors[0].diagonal;
//        Color color = monitors[0].color;
//
//        for (int i = 0; i < monitors.length; i++) {
//            if(monitors[i].diagonal > max) {
//                max = monitors[i].diagonal;
//                color = monitors[i].color;
//            }
//        }
//        System.out.printf("Monitor %s on suurima ekraaniga ja see on %s värvi%n", monitors[i], monitors[i].color);

    }
}
