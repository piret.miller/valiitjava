package it.vali;

// Enum on tüüp, kus saab defineerida erinevaid lõplikke valikuid
// Tegelikult salvestatakse enum alati int-na (selgitus materjalidesse saadetud)

enum Color {
    BLACK,
    WHITE,
    GREY,
}
enum ScreenType {
    LCD, TFT, OLED, AMOLED
}

public class Monitor {
    String manufacturer;
    double diagonal; // " inch, tollides
    Color color; // Stringi asemel lõime enum paketi/tüübi, mille täitsime värvidega. Lisame selle värvi tüübiks
    ScreenType screenType;

    void printInfo() {
        System.out.println();
        System.out.println("Monitori info: ");
        System.out.printf("Tootja: %s%n", manufacturer);
        System.out.printf("Diagonaal: %.1f%n", diagonal);
        System.out.printf("Värv: %s%n", color);
        System.out.printf("Ekraani tüüp: %s%n", screenType);
        System.out.println();
    }

    // Tee meetod, mis tagastab ekraani diagonaali sentimeetrites

    double DiagonalToCm() {
        return diagonal *2.54;
    }
}
